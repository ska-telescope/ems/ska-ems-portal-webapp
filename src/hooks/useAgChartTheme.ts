// eslint-disable-next-line import/named
import { AgChartTheme } from 'ag-grid-enterprise';

import { useCustomTheme } from '@hooks/useCustomTheme.ts';

export const useAgChartTheme = (): AgChartTheme => {
  const { colors } = useCustomTheme();

  return {
    baseTheme: 'ag-default',
    palette: {
      fills: [
        colors.customAccentPrimary,
        colors.customAccentSecondary,
        colors.customAccentTertiary,
        colors.customAccentQuaternary,
        colors.customAccentQuinary,
      ].filter(Boolean) as string[],
      strokes: [
        colors.customAccentPrimary,
        colors.customAccentSecondary,
        colors.customAccentTertiary,
        colors.customAccentQuaternary,
        colors.customAccentQuinary,
      ].filter(Boolean) as string[],
    },
    overrides: {
      common: {
        title: {
          color: colors.textPrimary,
        },
        tooltip: {
          class: 'ag-charts-custom-tooltip',
        },
        subtitle: {
          color: colors.textSecondary,
        },
        legend: {
          item: {
            label: {
              color: colors.textPrimary,
            },
          },
        },
      },
      line: {
        series: {
          label: {
            color: '#FFFFFF',
          },
        },
        axes: {
          category: {
            title: {
              color: colors.textPrimary,
            },
            label: {
              color: colors.textPrimary,
            },
            crossLines: {
              label: {
                color: colors.textPrimary,
              },
            },
          },
          log: {
            title: {
              color: colors.textPrimary,
            },
            label: {
              color: colors.textPrimary,
            },
            crossLines: {
              label: {
                color: colors.textPrimary,
              },
            },
          },
          number: {
            title: {
              color: colors.textPrimary,
            },
            label: {
              color: colors.textPrimary,
            },
            crossLines: {
              label: {
                color: colors.textPrimary,
              },
            },
          },
        },
      },
      bar: {
        series: {
          label: {
            enabled: true,
            color: '#FFFFFF',
          },
        },
        axes: {
          category: {
            title: {
              color: colors.textPrimary,
            },
            label: {
              color: colors.textPrimary,
            },
            crossLines: {
              label: {
                color: colors.textPrimary,
              },
            },
          },
          log: {
            title: {
              color: colors.textPrimary,
            },
            label: {
              color: colors.textPrimary,
            },
            crossLines: {
              label: {
                color: colors.textPrimary,
              },
            },
          },
          number: {
            title: {
              color: colors.textPrimary,
            },
            label: {
              color: colors.textPrimary,
            },
            crossLines: {
              label: {
                color: colors.textPrimary,
              },
            },
          },
        },
      },
    },
  };
};
