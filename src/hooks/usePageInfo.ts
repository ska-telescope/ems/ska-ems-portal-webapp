import { useLocation } from 'react-router-dom';

/**
 * usePageInfo Hook
 *
 * Returns the title and subtitle for the current page based on the URL path.
 * Ensures that users receive meaningful page context dynamically.
 */
export const usePageInfo = () => {
  const location = useLocation();

  /**
   * Retrieves the page title and subtitle based on the given pathname.
   *
   * @param {string} pathname - The current browser route
   * @returns {{ title: string, subtitle: string }} Page title and subtitle
   */
  const getPageInfo = (pathname: string) => {
    const pageInfo: Record<string, { title: string; subtitle: string }> = {
      '/': { title: 'Home', subtitle: 'Welcome to the dashboard' },
      '/locations': { title: 'Locations', subtitle: 'Manage your locations' },
      '/maintenance': {
        title: 'Preventative Maintenance',
        subtitle: 'Manage preventative actions',
      },
      '/settings': {
        title: 'Settings',
        subtitle: 'Configure your preferences',
      },
      '/unauthorized': {
        title: 'Limited Access',
        subtitle: 'Your role may restrict certain sections',
      },
    };

    return (
      pageInfo[pathname] || {
        title: 'Not Found',
        subtitle: 'The page you are looking for does not exist',
      }
    );
  };

  return getPageInfo(location.pathname);
};
