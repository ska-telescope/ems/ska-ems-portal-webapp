import { render, screen } from '@testing-library/react';
import { useLocation } from 'react-router-dom';
import { describe, it, expect, vi } from 'vitest';

import { usePageInfo } from '@hooks/usePageInfo';

// Mock useLocation from react-router-dom
vi.mock('react-router-dom', () => ({
  useLocation: vi.fn(),
}));

const TestComponent = () => {
  const { title, subtitle } = usePageInfo();
  return (
    <div>
      <h1>{title}</h1>
      <h2>{subtitle}</h2>
    </div>
  );
};

describe('usePageInfo', () => {
  it('returns correct info for home page', () => {
    (useLocation as ReturnType<typeof vi.fn>).mockReturnValue({
      pathname: '/',
    });

    render(<TestComponent />);

    expect(screen.getByText('Home')).toBeInTheDocument();
    expect(screen.getByText('Welcome to the dashboard')).toBeInTheDocument();
  });

  it('returns correct info for locations page', () => {
    (useLocation as ReturnType<typeof vi.fn>).mockReturnValue({
      pathname: '/locations',
    });

    render(<TestComponent />);

    expect(screen.getByText('Locations')).toBeInTheDocument();
    expect(screen.getByText('Manage your locations')).toBeInTheDocument();
  });

  it('returns correct info for settings page', () => {
    (useLocation as ReturnType<typeof vi.fn>).mockReturnValue({
      pathname: '/settings',
    });

    render(<TestComponent />);

    expect(screen.getByText('Settings')).toBeInTheDocument();
    expect(screen.getByText('Configure your preferences')).toBeInTheDocument();
  });

  it('returns correct info for unknown page', () => {
    (useLocation as ReturnType<typeof vi.fn>).mockReturnValue({
      pathname: '/unknown',
    });

    render(<TestComponent />);

    expect(screen.getByText('Not Found')).toBeInTheDocument();
    expect(
      screen.getByText('The page you are looking for does not exist'),
    ).toBeInTheDocument();
  });
});
