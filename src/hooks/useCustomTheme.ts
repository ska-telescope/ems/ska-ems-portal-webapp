import { useTheme } from '@mui/material/styles';

export interface CustomColors {
  primary: string;
  primaryLight: string;
  primaryDark: string;
  secondary: string;
  secondaryLight: string;
  secondaryDark: string;
  customAccentPrimary?: string;
  customAccentSecondary?: string;
  customAccentTertiary?: string;
  customAccentQuaternary?: string;
  customAccentQuinary?: string;
  backgroundDefault: string;
  backgroundPaper: string;
  textPrimary: string;
  textSecondary: string;
}

export const useCustomTheme = () => {
  const theme = useTheme();

  return {
    colors: {
      primary: theme.palette.primary.main,
      primaryLight: theme.palette.primary.light,
      primaryDark: theme.palette.primary.dark,
      secondary: theme.palette.secondary.main,
      secondaryLight: theme.palette.secondary.light,
      secondaryDark: theme.palette.secondary.dark,
      customAccentPrimary: theme.custom.accentPrimary,
      customAccentSecondary: theme.custom.accentSecondary,
      customAccentTertiary: theme.custom.accentTertiary,
      customAccentQuaternary: theme.custom.accentQuaternary,
      customAccentQuinary: theme.custom.accentQuinary,
      backgroundDefault: theme.palette.background.default,
      backgroundPaper: theme.palette.background.paper,
      customContainerBackground: theme.custom.containerBackground,
      textPrimary: theme.palette.text.primary,
      textSecondary: theme.palette.text.secondary,
    },
    typography: theme.typography,
    spacing: theme.spacing,
    transitions: theme.transitions,
  };
};
