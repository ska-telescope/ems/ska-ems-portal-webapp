import { useIsAuthenticated } from '@azure/msal-react';
import { useQuery } from '@tanstack/react-query';

import {
  fetchCompleteLocationsTree,
  LocationNode,
} from '@/api/locations/locationTreeApi';
import useAuthApiClient from '@/auth/hooks/useAuthApiClient';
import { useUserRoles } from '@/auth/hooks/useUserRoles';
import { useLocationStore } from '@/stores/useLocationStore';

/**
 * usePreloadData Hook
 *
 * Preloads and caches the complete location tree structure.
 * Ensures that data is only fetched for authenticated users with valid roles.
 * Uses React Query for efficient data fetching and Zustand for state management.
 *
 * @returns {{
 *  isLoading: boolean;
 *  isError: boolean;
 * }}
 */
const usePreloadData = () => {
  const { loadTree } = useLocationStore(); // Zustand store for location data
  const authApiClient = useAuthApiClient();
  const isAuthenticated = useIsAuthenticated();
  const { baseRoles } = useUserRoles(); // Get user roles
  const hasValidRoles = baseRoles.length > 0; // Check if user has any valid roles

  /**
   * Fetch the complete location tree if the user is authorized.
   * - Prevents unauthorized users from making API calls (avoiding 403 errors).
   * - Stores fetched data in Zustand for global access.
   */
  const query = useQuery<LocationNode[], Error>({
    queryKey: ['complete-location-tree'],
    queryFn: async () => {
      if (!hasValidRoles) return []; // Prevent API calls if user lacks roles

      const fullTree = await fetchCompleteLocationsTree(authApiClient);
      loadTree(fullTree);
      return fullTree;
    },
    enabled: isAuthenticated && hasValidRoles, // Ensures query only runs for authorized users
    staleTime: 1000 * 60 * 10, // Cache tree data for 10 minutes
    refetchOnWindowFocus: false, // Prevent refetching when switching tabs
  });

  return {
    isLoading: query.isLoading, // Indicates loading state
    isError: query.isError, // Indicates if an error occurred
  };
};

export default usePreloadData;
