import { Box, Container, Typography } from '@mui/material';

/**
 * UnauthorizedPage Component
 *
 * Displays a message when a user lacks the required permissions.
 * Provides accessibility features and test-friendly attributes.
 */
const UnauthorizedPage = () => {
  return (
    <Container maxWidth="sm" data-testid="unauthorized-container">
      <Box
        sx={{
          textAlign: 'center',
          mt: 10,
          p: 4,
          borderRadius: 2,
          boxShadow: 3,
          bgcolor: 'background.paper',
        }}
        role="alert"
        aria-live="assertive"
        data-testid="unauthorized-box"
      >
        {/* Title */}
        <Typography
          variant="h4"
          color="error"
          gutterBottom
          aria-label="Access Restricted"
          data-testid="unauthorized-title"
        >
          Access Restricted
        </Typography>

        {/* Message */}
        <Typography
          variant="body1"
          sx={{ mb: 2 }}
          aria-label="Permission message"
          data-testid="unauthorized-message"
        >
          You are signed in, but your account does not have the necessary
          permissions to view this page.
        </Typography>

        {/* Additional info */}
        <Typography
          variant="body2"
          color="textSecondary"
          aria-label="Admin contact info"
          data-testid="unauthorized-contact"
        >
          If you believe this is an error, please contact your administrator.
        </Typography>
      </Box>
    </Container>
  );
};

export default UnauthorizedPage;
