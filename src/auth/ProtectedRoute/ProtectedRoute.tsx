import { useIsAuthenticated, useMsal } from '@azure/msal-react';
import React from 'react';
import { Navigate } from 'react-router-dom';

interface ProtectedRouteProps {
  children: React.ReactElement;
  requiredRoles?: string[];
}

/**
 * ProtectedRoute Component
 *
 * A wrapper component that protects a route based on user authentication and roles.
 * Redirects the user to the login page if not authenticated, or to an unauthorized
 * page if they lack the required roles.
 *
 * @param {React.ReactElement} children - The component to render if access is granted.
 * @param {string[]} requiredRoles - Optional array of roles that the user must have to access this route.
 */
const ProtectedRoute: React.FC<ProtectedRouteProps> = ({
  children,
  requiredRoles,
}) => {
  const isAuthenticated = useIsAuthenticated();
  const { accounts } = useMsal();

  /**
   * Checks if the authenticated user has the required roles.
   *
   * If no roles are specified in requiredRoles, access is granted by default.
   * Otherwise, checks the user's roles in the ID token against requiredRoles.
   *
   * @returns {boolean} - True if the user has required roles, or if no roles are specified; false otherwise.
   */
  const userHasRequiredRole = () => {
    if (!requiredRoles || requiredRoles.length === 0) {
      return true; // No roles required
    }

    if (accounts.length > 0) {
      const account = accounts[0];
      const roles =
        account.idTokenClaims?.roles || account.idTokenClaims?.role || [];

      // Ensure roles is an array
      const userRoles = Array.isArray(roles) ? roles : [roles];

      // Check if the user has at least one of the required roles
      return requiredRoles.some((role) => userRoles.includes(role));
    }

    // No account or roles found
    return false;
  };

  if (!isAuthenticated) {
    // User is not authenticated; redirect to the login page
    return <Navigate to="/login" replace />;
  }

  if (!userHasRequiredRole()) {
    // User does not have the required roles; redirect to unauthorized page
    return <Navigate to="/unauthorized" replace />;
  }

  // User is authenticated and has required roles if specified
  return children;
};

export default ProtectedRoute;
