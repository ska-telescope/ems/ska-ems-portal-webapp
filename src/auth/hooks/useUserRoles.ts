import { useMsal } from '@azure/msal-react';

import {
  SUPER_ADMIN,
  BASE_ROLES,
  MID_ADMIN,
  MID_EDIT,
  MID_READ,
  LOW_ADMIN,
  LOW_EDIT,
  LOW_READ,
} from '@/constants/roles';

export const useUserRoles = () => {
  const { accounts } = useMsal();

  // Get all roles from token
  const roles = accounts[0]?.idTokenClaims?.roles || [];
  const allRoles = Array.isArray(roles) ? roles : [roles];

  // Filter roles based on predefined role lists
  const baseRoles = allRoles.filter((role) => BASE_ROLES.includes(role));
  // Extract location-based roles (MID, LOW)
  const extendedRoles = allRoles.filter((role) =>
    [MID_ADMIN, MID_EDIT, MID_READ, LOW_ADMIN, LOW_EDIT, LOW_READ].includes(
      role,
    ),
  );

  // const isSuperAdmin = baseRoles.includes(SUPER_ADMIN);

  return {
    // All roles from token
    allRoles,
    // Fundamental application roles
    baseRoles,
    // Extended/custom roles (location-based, etc)
    extendedRoles,
    // Special super admin flag
    isSuperAdmin: baseRoles.includes(SUPER_ADMIN),

    // Core checks
    hasBaseRole: (role: string) => baseRoles.includes(role),
    hasExtendedRole: (role: string) => extendedRoles.includes(role),

    // Flexible checks
    hasAnyRole: (...roles: string[]) => roles.some((r) => allRoles.includes(r)),
    hasAllRoles: (...roles: string[]) =>
      roles.every((r) => allRoles.includes(r)),
  };
};
