import { createTheme, lighten, darken } from '@mui/material/styles';

// TODO: Fix eslint-disable-next-line and typescript disablerers comments

// Extend Material-UI's theme types
declare module '@mui/material/styles' {
  interface Theme {
    custom: {
      accentPrimary: string;
      accentSecondary: string;
      accentTertiary: string;
      accentQuaternary?: string;
      accentQuinary?: string;
      containerBackground?: string;
    };
  }
}

export const themes = {
  theme1: {
    accentPrimary: darken('#00bcd4', 0.7),
    accentSecondary: '#00bcd4',
    accentTertiary: darken('#00bcd4', 0.3),
    accentQuaternary: darken('#00bcd4', 0.2),
    accentQuinary: darken('#00bcd4', 0.5),
  },
  theme2: {
    // accentPrimary: '#e91e63',
    // accentSecondary: lighten('#e91e63', 0.7),
    // accentTertiary: lighten('#e91e63', 0.5),
    // accentQuaternary: lighten('#e91e63', 0.2),
    // accentQuinary: lighten('#e91e63', 0.1),
    accentPrimary: '#070068',
    accentSecondary: '#E70068',
    accentTertiary: '#890071',
    accentQuaternary: '#46006f',
    accentQuinary: lighten('#28006a', 0.2),
  },
  theme3: {
    // accentPrimary: '#8bc34a',
    // accentSecondary: lighten('#8bc34a', 0.7),
    // accentTertiary: lighten('#8bc34a', 0.5),
    // accentQuaternary: lighten('#8bc34a', 0.2),
    // accentQuinary: lighten('#8bc34a', 0.1),
    accentPrimary: lighten('#0D1D2C', 0.1),
    accentSecondary: '#C1D66E',
    accentTertiary: '#087CBB',
    accentQuaternary: lighten('#17243D', 0.2),
    accentQuinary: lighten('#0E2538', 0.3),
  },
  theme4: {
    accentPrimary: darken('#ff5722', 0.7),
    accentSecondary: '#ff5722',
    accentTertiary: darken('#ff5722', 0.3),
    accentQuaternary: darken('#ff5722', 0.2),
    accentQuinary: darken('#ff5722', 0.5),
    // accentPrimary: '#003D51',
    // accentSecondary: '#EFEA78',
    // accentTertiary: '#F49E6B',
    // accentQuaternary: '#EF7C38',
    // accentQuinary: '#562A31',
  },
  theme5: {
    // accentPrimary: '#9c27b0',
    // accentSecondary: lighten('#9c27b0', 0.7),
    // accentTertiary: lighten('#9c27b0', 0.5),
    // accentQuaternary: lighten('#9c27b0', 0.2),
    // accentQuinary: lighten('#9c27b0', 0.1),
    accentPrimary: '#0B3B5C',
    accentSecondary: '#EB5C5D',
    accentTertiary: '#D9B38E',
    accentQuaternary: '#74C095',
    accentQuinary: '#4689C8',
  },
};

const getDesignTokens = (mode: 'light' | 'dark', themeName: string) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  const selectedTheme = themes[themeName];

  return {
    palette: {
      mode,
      primary: {
        main: mode === 'light' ? '#8199a6' : '#424242',
        light: mode === 'light' ? '#90a4ae' : '#6d6d6d',
        dark: mode === 'light' ? '#34515e' : '#1b1b1b',
      },
      secondary: {
        main: mode === 'light' ? '#ffab91' : '#ff8a65',
        light: mode === 'light' ? '#ffddc1' : '#ffbb93',
        dark: mode === 'light' ? '#c97b63' : '#c75b39',
      },
      background: {
        default: mode === 'light' ? '#f5f5f5' : '#666666',
        paper: mode === 'light' ? '#ffffff' : '#1e1e1e',
      },
      text: {
        primary: mode === 'light' ? '#000000' : '#ffffff',
        secondary: mode === 'light' ? '#555555' : '#cdcdcd',
      },
    },
    custom: {
      accentPrimary: selectedTheme.accentPrimary,
      accentSecondary: selectedTheme.accentSecondary,
      accentTertiary: selectedTheme.accentTertiary,
      accentQuaternary: selectedTheme.accentQuaternary,
      accentQuinary: selectedTheme.accentQuinary,
      containerBackground: '#f0f0f0',
    },
    typography: {
      fontFamily: 'Noto Sans, system-ui, Avenir, Helvetica, Arial, sans-serif',
    },
    components: {
      MuiCssBaseline: {
        styleOverrides: {
          '.rbc-button-link.rbc-show-more': {
            color: selectedTheme.accentPrimary,
            fontWeight: 'bold',
            textDecoration: 'underline',
          },
          ':root': {
            '--primary-main': mode === 'light' ? '#8199a6' : '#424242',
          },
        },
      },
      MuiButton: {
        styleOverrides: {
          root: {
            borderRadius: '8px',
          },
        },
        variants: [
          {
            props: { variant: 'contained' },
            style: {
              backgroundColor: selectedTheme.accentSecondary,
              color: '#fff',
              '&:hover': {
                backgroundColor: selectedTheme.accentSecondary,
              },
            },
          },
        ],
      },
    },
  };
};

export const getTheme = (mode: 'light' | 'dark', themeName: string) =>
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  createTheme(getDesignTokens(mode, themeName));
