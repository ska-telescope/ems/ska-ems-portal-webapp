/**
 * ThemeProvider: Manages the application's theme (light/dark) and user-selected accent color.
 *
 * Features:
 * - Theme switching between light and dark modes.
 * - Accent color customization.
 * - Theme persistence across sessions using localStorage.
 * - Dynamically applies global styles based on the active theme.
 */
import { ThemeProvider as MUIThemeProvider, CssBaseline } from '@mui/material';
import { createContext, useState, useMemo, ReactNode } from 'react';

import GlobalStyles from '@/styles/GlobalStyles';
import { getTheme } from '@/theme/theme';

// Context to manage theme mode, selected theme, and accent color
interface ThemeContextProps {
  themeMode: 'light' | 'dark';
  toggleTheme: () => void;
  setThemeMode: (mode: 'light' | 'dark') => void;
  selectedTheme: string; // theme1, theme2, theme3
  setSelectedTheme: (theme: string) => void;
  accentColor: string;
  setAccentColor: (color: string) => void;
}

// ThemeContext: Provides the theme mode, selected theme, accent color, and related updater functions
export const ThemeContext = createContext<ThemeContextProps>({
  themeMode: 'light',
  toggleTheme: () => {},
  setThemeMode: () => {},
  selectedTheme: 'theme1',
  setSelectedTheme: () => {},
  accentColor: '#00bcd4',
  setAccentColor: () => {},
});

export const CustomThemeProvider = ({ children }: { children: ReactNode }) => {
  // Retrieve persisted theme settings from localStorage
  const savedThemeMode = localStorage.getItem('themeMode') || 'light';
  const savedAccentColor = localStorage.getItem('accentColor') || '#00bcd4';
  const savedSelectedTheme = localStorage.getItem('selectedTheme') || 'theme1';

  const themeSettings = [
    { key: 'themeMode', value: savedThemeMode },
    { key: 'accentColor', value: savedAccentColor },
    { key: 'selectedTheme', value: savedSelectedTheme },
  ];

  themeSettings.forEach(({ key, value }) => {
    if (!localStorage.getItem(key)) {
      localStorage.setItem(key, value);
    }
  });

  // State management for theme mode, selected theme, and accent color
  const [themeMode, setThemeModeState] = useState<'light' | 'dark'>(
    savedThemeMode as 'light' | 'dark',
  );
  const [accentColor, setAccentColorState] = useState<string>(savedAccentColor);
  const [selectedTheme, setSelectedThemeState] =
    useState<string>(savedSelectedTheme);

  // Toggle theme between light and dark
  const toggleTheme = () => {
    const newMode = themeMode === 'light' ? 'dark' : 'light';
    setThemeModeState(newMode);
    localStorage.setItem('themeMode', newMode);
  };

  // Set theme mode explicitly
  const setThemeMode = (mode: 'light' | 'dark') => {
    setThemeModeState(mode);
    localStorage.setItem('themeMode', mode);
  };

  // Set accent color explicitly
  const setAccentColor = (color: string) => {
    setAccentColorState(color);
    localStorage.setItem('accentColor', color);
  };

  // Set the selected theme explicitly
  const setSelectedTheme = (theme: string) => {
    setSelectedThemeState(theme);
    localStorage.setItem('selectedTheme', theme);
  };

  // Memoized theme object based on current mode, selected theme, and accent color
  const currentTheme = useMemo(
    () => getTheme(themeMode, selectedTheme),
    [themeMode, selectedTheme],
  );

  return (
    <ThemeContext.Provider
      value={{
        themeMode,
        toggleTheme,
        setThemeMode,
        selectedTheme,
        setSelectedTheme,
        accentColor,
        setAccentColor,
      }}
    >
      {/* Apply the current theme using MUI's ThemeProvider */}
      <MUIThemeProvider theme={currentTheme}>
        <CssBaseline />
        <GlobalStyles />
        {children}
      </MUIThemeProvider>
    </ThemeContext.Provider>
  );
};
