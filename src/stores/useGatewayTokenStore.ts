import { create } from 'zustand';
import { createJSONStorage, persist } from 'zustand/middleware';

import { TokenData } from '@/api/apiTokens/gatewayTokensApi';

// Define the state interface for the store
interface TokenState {
  tokenData: TokenData | null;
  showForm: boolean;
  showResult: boolean;
  setShowForm: (show: boolean) => void;
  setShowResult: (show: boolean) => void;
  setTokenData: (data: TokenData) => void;
  reset: () => void; // Reset the store state
}

// Zustand store definition with persistence
export const useTokenStore = create<TokenState>()(
  persist(
    (set) => ({
      tokenData: null,
      showForm: false, // Start with showing the table, not the form
      showResult: false,
      setShowForm: (show: boolean) =>
        set({ showForm: show, showResult: false }),
      setShowResult: (show: boolean) =>
        set({ showResult: show, showForm: false }),
      setTokenData: (data: TokenData) => set({ tokenData: data }),
      reset: () =>
        set({
          tokenData: null,
          showForm: false,
          showResult: false,
        }), // Reset all state back to default
    }),
    {
      name: 'token-storage', // Name for localStorage
      storage: createJSONStorage(() => localStorage), // Persist in localStorage
    },
  ),
);
