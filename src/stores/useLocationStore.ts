/**
 * Location Store using Zustand with Persistence
 *
 * This module defines the Zustand store for managing the hierarchical location data.
 * The store is persisted using localStorage, allowing data to be saved across sessions.
 *
 * Key Features:
 * - Load the complete tree structure
 * - Dynamically update specific nodes
 * - Retrieve individual nodes by ID
 * - Track the selected node and its ancestors (for breadcrumbs)
 * - Persistent state management using localStorage
 */

import { create } from 'zustand';
import { persist, createJSONStorage } from 'zustand/middleware';

interface LocationNode {
  id: string;
  label: string;
  type: number;
  children?: LocationNode[];
  isLoaded?: boolean;
}

interface LocationState {
  locations: LocationNode[] | null;
  selectedNode: LocationNode | null;
  loadTree: (locations: LocationNode[]) => void;
  setSelectedNode: (node: LocationNode | null) => void;
  getAncestors: (id: string) => LocationNode[];
  setLocations: (locations: LocationNode[]) => void;
  clearSelectedNode: () => void;
  updateNode: (id: string, updates: Partial<LocationNode>) => void;
  getNode: (id: string) => LocationNode | undefined;
}

/**
 * Recursively searches for a node by ID and updates it with the provided changes.
 *
 * @param nodes - The array of location nodes to search within.
 * @param id - The ID of the node to find and update.
 * @param updates - The updates to apply to the found node.
 * @returns Updated array of nodes with the changes applied.
 */
const findAndUpdateNode = (
  nodes: LocationNode[],
  id: string,
  updates: Partial<LocationNode>,
): LocationNode[] => {
  return nodes.map((node) => {
    if (node.id === id) {
      return { ...node, ...updates };
    }
    if (node.children) {
      return {
        ...node,
        children: findAndUpdateNode(node.children, id, updates),
      };
    }
    return node;
  });
};

/**
 * Recursively searches for a node by ID.
 *
 * @param nodes - The array of location nodes to search within.
 * @param id - The ID of the node to find.
 * @returns The found node, or undefined if not found.
 */
const findNode = (
  nodes: LocationNode[],
  id: string,
): LocationNode | undefined => {
  for (const node of nodes) {
    if (node.id === id) return node;
    if (node.children) {
      const found = findNode(node.children, id);
      if (found) return found;
    }
  }
  return undefined;
};

/**
 * Recursively finds all ancestors of a node by ID.
 *
 * @param nodes - The array of location nodes to search within.
 * @param id - The ID of the node whose ancestors are to be found.
 * @returns An array of LocationNode objects representing the ancestors.
 */
const findAncestors = (nodes: LocationNode[], id: string): LocationNode[] => {
  for (const node of nodes) {
    if (node.id === id) {
      return [node];
    }
    if (node.children) {
      const ancestors = findAncestors(node.children, id);
      if (ancestors.length > 0) {
        return [node, ...ancestors];
      }
    }
  }
  return [];
};

/**
 * Zustand store definition with persistence.
 *
 * This store manages the hierarchical location data and persists it using localStorage.
 */
export const useLocationStore = create<LocationState>()(
  persist(
    (set, get) => ({
      locations: null, // Initially null, indicating that it hasn’t been loaded yet
      selectedNode: null, // Track the currently selected node
      clearSelectedNode: () => set({ selectedNode: null }), // Remove selected node
      loadTree: (locations) => {
        set({ locations });
      },
      setSelectedNode: (node) => {
        set({ selectedNode: node });
      },
      getAncestors: (id) => {
        const currentLocations = get().locations;
        if (!currentLocations) return [];
        return findAncestors(currentLocations, id);
      },
      setLocations: (locations) => {
        const currentLocations = get().locations;
        if (currentLocations) {
          // Merge new locations with existing ones
          const updatedLocations = [...currentLocations, ...locations];
          set({ locations: updatedLocations });
        } else {
          // First-time load or reset
          set({ locations });
        }
      },
      updateNode: (id, updates) => {
        const currentLocations = get().locations;
        if (currentLocations) {
          const newLocations = findAndUpdateNode(currentLocations, id, updates);
          set({ locations: newLocations });
        }
      },
      getNode: (id) => {
        const currentLocations = get().locations;
        if (!currentLocations) return undefined;
        return findNode(currentLocations, id);
      },
    }),
    {
      name: 'location-storage',
      storage: createJSONStorage(() => localStorage), // Use createJSONStorage for localStorage compatibility
    },
  ),
);
