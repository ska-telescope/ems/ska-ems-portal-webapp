import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { render, screen } from '@testing-library/react';
import { endOfMonth, startOfMonth } from 'date-fns';
import { vi } from 'vitest';

import { fetchEvents } from '@/api';

import CalendarContainer from './Calendar';

vi.mock('@mui/material', () => ({
  useTheme: vi.fn(),
}));

vi.mock('@/api', () => ({
  fetchEvents: vi.fn(),
}));

const createTestQueryClient = () => {
  return new QueryClient({
    defaultOptions: {
      queries: {
        retry: false,
      },
    },
  });
};

const renderWithQueryClient = (ui: React.ReactElement) => {
  const queryClient = createTestQueryClient();
  return render(
    <QueryClientProvider client={queryClient}>{ui}</QueryClientProvider>,
  );
};
describe('CalendarContainer', () => {
  test('renders the calendar component', () => {
    renderWithQueryClient(<CalendarContainer />);
    expect(
      screen.getByRole('table', { name: 'Month View' }),
    ).toBeInTheDocument();
  });

  test('calls fetchEvents on component mount', async () => {
    renderWithQueryClient(<CalendarContainer />);

    expect(fetchEvents).toHaveBeenCalledWith(
      startOfMonth(new Date()),
      endOfMonth(new Date()),
      expect.any(Function),
      false,
      [1, 2, 3],
    );
  });
});
