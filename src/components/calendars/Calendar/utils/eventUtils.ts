import { darken } from '@mui/material';

import { CustomColors } from '@hooks/useCustomTheme.ts';

export interface CalendarEvent {
  id: number;
  title: string;
  start: Date;
  end: Date;
  location: string;
  asset: string;
  description: string;
  status: number | null;
  priority: number | null;
  due_date: string | null;
  assigned_to: string | null;
}

// TODO: Refactor this function to improve code quality, (See MR !15, Comment #2229571943)
export const getEventStyle = (event: CalendarEvent, colors: CustomColors) => {
  let backgroundColor: string | undefined;
  let opacity = 1;

  if (event.priority === 1) {
    if (colors.customAccentPrimary != null) {
      backgroundColor = darken(colors.customAccentPrimary, 0.6);
    }
  } else if (event.priority === 2) {
    backgroundColor = colors.customAccentPrimary;
  } else if (event.priority === 3) {
    backgroundColor = colors.customAccentQuaternary;
  } else {
    backgroundColor = colors.customAccentSecondary;
  }

  if (event.status === 1) {
    opacity = 0.5;
  }

  return {
    backgroundColor,
    color: '#fff',
    opacity,
  };
};
