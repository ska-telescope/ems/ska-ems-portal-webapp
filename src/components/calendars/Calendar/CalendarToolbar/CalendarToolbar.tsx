import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import FilterListIcon from '@mui/icons-material/FilterList';
import TodayIcon from '@mui/icons-material/Today';
import { useTheme } from '@mui/material';
import React, { useCallback, useEffect, useRef } from 'react';

import { useCustomTheme } from '@hooks/useCustomTheme.ts';

import styles from '../Calendar.module.css';

interface CalendarToolbarProps {
  label: string;
  onNavigate: (action: 'PREV' | 'NEXT' | 'TODAY') => void;
  onView: (view: 'month' | 'week' | 'day' | 'agenda') => void;
  selectedPriorities: string[];
  onPriorityChange: (priority: string) => void;
  priorities: string[];
  hideClosed: boolean;
  dropdownOpen: boolean;
  onHideClosedToggle: () => void;
  onPriorityChangeToggle: () => void;
}

const CalendarToolbar: React.FC<CalendarToolbarProps> = ({
  label,
  onNavigate,
  onView,
  selectedPriorities,
  onPriorityChange,
  priorities,
  hideClosed,
  dropdownOpen,
  onHideClosedToggle,
  onPriorityChangeToggle,
}) => {
  const dropdownRef = useRef<HTMLDivElement>(null);
  const theme = useTheme();
  const { colors } = useCustomTheme();

  const views: Array<'month' | 'week' | 'day' | 'agenda'> = [
    'month',
    'week',
    'day',
    'agenda',
  ];

  const memoizedOnPriorityChangeToggle = useCallback(onPriorityChangeToggle, [
    onPriorityChangeToggle,
  ]);

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        dropdownRef.current &&
        !dropdownRef.current.contains(event.target as Node)
      ) {
        memoizedOnPriorityChangeToggle();
      }
    };

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [memoizedOnPriorityChangeToggle]);

  return (
    <div
      className="rbc-toolbar"
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <div className="rbc-btn-group">
          <ChevronLeftIcon
            style={{ cursor: 'pointer', fontSize: '24px', marginRight: '10px' }}
            onClick={() => onNavigate('PREV')}
          />
          <TodayIcon
            style={{ cursor: 'pointer', fontSize: '23px', marginRight: '10px' }}
            onClick={() => onNavigate('TODAY')}
          />
          <ChevronRightIcon
            style={{ cursor: 'pointer', fontSize: '24px' }}
            onClick={() => onNavigate('NEXT')}
          />
        </div>

        <span
          className="rbc-toolbar-label"
          style={{
            fontWeight: 'bold',
            marginLeft: '10px',
            minWidth: '200px',
            textAlign: 'center',
          }}
        >
          {label}
        </span>

        <div className="rbc-btn-group" style={{ marginLeft: '20px' }}>
          <div className={styles.dropdown}>
            <FilterListIcon
              style={{
                cursor: 'pointer',
                color: colors.textPrimary,
              }}
              onClick={onPriorityChangeToggle}
            />
            {dropdownOpen && (
              <div
                className={styles['dropdown-content']}
                ref={dropdownRef}
                style={{
                  backgroundColor: colors.backgroundDefault,
                  color: colors.textPrimary,
                  border: `1px solid ${theme.palette.divider}`,
                  padding: '10px',
                  borderRadius: '4px',
                  boxShadow: theme.shadows[3],
                }}
              >
                <div style={{ marginBottom: '10px' }}>Priority</div>
                <hr style={{ marginBottom: '10px', borderColor: '#ccc' }} />
                {priorities.map((priority) => (
                  <label
                    key={priority}
                    className={styles['priority-checkbox']}
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      marginBottom: '5px',
                      cursor: 'pointer',
                      color: colors.textPrimary,
                    }}
                  >
                    <input
                      type="checkbox"
                      checked={selectedPriorities.includes(priority)}
                      onChange={() => onPriorityChange(priority)}
                      style={{
                        accentColor: colors.customAccentPrimary,
                      }}
                    />
                    <span style={{ marginLeft: '8px' }}>{priority}</span>
                  </label>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>

      <div style={{ display: 'flex', alignItems: 'center' }}>
        <div className="rbc-btn-group">
          <button
            type="button"
            className={`btn ${hideClosed ? 'active' : ''}`}
            onClick={onHideClosedToggle}
            style={{
              backgroundColor: hideClosed
                ? theme.palette.primary.main
                : 'inherit',
              color: colors.textPrimary,
              transition: 'background-color 0.3s ease',
              cursor: 'pointer',
            }}
            onMouseEnter={(e) =>
              (e.currentTarget.style.backgroundColor =
                theme.palette.primary.main)
            }
            onMouseLeave={(e) =>
              (e.currentTarget.style.backgroundColor = hideClosed
                ? theme.palette.primary.main
                : 'inherit')
            }
          >
            {hideClosed ? 'Show Closed' : 'Hide Closed'}
          </button>

          {views.map((view) => (
            <button
              key={view}
              type="button"
              onClick={() => onView(view)}
              style={{
                color: colors.textPrimary,
                transition: 'background-color 0.3s ease',
                cursor: 'pointer',
              }}
              onMouseEnter={(e) =>
                (e.currentTarget.style.backgroundColor =
                  theme.palette.primary.main)
              }
              onMouseLeave={(e) =>
                (e.currentTarget.style.backgroundColor = 'inherit')
              }
            >
              {view.charAt(0).toUpperCase() + view.slice(1)}
            </button>
          ))}
        </div>
      </div>
    </div>
  );
};

export default CalendarToolbar;
