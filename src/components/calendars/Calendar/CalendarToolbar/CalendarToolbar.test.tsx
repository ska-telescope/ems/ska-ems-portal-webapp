import { ThemeProvider, createTheme } from '@mui/material/styles';
import { render, screen, fireEvent } from '@testing-library/react';
import { describe, it, vi } from 'vitest';

import CalendarToolbar from './CalendarToolbar';

describe('CalendarToolbar', () => {
  const theme = createTheme();
  const mockNavigate = vi.fn();
  const mockViewChange = vi.fn();
  const mockPriorityChange = vi.fn();
  const mockHideClosedToggle = vi.fn();
  const mockPriorityChangeToggle = vi.fn();

  const defaultProps = {
    label: 'October 2024',
    onNavigate: mockNavigate,
    onView: mockViewChange,
    selectedPriorities: ['High'],
    onPriorityChange: mockPriorityChange,
    priorities: ['Low', 'Medium', 'High'],
    hideClosed: false,
    dropdownOpen: false,
    onHideClosedToggle: mockHideClosedToggle,
    onPriorityChangeToggle: mockPriorityChangeToggle,
  };

  it('renders navigation buttons and calls onNavigate on click', () => {
    render(
      <ThemeProvider theme={theme}>
        <CalendarToolbar {...defaultProps} />
      </ThemeProvider>,
    );

    const prevButton = screen.getByTestId('ChevronLeftIcon');
    const nextButton = screen.getByTestId('ChevronRightIcon');
    const todayButton = screen.getByTestId('TodayIcon');

    fireEvent.click(prevButton);
    expect(mockNavigate).toHaveBeenCalledWith('PREV');

    fireEvent.click(todayButton);
    expect(mockNavigate).toHaveBeenCalledWith('TODAY');

    fireEvent.click(nextButton);
    expect(mockNavigate).toHaveBeenCalledWith('NEXT');
  });

  it('renders the label', () => {
    render(
      <ThemeProvider theme={theme}>
        <CalendarToolbar {...defaultProps} />
      </ThemeProvider>,
    );
    expect(screen.getByText('October 2024')).toBeInTheDocument();
  });

  it('renders view buttons and calls onView on click', () => {
    render(
      <ThemeProvider theme={theme}>
        <CalendarToolbar {...defaultProps} />
      </ThemeProvider>,
    );

    fireEvent.click(screen.getByText('Month'));
    expect(mockViewChange).toHaveBeenCalledWith('month');

    fireEvent.click(screen.getByText('Week'));
    expect(mockViewChange).toHaveBeenCalledWith('week');

    fireEvent.click(screen.getByText('Day'));
    expect(mockViewChange).toHaveBeenCalledWith('day');

    fireEvent.click(screen.getByText('Agenda'));
    expect(mockViewChange).toHaveBeenCalledWith('agenda');
  });

  it('toggles hide closed button', () => {
    render(
      <ThemeProvider theme={theme}>
        <CalendarToolbar {...defaultProps} hideClosed={true} />
      </ThemeProvider>,
    );

    fireEvent.click(screen.getByText('Show Closed'));
    expect(mockHideClosedToggle).toHaveBeenCalled();
  });

  it('opens and closes the priority dropdown', () => {
    const { rerender } = render(
      <ThemeProvider theme={theme}>
        <CalendarToolbar {...defaultProps} dropdownOpen={false} />
      </ThemeProvider>,
    );

    fireEvent.click(screen.getByTestId('FilterListIcon'));
    expect(mockPriorityChangeToggle).toHaveBeenCalled();

    // Re-render with dropdownOpen true
    rerender(
      <ThemeProvider theme={theme}>
        <CalendarToolbar {...defaultProps} dropdownOpen={true} />
      </ThemeProvider>,
    );

    expect(screen.getByText('Priority')).toBeInTheDocument();
  });

  it('calls onPriorityChange when a priority is selected', () => {
    render(
      <ThemeProvider theme={theme}>
        <CalendarToolbar {...defaultProps} dropdownOpen={true} />
      </ThemeProvider>,
    );

    fireEvent.click(screen.getByLabelText('Low'));
    expect(mockPriorityChange).toHaveBeenCalledWith('Low');
  });
});
