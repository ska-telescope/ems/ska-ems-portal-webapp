import { render, screen, fireEvent, within } from '@testing-library/react';
import { vi } from 'vitest';

import { CalendarEvent } from '@components/calendars/Calendar/utils/eventUtils.ts';

import EventDetailsDialog from './EventDetailsDialog';

describe('EventDetailsDialog', () => {
  const mockOnClose = vi.fn();

  const mockEvent: CalendarEvent = {
    id: 1,
    title: 'Test Event',
    description: 'Test Description',
    status: 0,
    location: 'Location 1',
    asset: 'Asset123',
    priority: 1,
    due_date: '2024-12-31',
    assigned_to: 'John Doe',
    start: new Date('2024-12-30'),
    end: new Date('2024-12-31'),
  };

  afterEach(() => {
    vi.clearAllMocks();
  });

  test('renders dialog with event details', () => {
    render(<EventDetailsDialog event={mockEvent} onClose={mockOnClose} />);

    expect(screen.getByText('Test Event')).toBeInTheDocument();
    expect(screen.getByText('Test Description')).toBeInTheDocument();
    const dialogContent = screen.getByRole('dialog');
    const statusElement = within(dialogContent).getByText('Open', {
      exact: false,
    });
    expect(statusElement).toBeInTheDocument();
    expect(within(dialogContent).getByText('High')).toBeInTheDocument();
    const dueDateElement = within(dialogContent).getByText(
      'Tuesday, December 31st, 2024',
      { exact: false },
    );
    expect(dueDateElement).toBeInTheDocument();

    const assignedToElement = within(dialogContent).getByText('John Doe', {
      exact: false,
    });
    expect(assignedToElement).toBeInTheDocument();
  });

  test('closes the dialog when close button is clicked', () => {
    render(<EventDetailsDialog event={mockEvent} onClose={mockOnClose} />);

    const closeButton = screen.getByTestId('close-button');
    fireEvent.click(closeButton);
    expect(mockOnClose).toHaveBeenCalled();
  });

  test('renders N/A for undefined or null fields', () => {
    const mockIncompleteEvent: CalendarEvent = {
      id: 2,
      title: 'Incomplete Event',
      description: 'Incomplete Event',
      location: 'Location 1',
      asset: 'Asset123',
      status: null,
      priority: null,
      due_date: null,
      assigned_to: null,
      start: new Date('2024-12-30'),
      end: new Date('2024-12-31'),
    };

    render(
      <EventDetailsDialog event={mockIncompleteEvent} onClose={mockOnClose} />,
    );

    const naElements = screen.getAllByText('N/A');
    expect(naElements).toHaveLength(4);
  });
});
