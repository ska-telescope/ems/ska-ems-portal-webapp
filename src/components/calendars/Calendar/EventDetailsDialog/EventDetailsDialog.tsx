import CloseIcon from '@mui/icons-material/Close';
import {
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  Typography,
} from '@mui/material';
import Box from '@mui/material/Box';
import { useTheme } from '@mui/material/styles';
import { format } from 'date-fns';
import React from 'react';

import {
  CalendarEvent,
  getEventStyle,
} from '@components/calendars/Calendar/utils/eventUtils.ts';
import { useCustomTheme } from '@hooks/useCustomTheme.ts';

// TODO: Refactor this component to improve code quality, (See MR !15, Comment #2229571941)
interface EventDetailsDialogProps {
  event: CalendarEvent | null;
  onClose: () => void;
}

const priorityReverseMapping: Record<number, string> = {
  1: 'High',
  2: 'Medium',
  3: 'Low',
};

const getStatusText = (status: number | null | undefined) => {
  if (status === null || status === undefined) return 'N/A';
  return status === 1 ? 'Closed' : 'Open';
};

const getPriorityText = (priority: number | null | undefined) => {
  if (!priority) return 'N/A';
  return priorityReverseMapping[priority];
};

const getFormattedDate = (date: string | null | undefined) => {
  if (!date) return 'N/A';
  return format(new Date(date), 'PPPP');
};

const formatDescription = (description: string) => {
  if (!description || description.trim() === '') return 'N/A';

  // Split by <br> tags and process each part separately
  const parts = description.replace(/<br\s*\/?>/gi, '\n').split('\n');

  return parts.map((part, index) => {
    // Replace <b> and <strong> tags
    const boldText = part.match(/<b>(.*?)<\/b>|<strong>(.*?)<\/strong>/gi);

    if (boldText) {
      return (
        <Typography component="span" key={index}>
          {/* Render the text with bold formatting */}
          {part
            .split(/<b>|<\/b>|<strong>|<\/strong>/)
            .map((segment, i) =>
              boldText.includes(`<b>${segment}</b>`) ||
              boldText.includes(`<strong>${segment}</strong>`) ? (
                <strong key={i}>{segment}</strong>
              ) : (
                <React.Fragment key={i}>{segment}</React.Fragment>
              ),
            )}
          <br />
        </Typography>
      );
    }

    // Otherwise, return the part with a single line break
    return (
      <Typography component="span" key={index}>
        {part}
        <br />
      </Typography>
    );
  });
};

const EventDetailsDialog = ({ event, onClose }: EventDetailsDialogProps) => {
  const { colors } = useCustomTheme();
  const theme = useTheme();

  if (!event) return null;
  const { backgroundColor, opacity } = getEventStyle(event, colors);

  return (
    <Dialog open={!!event} onClose={onClose}>
      <DialogTitle
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          backgroundColor,
          opacity,
          color: '#fff',
        }}
      >
        {event.title}
        <IconButton
          edge="end"
          color="inherit"
          onClick={onClose}
          data-testid="close-button"
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent
        sx={{
          padding: theme.spacing(3),
        }}
      >
        <Box
          id="alert-dialog-description"
          sx={{
            marginTop: theme.spacing(2),
          }}
        >
          <Typography variant="body1" sx={{ marginBottom: theme.spacing(1) }}>
            <strong>Description:</strong>{' '}
            {formatDescription(event.description || 'N/A')}
          </Typography>
          <Typography variant="body1" sx={{ marginBottom: theme.spacing(1) }}>
            <strong>Status:</strong> {getStatusText(event.status)}
          </Typography>
          <Typography variant="body1" sx={{ marginBottom: theme.spacing(1) }}>
            <strong>Priority:</strong> {getPriorityText(event.priority)}
          </Typography>
          <Typography variant="body1" sx={{ marginBottom: theme.spacing(1) }}>
            <strong>Due Date:</strong> {getFormattedDate(event.due_date)}
          </Typography>
          <Typography variant="body1" sx={{ marginBottom: theme.spacing(1) }}>
            <strong>Assigned To:</strong> {event.assigned_to ?? 'N/A'}
          </Typography>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export default EventDetailsDialog;
