import { useQuery } from '@tanstack/react-query';
import {
  endOfDay,
  endOfMonth,
  format,
  getDay,
  parse,
  parseISO,
  startOfDay,
  startOfMonth,
  startOfWeek,
} from 'date-fns';
import { enUS } from 'date-fns/locale/en-US';
import { useEffect, useState } from 'react';
import { Calendar, dateFnsLocalizer } from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';

import { fetchEvents } from '@/api';
import useAuthApiClient from '@/auth/hooks/useAuthApiClient';
import { useCustomTheme } from '@/hooks/useCustomTheme';
import ApiTask from '@api/maintenance/maintenanceApi.ts';
import CalendarToolbar from '@components/calendars/Calendar/CalendarToolbar/CalendarToolbar.tsx';
import EventDetailsDialog from '@components/calendars/Calendar/EventDetailsDialog/EventDetailsDialog.tsx';
import {
  CalendarEvent,
  getEventStyle,
} from '@components/calendars/Calendar/utils/eventUtils.ts';

import './Calendar.module.css';

const locales = {
  'en-US': enUS,
};

const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek: () => startOfWeek(new Date(), { weekStartsOn: 1 }),
  getDay,
  locales,
});

// TODO: Refactor this component to improve code quality, (See MR !15, Comment #2229571947)
const mapTasksToEvents = (tasks: ApiTask[]): CalendarEvent[] => {
  return tasks.map((task) => ({
    id: task.id,
    title: task.name,
    start: parseISO(task.created_date),
    end:
      task.due_date && task.due_date !== '1970-01-01T00:00:00'
        ? parseISO(task.due_date)
        : parseISO(task.created_date),
    location: task.location_name,
    asset: task.asset_name,
    description: task.description,
    status: task.status,
    priority: task.priority,
    due_date: task.due_date,
    assigned_to: task.assigned_to,
  }));
};

const CalendarContainer = () => {
  const authApiClient = useAuthApiClient();
  const { colors } = useCustomTheme();
  const [events, setEvents] = useState<CalendarEvent[]>([]);
  const [selectedEvent, setSelectedEvent] = useState<CalendarEvent | null>(
    null,
  );
  const [selectedPriorities, setSelectedPriorities] = useState<string[]>([
    'High',
    'Medium',
    'Low',
  ]);
  const [hideClosed, setHideClosed] = useState<boolean>(false);
  const [dropdownOpen, setDropdownOpen] = useState<boolean>(false);
  const [currentRange, setCurrentRange] = useState<{ start: Date; end: Date }>({
    start: startOfMonth(new Date()),
    end: endOfMonth(new Date()),
  });

  const priorityMapping: { [key: string]: number } = {
    High: 1,
    Medium: 2,
    Low: 3,
  };

  const { data: fetchedEvents } = useQuery<ApiTask[], Error>({
    queryKey: [
      'events',
      currentRange.start.toISOString(),
      currentRange.end.toISOString(),
      hideClosed,
      JSON.stringify(selectedPriorities),
    ],
    queryFn: async () => {
      const numericPriorities = selectedPriorities.map(
        (priority) => priorityMapping[priority],
      );
      return fetchEvents(
        currentRange.start,
        currentRange.end,
        authApiClient,
        hideClosed,
        numericPriorities,
      );
    },
    refetchOnWindowFocus: false,
    staleTime: 1000 * 60 * 5,
  });

  useEffect(() => {
    if (fetchedEvents && Array.isArray(fetchedEvents)) {
      setEvents(mapTasksToEvents(fetchedEvents));
    }
  }, [fetchedEvents, setEvents]);

  const handleRangeChange = (range: Date[] | { start: Date; end: Date }) => {
    let startDate: Date, endDate: Date;

    if (Array.isArray(range)) {
      // If the range is an array (week or day view), the array contains start and end dates
      startDate = startOfDay(range[0]);
      endDate = endOfDay(range[range.length - 1]);
    } else {
      // In month view, the range is an object with start and end
      startDate = startOfDay(range.start);
      endDate = endOfDay(range.end);
    }

    setCurrentRange({
      start: startDate,
      end: endDate,
    });
  };

  const handlePriorityChange = (priority: string) => {
    setSelectedPriorities((prev) =>
      prev.includes(priority)
        ? prev.filter((p) => p !== priority)
        : [...prev, priority],
    );
  };

  const handleEventClick = (event: CalendarEvent) => {
    setSelectedEvent(event);
  };

  const handleHideClosedToggle = () => {
    setHideClosed(!hideClosed);
  };

  const handlePriorityChangeToggle = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const eventPropGetter = (event: CalendarEvent) => {
    const { backgroundColor, opacity, color } = getEventStyle(event, colors);

    return {
      style: {
        backgroundColor,
        color,
        opacity,
      },
    };
  };

  return (
    <>
      <Calendar
        style={{
          width: '100%',
          height: '100%',
          minWidth: '100px',
          minHeight: '100px',
        }}
        localizer={localizer}
        events={events}
        startAccessor="start"
        endAccessor="end"
        titleAccessor="title"
        onSelectEvent={handleEventClick}
        eventPropGetter={eventPropGetter}
        onRangeChange={handleRangeChange}
        popup={true}
        components={{
          toolbar: (props) => (
            <CalendarToolbar
              {...props}
              selectedPriorities={selectedPriorities}
              priorities={['High', 'Medium', 'Low']}
              hideClosed={hideClosed}
              onHideClosedToggle={handleHideClosedToggle}
              dropdownOpen={dropdownOpen}
              onPriorityChangeToggle={handlePriorityChangeToggle}
              onPriorityChange={handlePriorityChange}
            />
          ),
        }}
      />
      <EventDetailsDialog
        event={selectedEvent}
        onClose={() => setSelectedEvent(null)}
      />
    </>
  );
};

export default CalendarContainer;
