import { render, screen } from '@testing-library/react';

import MaintenanceStats from './MaintenanceStats';

describe('MaintenanceStats Component', () => {
  it('renders completed and not completed stats correctly', () => {
    render(
      <MaintenanceStats
        totalCompletedLastMonth={5}
        totalNotCompletedLastMonth={3}
        numbers={[]}
      />,
    );

    expect(screen.getByTestId('completed-last-month')).toHaveTextContent('5');
    expect(screen.getByTestId('completed-text')).toHaveTextContent(
      'Maintenance actions resolved last month',
    );

    expect(screen.getByTestId('not-completed-last-month')).toHaveTextContent(
      '3',
    );
    expect(screen.getByTestId('not-completed-text')).toHaveTextContent(
      'Maintenance actions to be resolved',
    );
  });

  it('renders "No data available" when numbers array is empty', () => {
    render(<MaintenanceStats numbers={[]} />);

    expect(screen.getByTestId('no-data')).toHaveTextContent(
      'No data available',
    );
    expect(screen.queryByTestId('sparkline-chart')).not.toBeInTheDocument();
  });

  it('renders the SparkLineChart when numbers array has data', () => {
    render(<MaintenanceStats numbers={[1, 2, 3, 4, 5]} />);

    expect(screen.getByTestId('sparkline-chart')).toBeInTheDocument();
    expect(screen.queryByTestId('no-data')).not.toBeInTheDocument();
  });

  it('renders the variation text', () => {
    render(<MaintenanceStats numbers={[1, 2, 3]} />);
    expect(screen.getByTestId('variation-text')).toHaveTextContent(
      'Variation of number of tasks completed per month',
    );
  });
});
