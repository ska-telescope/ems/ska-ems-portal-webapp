/**
 * Maintenance Statistics Component
 *
 * This component displays maintenance data values and a MUI Sparkline of data values.
 */
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { SparkLineChart } from '@mui/x-charts';

import { useCustomTheme } from '@hooks/useCustomTheme.ts';

interface MaintenanceStatsProps {
  totalCompletedLastMonth?: number;
  totalNotCompletedLastMonth?: number;
  numbers?: (number | null)[];
  sparklineCaption?: string;
  noDataMessage?: string;
}

const MaintenanceStats = ({
  totalCompletedLastMonth = 0,
  totalNotCompletedLastMonth = 0,
  numbers = [],
  sparklineCaption = 'Variation of number of tasks completed per month',
  noDataMessage = 'No data available',
}: MaintenanceStatsProps) => {
  const { colors } = useCustomTheme();
  const now = new Date();
  const monthName = now.toLocaleString('default', { month: 'long' });
  const year = now.getFullYear();

  // Validate that 'numbers' is an array of (number | null)
  const validNumbers = numbers.filter(
    (n): n is number => n !== null && typeof n === 'number',
  );

  const hasData = validNumbers.length > 0;

  return (
    <Box
      component="section"
      aria-labelledby="maintenance-stats-title"
      sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: 2,
        width: '100%',
      }}
      data-testid="maintenance-stats-section"
    >
      {/* Numerical Statistics Section */}
      <Box sx={{ flex: 1 }}>
        <Typography
          variant="h2"
          fontWeight="bold"
          sx={{ color: colors.customAccentPrimary }}
          id="maintenance-stats-title"
          data-testid="completed-last-month"
        >
          {totalCompletedLastMonth}
        </Typography>
        <Typography
          variant="h6"
          sx={{ color: colors.textPrimary, marginTop: 1 }}
          data-testid="completed-text"
        >
          Maintenance actions resolved last month
        </Typography>

        <Typography
          variant="h2"
          fontWeight="bold"
          sx={{ color: colors.customAccentPrimary }}
          data-testid="not-completed-last-month"
        >
          {totalNotCompletedLastMonth}
        </Typography>
        <Typography
          variant="h6"
          sx={{ color: colors.textPrimary, marginTop: 1 }}
          data-testid="not-completed-text"
        >
          Maintenance actions to be resolved
        </Typography>

        <Typography
          variant="h6"
          sx={{ color: colors.textSecondary, marginTop: 0.5 }}
          data-testid="month-text"
        >
          {`${monthName} ${year}`}
        </Typography>
      </Box>

      {/* Sparkline Chart Section */}
      <Box
        sx={{
          flex: 1,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        }}
        data-testid="sparkline-section"
      >
        {hasData ? (
          <SparkLineChart
            data={validNumbers}
            height={100}
            showTooltip
            showHighlight
            colors={[colors.customAccentPrimary]}
            data-testid="sparkline-chart"
            aria-label="Sparkline chart showing variation in the number of tasks completed per month"
          />
        ) : (
          <Typography
            variant="body1"
            aria-live="polite"
            data-testid="no-data"
            sx={{
              textAlign: 'center',
              color: colors.textSecondary,
              fontSize: '1rem',
              marginBottom: '1rem',
            }}
          >
            {noDataMessage}
          </Typography>
        )}

        <Typography
          variant="caption"
          sx={{ color: colors.textSecondary, marginTop: 1 }}
          data-testid="variation-text"
        >
          {sparklineCaption}
        </Typography>
      </Box>
    </Box>
  );
};

export default MaintenanceStats;
