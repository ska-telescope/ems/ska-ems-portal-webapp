export interface ProblemReportData {
  id: number;
  label: string;
  value: number;
}

const JIRA_BASE_URL = window.env.JIRA_BASE_URL;

/**
 * Builds the Jira URL for a given problem report status.
 *
 * @param status - The status of the problem report (e.g., "Open", "Resolved").
 * @returns The constructed Jira URL.
 */
export const buildJiraUrl = (status: string): string => {
  return `${JIRA_BASE_URL}/issues/?jql=project%20=%20SPRTS%20AND%20status%20=%20%22${encodeURIComponent(
    status,
  )}%22%20ORDER%20BY%20priority%20DESC,%20updated%20DESC`;
};

/**
 * Handles the click event for a problem report item.
 *
 * @param data - Array of problem report data.
 * @param dataIndex - The index of the clicked data item.
 */
export const handleProblemReportClick = (
  data: ProblemReportData[],
  dataIndex: number,
): void => {
  if (dataIndex >= 0 && dataIndex < data.length) {
    const status = data[dataIndex].label;
    const url = buildJiraUrl(status);
    window.open(url, '_blank', 'noopener,noreferrer');
  }
};
