/**
 * Problem Reports Information Component
 *
 * This component displays problem report information and links.
 */
import { Button } from '@mui/material';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import { useCustomTheme } from '@hooks/useCustomTheme.ts';

const JIRA_BASE_URL = window.env.JIRA_BASE_URL;

const SUMMARY_TRUNCATION_LIMIT = 65;

interface IssueData {
  jira_key?: string;
  summary?: string;
  description?: string;
  url?: string;
}

interface ProblemReportsInfoProps {
  dataLatestIssues?: IssueData[];
}

function ProblemReportsInfo({
  dataLatestIssues = [],
}: Readonly<ProblemReportsInfoProps>) {
  const { colors } = useCustomTheme();
  const hasData = dataLatestIssues.length > 0;

  /**
   * Truncation helper function.
   * If the text is longer than `limit`, it slices it and appends '...'.
   */
  const truncateText = (text: string | undefined, limit: number): string => {
    if (!text) return '';
    return text.length > limit ? `${text.slice(0, limit)}...` : text;
  };

  return (
    <Box
      component="section"
      aria-labelledby="problem-reports-title"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: '100%',
        height: '100%',
      }}
    >
      <Typography
        variant="h6"
        fontWeight="bold"
        data-testid="problem-reports-title"
        sx={{ color: colors.textPrimary, marginBottom: 1, width: '100%' }}
        id="problem-reports-title"
      >
        Latest Problem Reports Raised
      </Typography>

      {hasData ? (
        <Box sx={{ display: 'flex', flexDirection: 'column', gap: 2 }}>
          {dataLatestIssues.map((issue) => {
            // Validate required fields
            if (!issue.jira_key || !issue.summary) {
              return (
                <Typography
                  key={`missing-${issue.jira_key}`}
                  variant="body2"
                  color={colors.textPrimary}
                  data-testid={`issue-missing-fields-${issue.jira_key}`}
                >
                  Missing required fields for this issue.
                </Typography>
              );
            }

            return (
              <Box key={issue.jira_key}>
                <Typography
                  variant="h6"
                  component="a"
                  href={`${JIRA_BASE_URL}/browse/${issue.jira_key}`}
                  target="_blank"
                  rel="noopener noreferrer"
                  data-testid={`issue-link-${issue.jira_key}`}
                  aria-label={`Open Jira issue ${issue.jira_key} in a new tab`}
                  sx={{
                    color: colors.customAccentPrimary,
                    fontWeight: 'bold',
                    textDecoration: 'none',
                    '&:hover': { textDecoration: 'underline' },
                  }}
                >
                  {issue.jira_key}
                </Typography>
                <Typography
                  variant="subtitle1"
                  sx={{ color: colors.textSecondary }}
                  data-testid={`issue-summary-${issue.jira_key}`}
                >
                  {truncateText(issue.summary, SUMMARY_TRUNCATION_LIMIT)}
                </Typography>
              </Box>
            );
          })}
        </Box>
      ) : (
        <Typography
          variant="body1"
          sx={{ color: colors.textSecondary, marginBottom: 2 }}
          data-testid="no-issues-message"
        >
          No problem reports available at the moment.
        </Typography>
      )}

      <Button
        variant="text"
        href={`${JIRA_BASE_URL}/secure/Dashboard.jspa?selectPageId=15600`}
        target="_blank"
        rel="noopener noreferrer"
        data-testid="see-all-issues-button"
        aria-label="Open Jira dashboard in a new tab"
        sx={{
          textTransform: 'none',
          alignSelf: 'flex-start',
          width: '100%',
          color: colors.customAccentPrimary,
        }}
      >
        See all issues
      </Button>
    </Box>
  );
}

export default ProblemReportsInfo;
