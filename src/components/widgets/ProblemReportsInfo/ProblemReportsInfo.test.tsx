import { render, screen } from '@testing-library/react';

import ProblemReportsInfo from './ProblemReportsInfo';

const mockData = [
  {
    jira_key: 'PRJ-123',
    summary:
      'An issue summary that is longer than fifty characters for testing purposes',
    description: '',
    url: '',
  },
  {
    jira_key: 'PRJ-456',
    summary: 'A short summary',
    description: '',
    url: '',
  },
];

describe('ProblemReportsInfo', () => {
  test('renders the title', () => {
    render(<ProblemReportsInfo />);
    const title = screen.getByTestId('problem-reports-title');
    expect(title).toBeInTheDocument();
    expect(title).toHaveTextContent('Latest Problem Reports Raised');
  });

  test('renders a list of issues with correct links and truncated summaries', () => {
    render(<ProblemReportsInfo dataLatestIssues={mockData} />);

    const firstIssueLink = screen.getByTestId('issue-link-PRJ-123');
    expect(firstIssueLink).toHaveTextContent('PRJ-123');
    expect(firstIssueLink).toHaveAttribute('href', 'undefined/browse/PRJ-123');

    const firstIssueSummary = screen.getByTestId('issue-summary-PRJ-123');
    expect(firstIssueSummary).toHaveTextContent(
      'An issue summary that is longer than fifty characters for testing...',
    );

    const secondIssueLink = screen.getByTestId('issue-link-PRJ-456');
    expect(secondIssueLink).toHaveTextContent('PRJ-456');
    expect(secondIssueLink).toHaveAttribute('href', 'undefined/browse/PRJ-456');

    const secondIssueSummary = screen.getByTestId('issue-summary-PRJ-456');
    expect(secondIssueSummary).toHaveTextContent('A short summary');
  });

  test('renders "See all issues" button with correct link', () => {
    render(<ProblemReportsInfo />);

    const seeAllButton = screen.getByTestId('see-all-issues-button');
    expect(seeAllButton).toBeInTheDocument();
    expect(seeAllButton).toHaveAttribute(
      'href',
      'undefined/secure/Dashboard.jspa?selectPageId=15600',
    );
  });
});
