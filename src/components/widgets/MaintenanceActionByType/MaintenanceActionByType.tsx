/**
 * Maintenance Action By Type Component
 *
 * This component displays maintenance status data in an AG Chart with horizontal bars.
 */
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import 'ag-grid-enterprise';
// eslint-disable-next-line import/named
import { AgCartesianChartOptions } from 'ag-charts-enterprise';
import { AgCharts } from 'ag-charts-react';
import { useMemo } from 'react';

import { useAgChartTheme } from '@hooks/useAgChartTheme.ts';

// TODO: Simplify the logic by evaluating hasData directly within the useMemo block.
// TODO: Validate maintenanceChartData structure to handle malformed or partial data.
// TODO: Generate dynamic chart descriptions to summarize data for better accessibility.
// TODO: Implement dynamic label rotation or truncation for category axis if data density increases.
interface MaintenanceActionByTypeProps {
  maintenanceChartData?: {
    month: string;
    preventative: number;
    unplanned: number;
    planned: number;
    workRequests: number;
    materials: number;
  }[];
}

const BASE_SERIES_CONFIG = {
  direction: 'horizontal' as const,
  type: 'bar' as const,
  stacked: true,
  grouped: true,
  label: {
    formatter: ({ value }: { value: number }) => value.toFixed(0),
  },
};

const MaintenanceActionByType = ({
  maintenanceChartData = [],
}: MaintenanceActionByTypeProps) => {
  const agChartTheme = useAgChartTheme();
  const hasData = Boolean(maintenanceChartData?.length);

  const chartOptions = useMemo<AgCartesianChartOptions | null>(() => {
    if (!hasData) {
      return null;
    }

    return {
      data: maintenanceChartData,
      subtitle: {
        text: 'Counts of maintenance actions by type',
      },
      series: [
        {
          ...BASE_SERIES_CONFIG,
          xKey: 'month',
          yKey: 'planned',
          yName: 'Planned Maintenance',
        },
        {
          ...BASE_SERIES_CONFIG,
          xKey: 'month',
          yKey: 'unplanned',
          yName: 'Unplanned Maintenance',
        },
        {
          ...BASE_SERIES_CONFIG,
          xKey: 'month',
          yKey: 'preventative',
          yName: 'Preventative Maintenance',
        },
        {
          ...BASE_SERIES_CONFIG,
          xKey: 'month',
          yKey: 'workRequests',
          yName: 'Work Requests',
        },
        {
          ...BASE_SERIES_CONFIG,
          xKey: 'month',
          yKey: 'materials',
          yName: 'Materials Request',
        },
      ],
      axes: [
        {
          type: 'category',
          position: 'left',
          label: {
            rotation: 0,
          },
        },
        {
          type: 'number',
          position: 'bottom',
        },
      ],
      background: {
        visible: false,
      },
      theme: agChartTheme,
    };
  }, [maintenanceChartData, hasData, agChartTheme]);

  return (
    <Box
      aria-labelledby="chart-title"
      sx={{
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        gap: 2,
        width: '100%',
        maxWidth: '100%',
      }}
    >
      <Typography
        variant="h6"
        fontWeight="bold"
        id="chart-title"
        sx={{ color: 'textPrimary', marginBottom: 1, width: '100%' }}
        data-testid="chart-title"
      >
        Monthly Maintenance Actions Distribution by Type
      </Typography>
      {hasData && chartOptions ? (
        <Box
          sx={{
            flex: 1,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            height: '100%',
          }}
        >
          <AgCharts
            data-testid="ag-charts-component"
            aria-label="Bar chart showing monthly maintenance actions by type"
            options={chartOptions}
            style={{ width: '100%', height: '100%' }}
          />
        </Box>
      ) : (
        <Typography
          variant="body1"
          sx={{ color: 'text.secondary', textAlign: 'center', marginTop: 2 }}
          data-testid="no-data-message"
        >
          No data available for maintenance actions.
        </Typography>
      )}
    </Box>
  );
};

export default MaintenanceActionByType;
