import { render, screen } from '@testing-library/react';

import StatusOfProblemReports from './StatusOfProblemReports';

const mockData = [
  { id: 1, label: 'Open', value: 40 },
  { id: 2, label: 'In Progress', value: 30 },
  { id: 3, label: 'Resolved', value: 30 },
];

const mockOnItemClick = vi.fn();

describe('StatusOfProblemReports', () => {
  beforeEach(() => {
    vi.clearAllMocks();
  });

  test('renders the title', () => {
    render(
      <StatusOfProblemReports data={mockData} onItemClick={mockOnItemClick} />,
    );

    const title = screen.getByTestId('problem-reports-title');
    expect(title).toBeInTheDocument();
    expect(title).toHaveTextContent('Status of Open Problem Reports');
  });

  test('renders the PieChart inside the container', () => {
    render(
      <StatusOfProblemReports data={mockData} onItemClick={mockOnItemClick} />,
    );

    const container = screen.getByTestId('problem-reports-chart-container');
    expect(container).toBeInTheDocument();

    const pieChart = screen.getByTestId('problem-reports-pie-chart');
    expect(pieChart).toBeInTheDocument();
  });

  test('renders correctly with empty data', () => {
    render(<StatusOfProblemReports data={[]} onItemClick={mockOnItemClick} />);

    const pieChart = screen.getByTestId('problem-reports-no-data');
    expect(pieChart).toBeInTheDocument();
  });
});
