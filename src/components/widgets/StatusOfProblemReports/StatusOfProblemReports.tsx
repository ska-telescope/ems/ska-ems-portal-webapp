/**
 * Status Of Problem Reports Component
 *
 * This component displays problem report status data in MUI Pie Chart with on Click links to Jira.
 */
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { PieChart } from '@mui/x-charts';
import React, { useMemo } from 'react';

import { useCustomTheme } from '@hooks/useCustomTheme.ts';

interface ClickEventData {
  dataIndex: number;
}

interface StatusOfProblemReportsProps {
  data: { id: number; label: string; value: number }[];
  onItemClick: (event: React.MouseEvent, d: ClickEventData) => void;
}

function StatusOfProblemReports({
  data = [],
  onItemClick = () => {},
}: Readonly<StatusOfProblemReportsProps>) {
  const { colors } = useCustomTheme();

  const colorPalette = useMemo(
    () => [
      colors.customAccentPrimary,
      colors.customAccentQuinary,
      colors.customAccentSecondary,
      colors.customAccentTertiary,
      colors.customAccentQuaternary,
    ],
    [colors],
  );

  const pieData = React.useMemo(
    () =>
      data.map((item, index) => ({
        ...item,
        color: colorPalette[index % colorPalette.length], // Cycle through colors
      })),
    [data, colorPalette],
  );

  const hasData = Array.isArray(pieData) && pieData.length > 0;

  const handleItemClick = (event: React.MouseEvent, d: ClickEventData) => {
    if (event && typeof d?.dataIndex === 'number') {
      onItemClick(event, d);
    }
  };

  return (
    <>
      <Typography
        variant="h6"
        fontWeight="bold"
        data-testid="problem-reports-title"
        sx={{ color: colors.textPrimary, marginBottom: 1, width: '100%' }}
        id="problem-reports-title"
      >
        Status of Open Problem Reports
      </Typography>
      <Box
        data-testid="problem-reports-chart-container"
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          width: '100%',
          height: '250px',
        }}
        aria-label="Status of Open Problem Reports Chart"
        aria-describedby="problem-reports-title"
      >
        {hasData ? (
          <PieChart
            data-testid="problem-reports-pie-chart"
            series={[
              {
                data: pieData,
                innerRadius: 50,
                highlightScope: {
                  faded: 'global',
                  highlighted: 'item',
                },
              },
            ]}
            slotProps={{ legend: { hidden: true } }}
            onItemClick={handleItemClick}
            margin={{ right: 0 }}
            aria-label="Pie chart of problem report statuses"
          />
        ) : (
          <Typography
            variant="body1"
            sx={{ color: 'text.secondary' }}
            data-testid="problem-reports-no-data"
          >
            No data available for problem report statuses.
          </Typography>
        )}
      </Box>
    </>
  );
}

export default StatusOfProblemReports;
