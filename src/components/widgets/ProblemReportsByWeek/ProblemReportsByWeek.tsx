/**
 * Problem Reports By Week Component
 *
 * This component displays problem report data in Responsive MUI Chart.
 */
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {
  ChartsAxisHighlight,
  ChartsGrid,
  ChartsLegend,
  ChartsTooltip,
  ChartsXAxis,
  ChartsYAxis,
  LineHighlightPlot,
  LinePlot,
  LineSeriesType,
  MarkPlot,
  ResponsiveChartContainer,
} from '@mui/x-charts';
import { DatasetElementType } from '@mui/x-charts/internals';
import { format } from 'date-fns';

import { useCustomTheme } from '@hooks/useCustomTheme.ts';

interface ProblemReportsByWeekProps {
  chartDataset: DatasetElementType<string | number | Date | null | undefined>[];
}

// TODO: Refactor chartDataset typing when upgrading to ag-charts to ensure type safety and match the expected structure.
// TODO: Add a utility function to validate and handle missing or invalid data in chartDataset for robust empty state handling.
// TODO: Pass a dynamic locale to format date values in the xAxis for better internationalization support.
const ProblemReportsByWeek = ({
  chartDataset = [],
}: ProblemReportsByWeekProps) => {
  const { colors } = useCustomTheme();

  const hasData = chartDataset.length > 0;

  // Shared configuration for series
  const baseSeriesConfig: Omit<LineSeriesType, 'id' | 'label' | 'dataKey'> = {
    type: 'line',
    curve: 'stepAfter',
    showMark: true,
    highlightScope: { highlight: 'series' },
  };

  return (
    <Box
      component="section"
      aria-labelledby="chart-title"
      aria-label="Problem reports by week"
      sx={{
        width: '100%',
        height: '100%',
        maxHeight: 500,
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Typography
        variant="h6"
        fontWeight="bold"
        id="chart-title"
        data-testid="chart-title"
        sx={{
          color: colors.textPrimary,
          marginBottom: 1,
          width: '100%',
        }}
      >
        Problem Reports by Week
      </Typography>

      {hasData ? (
        <ResponsiveChartContainer
          dataset={chartDataset}
          series={[
            {
              ...baseSeriesConfig,
              id: 'Created',
              label: 'Created',
              dataKey: 'created',
              color: colors.customAccentPrimary,
            },
            {
              ...baseSeriesConfig,
              id: 'Closed',
              label: 'Closed',
              dataKey: 'resolved',
              color: colors.customAccentSecondary,
            },
          ]}
          xAxis={[
            {
              scaleType: 'time',
              dataKey: 'x',
              label: 'Week Start Date',
              valueFormatter: (value) =>
                value instanceof Date
                  ? format(value, 'MMM dd, yyyy')
                  : String(value),
            },
          ]}
          yAxis={[]}
          data-testid="responsive-chart"
        >
          <ChartsGrid horizontal />
          <LinePlot />
          <ChartsXAxis />
          <ChartsYAxis />
          <MarkPlot />
          <ChartsLegend data-testid="charts-legend" />
          <LineHighlightPlot />
          <ChartsAxisHighlight x="line" />
          <ChartsTooltip trigger="axis" />
        </ResponsiveChartContainer>
      ) : (
        <Typography
          variant="body1"
          sx={{ color: 'text.secondary', marginBottom: 2 }}
          data-testid="no-data-message"
        >
          No data available for problem reports.
        </Typography>
      )}
    </Box>
  );
};

export default ProblemReportsByWeek;
