import { render, screen, within } from '@testing-library/react';

import ProblemReportsByWeek from './ProblemReportsByWeek';

const mockDataset = [
  { x: new Date('2024-11-01'), created: 10, resolved: 5 },
  { x: new Date('2024-11-08'), created: 8, resolved: 6 },
];

describe('ProblemReportsByWeek Component', () => {
  it('renders the chart title', () => {
    render(<ProblemReportsByWeek chartDataset={mockDataset} />);
    expect(screen.getByTestId('chart-title')).toHaveTextContent(
      'Problem Reports by Week',
    );
  });

  it('renders the ResponsiveChartContainer', () => {
    render(<ProblemReportsByWeek chartDataset={mockDataset} />);
    expect(screen.getByTestId('responsive-chart')).toBeInTheDocument();
  });

  it('renders x-axis labels', () => {
    render(<ProblemReportsByWeek chartDataset={mockDataset} />);
    const chartContainer = screen.getByTestId('responsive-chart');
    expect(
      within(chartContainer).getByText('Nov 03, 2024'),
    ).toBeInTheDocument();
  });

  it('handles an empty dataset without crashing', () => {
    render(<ProblemReportsByWeek chartDataset={[]} />);
    expect(screen.getByTestId('no-data-message')).toBeInTheDocument();
  });
});
