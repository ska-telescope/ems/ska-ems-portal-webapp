// eslint-disable-next-line import/named
import { CellClassParams, GridOptions } from 'ag-grid-community';
import { AgGridReact } from 'ag-grid-react';
import { useEffect, useState, useMemo } from 'react';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

/** Interface for the component props */
interface DocumentsProps {
  assetId: string;
}

/** Interface defining the structure of individual document items */
interface DocumentItem {
  id: number;
  data_module_code: string;
  description: string;
  url: string;
}

const DATA_ENDPOINT = `${window.env.EMS_BACKEND_URL}/assets`;

/**
 * A component that displays documents (data modules) for a specific asset using AG Grid.
 *
 * @component
 * @param {DocumentsProps} props - Component props
 * @param {string} props.assetId - Unique identifier for fetching asset-specific documents
 * @returns {JSX.Element} Rendered grid component
 */
const Documents = ({ assetId }: DocumentsProps) => {
  const [rowData, setRowData] = useState<DocumentItem[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          `${DATA_ENDPOINT}/${assetId}/data-modules`,
        );
        if (!response.ok) {
          throw new Error(
            `Server error: ${response.status} ${response.statusText}`,
          );
        }
        const data: DocumentItem[] = await response.json();
        setRowData(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, [assetId]);

  /** Grid configuration options */
  const gridOptions: GridOptions = useMemo(
    () => ({
      columnDefs: [
        {
          field: 'data_module_code',
          headerName: 'Data Module Code',
          sortable: true,
          filter: 'agTextColumnFilter',
          flex: 1,
          minWidth: 150,
        },
        {
          field: 'description',
          headerName: 'Description',
          sortable: true,
          filter: 'agTextColumnFilter',
          flex: 2,
          minWidth: 200,
        },
        {
          field: 'url',
          headerName: 'URL',
          sortable: false,
          filter: false,
          cellRenderer: (params: CellClassParams) => (
            <a href={params.value} target="_blank" rel="noopener noreferrer">
              Open Document
            </a>
          ),
          minWidth: 150,
        },
      ],
      defaultColDef: {
        resizable: true,
        sortable: true,
        filter: true,
        minWidth: 100,
      },
      pagination: true,
      paginationPageSize: 20,
    }),
    [],
  );

  return (
    <div
      className="ag-theme-alpine"
      style={{ height: '600px', width: '100%' }}
      data-testid="documents-grid-container"
    >
      <AgGridReact
        gridOptions={gridOptions}
        rowData={rowData}
        data-testid="documents-grid"
      />
    </div>
  );
};

export default Documents;
