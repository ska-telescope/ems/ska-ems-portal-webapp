// eslint-disable-next-line import/named
import { GridOptions } from 'ag-grid-community';
import { AgGridReact } from 'ag-grid-react';
import { useEffect, useState, useMemo } from 'react';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

/** Interface for the component props */
interface WorkOrdersProps {
  assetId: string;
}

/** Interface defining the structure of individual work order items */
interface WorkOrderItem {
  id: number;
  name: string;
  description: string;
  status: string;
  priority: string;
  due_date: string;
  assigned_to: string;
}

const DATA_ENDPOINT = `${window.env.EMS_BACKEND_URL}/assets`;

/**
 * A component that displays work orders for a specific asset using AG Grid.
 *
 * @component
 * @param {WorkOrdersProps} props - Component props
 * @param {string} props.assetId - Unique identifier for fetching asset-specific work orders
 * @returns {JSX.Element} Rendered grid component
 */
const WorkOrders = ({ assetId }: WorkOrdersProps) => {
  const [rowData, setRowData] = useState<WorkOrderItem[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`${DATA_ENDPOINT}/${assetId}/work-orders`);
        if (!response.ok) {
          throw new Error(
            `Server error: ${response.status} ${response.statusText}`,
          );
        }
        const data: WorkOrderItem[] = await response.json();
        setRowData(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, [assetId]);

  /** Grid configuration options */
  const gridOptions: GridOptions = useMemo(
    () => ({
      columnDefs: [
        {
          field: 'name',
          headerName: 'Name',
          sortable: true,
          filter: 'agTextColumnFilter',
          flex: 1,
          minWidth: 150,
        },
        {
          field: 'description',
          headerName: 'Description',
          sortable: true,
          filter: 'agTextColumnFilter',
          flex: 2,
          minWidth: 200,
        },
        {
          field: 'status',
          headerName: 'Status',
          sortable: true,
          filter: 'agTextColumnFilter',
          minWidth: 120,
        },
        {
          field: 'priority',
          headerName: 'Priority',
          sortable: true,
          filter: 'agTextColumnFilter',
          minWidth: 120,
        },
        {
          field: 'due_date',
          headerName: 'Due Date',
          sortable: true,
          filter: 'agDateColumnFilter',
          minWidth: 150,
          valueFormatter: (params) => {
            if (!params.value) return ''; // Display blank if date is null
            const date = new Date(params.value * 1000); // Convert UNIX timestamp to JavaScript date
            return date.toLocaleDateString(); // Format the date
          },
        },
        {
          field: 'assigned_to',
          headerName: 'Assigned To',
          sortable: true,
          filter: 'agTextColumnFilter',
          minWidth: 150,
        },
      ],
      defaultColDef: {
        resizable: true,
        sortable: true,
        filter: true,
        minWidth: 100,
      },
      pagination: true,
      paginationPageSize: 20,
    }),
    [],
  );

  return (
    <div
      className="ag-theme-alpine"
      style={{ height: '600px', width: '100%' }}
      data-testid="work-orders-grid-container"
    >
      <AgGridReact
        gridOptions={gridOptions}
        rowData={rowData}
        data-testid="work-orders-grid"
      />
    </div>
  );
};

export default WorkOrders;
