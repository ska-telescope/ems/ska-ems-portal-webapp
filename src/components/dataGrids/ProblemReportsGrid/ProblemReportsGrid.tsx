// eslint-disable-next-line import/named
import { CellClassParams, GridOptions } from 'ag-grid-community';
import { AgGridReact } from 'ag-grid-react';
import { useEffect, useState, useMemo } from 'react';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

/** Interface for the component props */
interface DocumentsProps {
  assetId: string;
}

/** Interface defining the structure of individual document items */
interface ProblemReportsItem {
  id: number;
  data_module_code: string;
  description: string;
  url: string;
}

const DATA_ENDPOINT = `${window.env.EMS_BACKEND_URL}/assets`;

/**
 * A component that displays documents (data modules) for a specific asset using AG Grid.
 *
 * @component
 * @param {DocumentsProps} props - Component props
 * @param {string} props.assetId - Unique identifier for fetching asset-specific documents
 * @returns {JSX.Element} Rendered grid component
 */
const ProblemReports = ({ assetId }: DocumentsProps) => {
  const [rowData, setRowData] = useState<ProblemReportsItem[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`${DATA_ENDPOINT}/${assetId}/issues`);
        if (!response.ok) {
          throw new Error(
            `Server error: ${response.status} ${response.statusText}`,
          );
        }
        const data: ProblemReportsItem[] = await response.json();
        setRowData(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, [assetId]);

  /** Grid configuration options */
  const gridOptions: GridOptions = useMemo(
    () => ({
      columnDefs: [
        {
          field: 'jira_key',
          headerName: 'Jira Key',
          sortable: true,
          filter: 'agTextColumnFilter',
          flex: 1,
          minWidth: 150,
        },
        {
          field: 'summary',
          headerName: 'Summary',
          sortable: true,
          filter: 'agTextColumnFilter',
          flex: 1,
          minWidth: 150,
        },
        {
          field: 'description',
          headerName: 'Description',
          sortable: true,
          filter: 'agTextColumnFilter',
          flex: 2,
          minWidth: 200,
        },
        {
          field: 'status',
          headerName: 'Status',
          sortable: true,
          filter: 'agTextColumnFilter',
          flex: 1,
          minWidth: 100,
        },
        {
          field: 'resolution',
          headerName: 'Resolution',
          sortable: true,
          filter: 'agTextColumnFilter',
          flex: 1,
          minWidth: 100,
        },
        {
          field: 'created_date',
          headerName: 'Created Date',
          sortable: true,
          filter: 'agDateColumnFilter',
          valueFormatter: (params) => {
            if (!params.value) return '';
            const date = new Date(params.value * 1000);
            return date.toLocaleDateString();
          },
          minWidth: 150,
        },
        {
          field: 'resolution_date',
          headerName: 'Resolution Date',
          sortable: true,
          filter: 'agDateColumnFilter',
          valueFormatter: (params) => {
            if (!params.value) return '';
            const date = new Date(params.value * 1000);
            return date.toLocaleDateString();
          },
          minWidth: 150,
        },
        {
          field: 'url',
          headerName: 'URL',
          sortable: false,
          filter: false,
          cellRenderer: (params: CellClassParams) => (
            <a href={params.value} target="_blank" rel="noopener noreferrer">
              Open Document
            </a>
          ),
          minWidth: 150,
        },
      ],
      defaultColDef: {
        resizable: true,
        sortable: true,
        filter: true,
        minWidth: 100,
      },
      pagination: true,
      paginationPageSize: 20,
    }),
    [],
  );

  return (
    <div
      className="ag-theme-alpine"
      style={{ height: '600px', width: '100%' }}
      data-testid="documents-grid-container"
    >
      <AgGridReact
        gridOptions={gridOptions}
        rowData={rowData}
        data-testid="documents-grid"
      />
    </div>
  );
};

export default ProblemReports;
