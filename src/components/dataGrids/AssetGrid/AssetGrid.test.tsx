import { render, screen } from '@testing-library/react';
import { describe, expect, it, vi } from 'vitest';

import AssetGrid from './AssetGrid';

// Mock environment variable
vi.mock('import.meta', () => ({
  env: {
    EMS_BACKEND_URL: 'http://test-api',
  },
}));

// Mock AG Grid React
vi.mock('ag-grid-react', () => ({
  AgGridReact: () => <div data-testid="mocked-ag-grid">AG Grid</div>,
}));

describe('AssetGrid', () => {
  // Create a mock for the onActionClick function
  const mockOnActionClick = vi.fn();

  it('renders the grid container with correct classes and styles', () => {
    render(
      <AssetGrid
        locationId="test-location"
        onActionClick={mockOnActionClick}
      />,
    );

    const gridContainer = screen.getByTestId('asset-grid-container');
    expect(gridContainer).toBeInTheDocument();
    expect(gridContainer).toHaveClass('ag-theme-alpine');
    expect(gridContainer).toHaveStyle({
      height: '600px',
      width: '100%',
    });
  });

  it('renders AG Grid component', () => {
    render(
      <AssetGrid
        locationId="test-location"
        onActionClick={mockOnActionClick}
      />,
    );

    const grid = screen.getByTestId('mocked-ag-grid');
    expect(grid).toBeInTheDocument();
  });

  it('includes required column definitions', () => {
    render(
      <AssetGrid
        locationId="test-location"
        onActionClick={mockOnActionClick}
      />,
    );

    // We can verify the component renders without errors
    expect(screen.getByTestId('asset-grid-container')).toBeInTheDocument();
  });
});
