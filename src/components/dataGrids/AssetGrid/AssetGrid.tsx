/* eslint-disable import/named */
import ReceiptLongIcon from '@mui/icons-material/ReceiptLong';
import { IconButton } from '@mui/material';
import {
  CellClassParams,
  GridOptions,
  IServerSideDatasource,
  IServerSideGetRowsParams,
} from 'ag-grid-community';
import { AgGridReact } from 'ag-grid-react';
import { useCallback, useMemo, useRef } from 'react';

import useAuthApiClient from '@/auth/hooks/useAuthApiClient.ts';
import StatusChip from '@/components/dataGrids/common/GridChip/StatusChip';
import GridIcon from '@/components/dataGrids/common/GridIcon/GridIcon';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

/** Interface for the component props */
interface AssetGridProps {
  locationId: string;
  onActionClick: (assetId: string) => void;
}

/** Interface defining the structure of individual asset items */
interface AssetItem {
  id: string;
  name: string;
  part_number: string | null;
  version: string | null;
  serial_number: string | null;
  topological_name: string | null;
  type: string | null;
  status: string;
  has_children: boolean;
  children: AssetItem[];
}

/** Interface for grid row data, excluding the children array */
interface RowDataItem extends Omit<AssetItem, 'children'> {
  has_children: boolean;
}

const DATA_ENDPOINT = `${window.env.EMS_BACKEND_URL}/locations`;

const AssetGrid = ({ locationId, onActionClick }: AssetGridProps) => {
  const authApiClient = useAuthApiClient();
  const gridRef = useRef<AgGridReact>(null);
  /** Server-side datasource implementation */
  const datasource: IServerSideDatasource = useMemo(() => {
    return {
      getRows: async (params: IServerSideGetRowsParams) => {
        try {
          const response = await authApiClient(
            `${DATA_ENDPOINT}/${locationId}/assets`,
            {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(params.request),
            },
          );

          if (!response.ok) {
            throw new Error(
              `Server error: ${response.status} ${response.statusText}`,
            );
          }

          const data: AssetItem[] = await response.json();

          if (!Array.isArray(data)) {
            params.fail();
            if (gridRef.current?.api) {
              gridRef.current.api.showNoRowsOverlay();
            }
            return;
          }

          /** Transform server data to grid row format */
          const rowData: RowDataItem[] = data.map((item: AssetItem) => ({
            ...item,
            group: item.has_children,
          }));

          if (rowData.length === 0) {
            params.success({ rowData: [] });
            if (gridRef.current?.api) {
              gridRef.current.api.showNoRowsOverlay();
            }
          } else {
            params.success({ rowData });
            if (gridRef.current?.api) {
              gridRef.current.api.hideOverlay();
            }
          }
        } catch (error) {
          console.error('Error fetching data:', error);
          params.fail();
          if (gridRef.current?.api) {
            gridRef.current.api.showNoRowsOverlay();
          }
        }
      },
    };
  }, [locationId]);

  /** Helper function to grey out text for rows with 'Missing' status */
  const getGreyedTextStyle = (params: CellClassParams) => {
    if (params.data?.status === 'Missing') {
      return { color: '#666666' };
    }
    return undefined;
  };

  /**
   * Handles click on the action button to add new tabs
   * @param {string} assetId - The ID of the asset for which tabs are added
   */
  const handleAddTabs = useCallback(
    (assetId: string) => {
      onActionClick(assetId);
    },
    [onActionClick],
  );

  /** Grid configuration options */
  const gridOptions: GridOptions = useMemo(
    () => ({
      columnDefs: [
        {
          field: 'name',
          headerName: 'Name',
          cellRenderer: 'agGroupCellRenderer',
          cellRendererParams: {
            innerRenderer: GridIcon,
          },
          cellStyle: getGreyedTextStyle,
          sortable: true,
          flex: 2,
          minWidth: 200,
        },
        {
          field: 'part_number',
          headerName: 'Part Number',
          cellStyle: getGreyedTextStyle,
        },
        {
          field: 'version',
          headerName: 'Version',
          cellStyle: getGreyedTextStyle,
        },
        {
          field: 'serial_number',
          headerName: 'Serial Number',
          cellStyle: getGreyedTextStyle,
        },
        {
          field: 'topological_name',
          headerName: 'Topological Name',
          cellStyle: getGreyedTextStyle,
        },
        {
          field: 'status',
          headerName: 'Status',
          cellRenderer: StatusChip,
          cellStyle: getGreyedTextStyle,
          width: 120,
        },
        {
          headerName: 'Action',
          // headerComponent: () => <ReceiptLongIcon />,
          cellRenderer: (params: CellClassParams) => (
            <IconButton
              color="primary"
              onClick={() => handleAddTabs(params.data.id)}
            >
              <ReceiptLongIcon />
            </IconButton>
          ),
          maxWidth: 100,
        },
      ],

      defaultColDef: {
        flex: 1,
        sortable: true,
        resizable: true,
        minWidth: 100,
      },

      rowModelType: 'serverSide',
      serverSideDatasource: datasource,
      treeData: true,
      animateRows: true,
      getRowId: (params: { data: RowDataItem }) => params.data.id,
      isServerSideGroup: (dataItem: RowDataItem) => dataItem.has_children,
      getServerSideGroupKey: (dataItem: RowDataItem) => dataItem.id,
      suppressAggFuncInHeader: true,
      groupDisplayType: 'groupRows',
    }),
    [datasource, handleAddTabs],
  );

  return (
    <div
      className="ag-theme-alpine"
      style={{ height: '600px', width: '100%' }}
      data-testid="asset-grid-container"
    >
      <AgGridReact
        ref={gridRef}
        gridOptions={gridOptions}
        data-testid="asset-grid"
      />
    </div>
  );
};

export default AssetGrid;
