import { Settings, Build } from '@mui/icons-material';
// eslint-disable-next-line import/named
import { ICellRendererParams } from 'ag-grid-community';

/**
 * A cell renderer component that displays type-based icon with text
 *
 * @component
 * @param {ICellRendererParams} props - AG Grid cell renderer parameters
 * @param {number} props.data.type - Determines which icon to display (1 for Settings, 2 for Build)
 * @param {string} props.value - The text to display next to the icon
 * @returns {JSX.Element} Rendered icon with text
 *
 * Features:
 * - Displays different MUI icons based on type (1 or 2)
 * - Maintains vertical alignment with text
 * - Provides consistent spacing between icon and text
 */
const GridIcon = (props: ICellRendererParams) => {
  /** Returns the appropriate icon component based on type value */
  const getIcon = () => {
    switch (props.data.type) {
      case 1:
        return (
          <Settings
            data-testid="grid-icon-settings"
            sx={{
              fontSize: 16,
              verticalAlign: 'middle',
              position: 'relative',
              top: '-1px', // Adjusts vertical alignment with text
            }}
            style={{ marginRight: '8px' }} // Spacing between icon and text
          />
        );
      case 2:
        return (
          <Build
            data-testid="grid-icon-build"
            sx={{
              fontSize: 16,
              verticalAlign: 'middle',
              position: 'relative',
              top: '-1px', // Adjusts vertical alignment with text
            }}
            style={{ marginRight: '8px' }} // Spacing between icon and text
          />
        );
      default:
        return null;
    }
  };
  return (
    <span data-testid="grid-icon-container">
      {getIcon()}
      <span data-testid="grid-icon-text">{props.value}</span>
    </span>
  );
};
export default GridIcon;
