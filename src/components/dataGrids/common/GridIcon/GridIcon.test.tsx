import { render, screen } from '@testing-library/react';
import type { ICellRendererParams } from 'ag-grid-community';
import { describe, expect, it } from 'vitest';

import GridIcon from './GridIcon';

// Mock MUI icons
vi.mock('@mui/icons-material', () => ({
  Settings: () => <div data-testid="grid-icon-settings">Settings Icon</div>,
  Build: () => <div data-testid="grid-icon-build">Build Icon</div>,
}));

describe('GridIcon', () => {
  const createProps = (
    value: string,
    type: number,
  ): ICellRendererParams<any, any, any> => ({
    value,
    data: { type },
    node: {} as any,
    colDef: {} as any,
    column: {} as any,
    api: {} as any,
    context: undefined,
    valueFormatted: null,
    eGridCell: document.createElement('div'),
    eParentOfValue: document.createElement('div'),
    formatValue: () => '',
    registerRowDragger: () => null,
    setTooltip: () => null,
  });

  it('renders Settings icon with text when type is 1', () => {
    const props = createProps('Settings Text', 1);
    render(<GridIcon {...props} />);

    const container = screen.getByTestId('grid-icon-container');
    const icon = screen.getByTestId('grid-icon-settings');
    const text = screen.getByTestId('grid-icon-text');

    expect(container).toBeInTheDocument();
    expect(icon).toBeInTheDocument();
    expect(text).toHaveTextContent('Settings Text');
    expect(screen.queryByTestId('grid-icon-build')).not.toBeInTheDocument();
  });

  it('renders Build icon with text when type is 2', () => {
    const props = createProps('Build Text', 2);
    render(<GridIcon {...props} />);

    const container = screen.getByTestId('grid-icon-container');
    const icon = screen.getByTestId('grid-icon-build');
    const text = screen.getByTestId('grid-icon-text');

    expect(container).toBeInTheDocument();
    expect(icon).toBeInTheDocument();
    expect(text).toHaveTextContent('Build Text');
    expect(screen.queryByTestId('grid-icon-settings')).not.toBeInTheDocument();
  });

  it('renders only text when type is invalid', () => {
    const props = createProps('Only Text', 3);
    render(<GridIcon {...props} />);

    const container = screen.getByTestId('grid-icon-container');
    const text = screen.getByTestId('grid-icon-text');

    expect(container).toBeInTheDocument();
    expect(text).toHaveTextContent('Only Text');
    expect(screen.queryByTestId('grid-icon-settings')).not.toBeInTheDocument();
    expect(screen.queryByTestId('grid-icon-build')).not.toBeInTheDocument();
  });
});
