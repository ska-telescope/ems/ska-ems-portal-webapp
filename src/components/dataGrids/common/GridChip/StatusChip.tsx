import { CheckCircle, Error } from '@mui/icons-material';
import { Chip } from '@mui/material';
// eslint-disable-next-line import/named
import { ICellRendererParams } from 'ag-grid-community';

/**
 * A cell renderer component that displays status as a styled MUI Chip
 *
 * @component
 * @param {ICellRendererParams} props - AG Grid cell renderer parameters
 * @param {string} props.value - The status value ('OK' or 'Missing')
 * @returns {JSX.Element | null} Rendered chip with appropriate styling and icon, or null if no status
 *
 * Features:
 * - Displays outlined MUI Chip with status-based styling
 * - Green with CheckCircle icon for 'OK' status
 * - Grey with Error icon for 'Missing' status
 * - Renders nothing if status is not provided
 */
const StatusChip = (props: ICellRendererParams) => {
  const { value } = props;

  // If there is no status, render nothing
  if (!value) {
    return null;
  }

  /** Returns style configuration based on row status */
  const getChipStyles = () => {
    switch (value) {
      case 'OK':
        return {
          color: '#1e4620',
          borderColor: '#1e4620',
          fontSize: '13px',
        };
      case 'Missing':
        return {
          color: '#666666',
          borderColor: '#666666',
          fontSize: '13px',
        };
      default:
        return {
          color: '#666666',
          borderColor: '#666666',
          fontSize: '13px',
        };
    }
  };

  /** Returns the appropriate icon based on status */
  const getIcon = () => {
    switch (value) {
      case 'OK':
        return <CheckCircle data-testid="status-icon-ok" />;
      case 'Missing':
        return <Error data-testid="status-icon-missing" />;
      default:
        // Return undefined instead of null
        return undefined;
    }
  };

  return (
    <Chip
      data-testid="status-chip"
      icon={getIcon()}
      label={value}
      variant="outlined"
      size="small"
      sx={{
        ...getChipStyles(),
        height: '24px',
        borderRadius: '16px',
        '& .MuiChip-icon': {
          color: 'inherit',
          marginLeft: '5px',
        },
      }}
    />
  );
};

export default StatusChip;
