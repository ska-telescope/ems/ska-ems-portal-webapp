import { render, screen } from '@testing-library/react';
import type { ICellRendererParams } from 'ag-grid-community';
import { describe, expect, it } from 'vitest';

import StatusChip from './StatusChip';

// Mock MUI icons
vi.mock('@mui/icons-material', () => ({
  CheckCircle: () => <div data-testid="status-icon-ok">CheckCircle</div>,
  Error: () => <div data-testid="status-icon-missing">Error</div>,
}));

describe('StatusChip', () => {
  const createProps = (value: string): ICellRendererParams<any, any, any> => ({
    value,
    data: {},
    node: {} as any,
    colDef: {} as any,
    column: {} as any,
    api: {} as any,
    context: undefined,
    valueFormatted: null,
    eGridCell: document.createElement('div'),
    eParentOfValue: document.createElement('div'),
    formatValue: () => '',
    registerRowDragger: () => null,
    setTooltip: () => null,
  });

  it('renders OK status correctly', () => {
    const props = createProps('OK');
    render(<StatusChip {...props} />);

    const chip = screen.getByTestId('status-chip');
    const okIcon = screen.getByTestId('status-icon-ok');

    expect(chip).toBeInTheDocument();
    expect(okIcon).toBeInTheDocument();
    expect(chip).toHaveTextContent('OK');

    expect(chip).toHaveStyle({
      color: '#1e4620',
      borderColor: '#1e4620',
      fontSize: '13px',
    });
  });

  it('renders Missing status correctly', () => {
    const props = createProps('Missing');
    render(<StatusChip {...props} />);

    const chip = screen.getByTestId('status-chip');
    const missingIcon = screen.getByTestId('status-icon-missing');

    expect(chip).toBeInTheDocument();
    expect(missingIcon).toBeInTheDocument();
    expect(chip).toHaveTextContent('Missing');

    expect(chip).toHaveStyle({
      color: '#666666',
      borderColor: '#666666',
      fontSize: '13px',
    });
  });

  it('handles unknown status with default styling', () => {
    const props = createProps('Unknown');
    render(<StatusChip {...props} />);

    const chip = screen.getByTestId('status-chip');
    expect(chip).toBeInTheDocument();
    expect(chip).toHaveTextContent('Unknown');

    expect(chip).toHaveStyle({
      color: '#666666',
      borderColor: '#666666',
      fontSize: '13px',
    });
  });

  it('uses correct icon based on status', () => {
    const okProps = createProps('OK');
    const { rerender } = render(<StatusChip {...okProps} />);

    expect(screen.getByTestId('status-icon-ok')).toBeInTheDocument();
    expect(screen.queryByTestId('status-icon-missing')).not.toBeInTheDocument();

    const missingProps = createProps('Missing');
    rerender(<StatusChip {...missingProps} />);

    expect(screen.getByTestId('status-icon-missing')).toBeInTheDocument();
    expect(screen.queryByTestId('status-icon-ok')).not.toBeInTheDocument();
  });
});
