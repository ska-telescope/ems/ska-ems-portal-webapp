import { styled } from '@mui/system';

import { useUserRoles } from '@/auth/hooks/useUserRoles';
import lowReadBackground from '@assets/map-australia.svg';
import midBackground from '@assets/map-south-africa.svg';
import worldBackground from '@assets/map-world.svg';

// Function to determine background image based on extended roles
const getBackgroundImage = (extendedRoles: string[], isSuperAdmin: boolean) => {
  if (isSuperAdmin || extendedRoles.length === 0) {
    return worldBackground; // Default background for SuperAdmin or no extended roles
  }
  if (extendedRoles.some((role) => role.startsWith('LOW'))) {
    return lowReadBackground;
  }
  if (extendedRoles.some((role) => role.startsWith('MID'))) {
    return midBackground;
  }
  return worldBackground; // Fallback (should not be reached)
};

const BackgroundWrapper = () => {
  const { extendedRoles, isSuperAdmin } = useUserRoles();

  const backgroundImage = getBackgroundImage(extendedRoles, isSuperAdmin);

  const StyledBackground = styled('div')({
    backgroundImage: `url(${backgroundImage})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    opacity: 0.5,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1,
  });

  return <StyledBackground />;
};

export default BackgroundWrapper;
