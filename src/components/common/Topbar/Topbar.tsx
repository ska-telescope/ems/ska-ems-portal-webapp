import { useMsal } from '@azure/msal-react';
import { Box, Typography, Avatar, Menu, MenuItem } from '@mui/material';
import React, { useState } from 'react';

import { useCustomTheme } from '@/hooks/useCustomTheme';

interface TopbarProps {
  title: string;
  subtitle: string;
  userName: string;
}

/**
 * Topbar component that displays a fixed header with a title, subtitle, and user menu.
 * The user menu shows the initials of the logged-in user and provides a logout option.
 *
 * @param {TopbarProps} props - Props for the Topbar component.
 * @returns {JSX.Element} - The rendered Topbar component.
 */
const Topbar = ({ title, subtitle, userName }: TopbarProps) => {
  const { colors, spacing } = useCustomTheme(); // Access theme and accent color
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { instance } = useMsal();

  /**
   * Handles the opening of the user menu.
   * @param {React.MouseEvent<HTMLElement>} event - The click event.
   */
  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  /**
   * Handles the closing of the user menu.
   */
  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  /**
   * Handles the logout action.
   */
  const handleLogout = () => {
    instance.logoutRedirect().catch((e) => {
      console.error(e);
    });
    setAnchorEl(null);
  };

  /**
   * Gets the initials of the user from their name.
   * @param {string} name - The full name of the user.
   * @returns {string} - The initials of the user.
   */
  const getInitials = (name: string) => {
    const initials = name
      .split(' ')
      .map((n) => n[0])
      .join('')
      .toUpperCase();
    return initials;
  };

  return (
    <Box
      data-testid="topbar"
      sx={{
        height: 100,
        backgroundColor: colors.primaryLight, // Use primary color from theme
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: spacing(2), // Use spacing from theme
        paddingLeft: {
          xs: `calc(${spacing(8)} + ${spacing(2)})`,
          sm: `calc(${spacing(8)} + ${spacing(2)})`,
        },
        width: '100%',
        position: 'fixed',
        top: 0,
        right: 0,
        zIndex: 10,
        boxShadow: '0px 4px 6px rgba(0, 0, 0, 0.1)', // Use a static value or theme.shadow
      }}
    >
      <Box>
        <Typography
          variant="h4"
          component="h1"
          color={colors.textPrimary} // Dynamic text color
          data-testid="topbar-title"
        >
          {title}
        </Typography>
        <Typography
          variant="subtitle1"
          color={colors.textSecondary} // Dynamic text color
          sx={{ opacity: 0.7 }}
          data-testid="topbar-subtitle"
        >
          {subtitle}
        </Typography>
      </Box>
      <Box>
        <Avatar
          sx={{
            bgcolor: colors.customAccentPrimary, // Use accent1 for the avatar background
            cursor: 'pointer',
            color: '#fff',
          }}
          onClick={handleMenuOpen}
          data-testid="topbar-user-avatar"
        >
          {getInitials(userName)}
        </Avatar>
        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleMenuClose}
          data-testid="topbar-user-menu"
        >
          <MenuItem onClick={handleLogout} data-testid="topbar-logout-button">
            Logout
          </MenuItem>
        </Menu>
      </Box>
    </Box>
  );
};

export default Topbar;
