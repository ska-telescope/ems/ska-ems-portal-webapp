import { createTheme, ThemeProvider } from '@mui/material/styles';
import { render, screen } from '@testing-library/react';
import React from 'react';
import { describe, it, expect } from 'vitest';

import Topbar from '@components/common/Topbar/Topbar';

const theme = createTheme();

const renderWithTheme = (ui: React.ReactElement) => {
  return render(<ThemeProvider theme={theme}>{ui}</ThemeProvider>);
};

describe('Topbar', () => {
  it('renders Topbar with correct title and subtitle', () => {
    renderWithTheme(
      <Topbar
        title="Test Title"
        subtitle="Test Subtitle"
        userName="John Doe"
      />,
    );

    expect(screen.getByTestId('topbar')).toBeInTheDocument();
    expect(screen.getByTestId('topbar-title')).toHaveTextContent('Test Title');
    expect(screen.getByTestId('topbar-subtitle')).toHaveTextContent(
      'Test Subtitle',
    );
  });

  it('applies correct styles from the theme', () => {
    renderWithTheme(
      <Topbar
        title="Test Title"
        subtitle="Test Subtitle"
        userName="John Doe"
      />,
    );

    const topbar = screen.getByTestId('topbar');
    expect(topbar).toHaveStyle({
      height: '100px',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '100%',
      position: 'fixed',
      top: '0px',
      right: '0px',
    });
  });
});
