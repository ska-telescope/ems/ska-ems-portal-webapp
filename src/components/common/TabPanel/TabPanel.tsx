import { Box } from '@mui/material';
import React from 'react';

/**
 * Props for the TabPanel component
 */
interface TabPanelProps extends React.HTMLAttributes<HTMLDivElement> {
  /** The content to be rendered inside the tab panel */
  children?: React.ReactNode;
  /** The index of this tab panel */
  index: number;
  /** The currently selected tab value */
  value: number;
}

/**
 * TabPanel component that shows/hides content based on selected tab.
 * Follows WAI-ARIA guidelines for accessibility by providing proper attributes
 * and roles for tab panel content.
 *
 * @param {Object} props - The component props
 * @param {React.ReactNode} [props.children] - The content to be rendered inside the tab panel
 * @param {number} props.index - The index of this tab panel
 * @param {number} props.value - The currently selected tab value
 * @param {React.HTMLAttributes<HTMLDivElement>} props.other - Additional HTML div element props
 * @returns {JSX.Element} A div element containing the tab panel content
 *
 * @example
 * ```tsx
 * <TabPanel value={0} index={0} className="custom-class">
 *   Panel Content
 * </TabPanel>
 * ```
 */
const TabPanel = ({ children, value, index, ...other }: TabPanelProps) => (
  <div
    role="tabpanel"
    hidden={value !== index}
    id={`location-tabpanel-${index}`}
    aria-labelledby={`location-tab-${index}`}
    data-testid={`location-tabpanel-${index}`}
    {...other}
  >
    {value === index && (
      <Box
        sx={{
          py: 1, // padding top and bottom
          px: 0, // padding left and right
        }}
        data-testid={`location-tabpanel-content-${index}`}
      >
        {children}
      </Box>
    )}
  </div>
);

export default TabPanel;
