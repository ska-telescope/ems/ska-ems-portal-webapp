import { render, screen } from '@testing-library/react';
import { describe, it, expect } from 'vitest';

import TabPanel from './TabPanel';

describe('TabPanel', () => {
  it('renders content when value matches index', () => {
    render(
      <TabPanel value={0} index={0}>
        Test Content
      </TabPanel>,
    );

    const panel = screen.getByTestId('location-tabpanel-0');
    const content = screen.getByTestId('location-tabpanel-content-0');

    expect(panel).toBeInTheDocument();
    expect(panel).not.toHaveAttribute('hidden');
    expect(content).toBeInTheDocument();
    expect(content).toHaveTextContent('Test Content');
  });

  it('hides content when value does not match index', () => {
    render(
      <TabPanel value={1} index={0}>
        Test Content
      </TabPanel>,
    );

    const panel = screen.getByTestId('location-tabpanel-0');
    expect(panel).toHaveAttribute('hidden');
    expect(
      screen.queryByTestId('location-tabpanel-content-0'),
    ).not.toBeInTheDocument();
  });

  it('applies correct ARIA attributes', () => {
    render(
      <TabPanel value={0} index={0}>
        Test Content
      </TabPanel>,
    );

    const panel = screen.getByTestId('location-tabpanel-0');
    expect(panel).toHaveAttribute('role', 'tabpanel');
    expect(panel).toHaveAttribute('id', 'location-tabpanel-0');
    expect(panel).toHaveAttribute('aria-labelledby', 'location-tab-0');
  });

  it('passes through additional props', () => {
    render(
      <TabPanel value={0} index={0} className="custom-class" data-custom="test">
        Test Content
      </TabPanel>,
    );

    const panel = screen.getByTestId('location-tabpanel-0');
    expect(panel).toHaveClass('custom-class');
    expect(panel).toHaveAttribute('data-custom', 'test');
  });
});
