import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { describe, it, expect, vi } from 'vitest';

import ActionModal from './ActionModal';

describe('ActionModal Component', () => {
  const mockOnClose = vi.fn();
  const mockOnClick = vi.fn(() => Promise.resolve());

  const defaultProps = {
    open: true,
    onClose: mockOnClose,
    onClick: mockOnClick,
    dialogContentText: 'Are you sure you want to delete this item?',
    dialogTitle: 'Delete Item',
  };

  it('renders the modal with the correct content', () => {
    render(<ActionModal {...defaultProps} />);

    expect(screen.getByText('Delete Item')).toBeInTheDocument();
    expect(
      screen.getByText('Are you sure you want to delete this item?'),
    ).toBeInTheDocument();

    expect(screen.getByText('Cancel')).toBeInTheDocument();
    expect(screen.getByText('Delete')).toBeInTheDocument();
  });

  it('calls onClose when the Cancel button is clicked', () => {
    render(<ActionModal {...defaultProps} />);

    const cancelButton = screen.getByText('Cancel');
    fireEvent.click(cancelButton);

    expect(mockOnClose).toHaveBeenCalledTimes(1);
  });

  it('calls onClick when the Delete button is clicked', async () => {
    render(<ActionModal {...defaultProps} />);

    const deleteButton = screen.getByText('Delete');
    fireEvent.click(deleteButton);

    await waitFor(() => expect(mockOnClick).toHaveBeenCalledTimes(1));
  });

  it('does not render when open is false', () => {
    render(<ActionModal {...defaultProps} open={false} />);

    expect(screen.queryByText('Delete Item')).not.toBeInTheDocument();
    expect(
      screen.queryByText('Are you sure you want to delete this item?'),
    ).not.toBeInTheDocument();
  });
});
