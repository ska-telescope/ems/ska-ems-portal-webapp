import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@mui/material';
import React from 'react';

import { useCustomTheme } from '@hooks/useCustomTheme.ts';

/**
 * Defines the props for the ActionModal component
 */
interface ActionModalProps {
  open: boolean;
  onClose: () => void;
  onClick: () => Promise<void>;
  dialogContentText: string;
  dialogTitle: string;
}

/**
 * ActionModal component
 * This component opens a modal with an onClose and onClick.
 * Example usage is as a confirmation of an action modal with a cancel option.
 */
const ActionModal: React.FC<ActionModalProps> = ({
  open,
  onClose,
  onClick,
  dialogContentText,
  dialogTitle,
}) => {
  const { colors } = useCustomTheme();

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{dialogTitle}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {dialogContentText}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} sx={{ color: colors.textPrimary }}>
          Cancel
        </Button>
        <Button onClick={onClick} sx={{ color: colors.customAccentSecondary }}>
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ActionModal;
