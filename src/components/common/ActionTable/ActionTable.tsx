import DeleteIcon from '@mui/icons-material/Delete';
import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';
import React, { ReactNode } from 'react';

import { useCustomTheme } from '@hooks/useCustomTheme.ts';

/**
 * Defines the props for the ActionTable component
 */
interface TableComponentProps<T> {
  /**
   * Array of row data to be displayed in the table.
   * Each object in the array represents a row, and it must contain at least an `key` field.
   */
  rows: T[];

  /**
   * Array of column definitions.
   * Each column definition contains:
   * - `header`: The title of the column to be displayed in the table header.
   * - `render`: A function that receives a row and returns the content to be displayed in the corresponding cell.
   */
  columns: Array<{
    key: string;
    header: string;
    render: (row: T) => ReactNode;
  }>;

  /**
   * Boolean flag indicating whether the table is in a loading state.
   * If true, a loading message or component is displayed instead of the table rows.
   */
  loading: boolean;

  /**
   * Function to be called when a delete action is triggered for a specific row.
   * The function receives the `id` of the row to be deleted.
   */
  onDeleteClick: (id: number) => void;

  /**
   * Optional array of action definitions.
   * Each action contains:
   * - `label`: The label for the action button.
   * - `icon`: Optional icon to be displayed alongside the label.
   * - `onClick`: A function to handle the action, which receives the row's `id`.
   * Defaults to a single delete action with the provided `onDeleteClick` function.
   */
  actions?: Array<{
    label: string;
    icon?: ReactNode;
    onClick: (id: number) => void;
  }>;

  /**
   * Optional custom component or message to be displayed when the table is in a loading state.
   * Defaults to the string "Loading...".
   */
  loadingComponent?: ReactNode;

  /**
   * Optional custom component or message to be displayed when there are no rows to display.
   * Defaults to the string "No data available".
   */
  emptyStateComponent?: ReactNode;

  /**
   * Optional custom styles for the overall table container.
   * Accepts any valid CSS properties object.
   */
  tableStyles?: React.CSSProperties;

  /**
   * Optional custom styles for the action buttons within each row.
   * Accepts any valid CSS properties object.
   */
  buttonStyles?: React.CSSProperties;
}

/**
 * A reusable table component that supports dynamic columns, row actions, and customizable loading/empty states.
 */
const ActionTable = <T extends { id: number }>({
  rows = [],
  columns,
  loading,
  onDeleteClick,
  actions = [{ label: 'Delete', icon: <DeleteIcon />, onClick: onDeleteClick }],
  loadingComponent = 'Loading...',
  emptyStateComponent = 'No tokens available',
  tableStyles = {},
  buttonStyles = {},
}: TableComponentProps<T>) => {
  const { colors } = useCustomTheme();

  const renderActions = (row: T) => (
    <>
      {actions.map((action) => (
        <Button
          key={action.label}
          variant="text"
          size="medium"
          data-testid={`delete-button-${row.id}`}
          onClick={() => action.onClick(row.id)}
          endIcon={action.icon}
          disableRipple
          sx={{
            ...buttonStyles,
            color: colors.customAccentSecondary,
            textTransform: 'none',
            padding: 0,
            minWidth: 0,
            background: 'none',
            outline: 'none',
            border: 'none',
            '&:hover': {
              textDecoration: 'underline',
              background: 'none',
            },
            '&:focus': {
              outline: 'none',
              background: 'none',
            },
            '&:active': {
              outline: 'none',
              background: 'none',
            },
          }}
        >
          {action.label}
        </Button>
      ))}
    </>
  );

  // Helper function to render table rows
  const renderRows = () => {
    if (loading) {
      return (
        <TableRow>
          <TableCell colSpan={columns.length + 1} align="center">
            {loadingComponent}
          </TableCell>
        </TableRow>
      );
    }

    if (rows.length === 0) {
      return (
        <TableRow>
          <TableCell colSpan={columns.length + 1} align="center">
            {emptyStateComponent}
          </TableCell>
        </TableRow>
      );
    }

    return rows.map((row) => (
      <TableRow key={row.id}>
        {columns.map((column) => (
          <TableCell key={column.key}>{column.render(row)}</TableCell>
        ))}
        <TableCell>{renderActions(row)}</TableCell>
      </TableRow>
    ));
  };

  return (
    <TableContainer component={Paper} style={tableStyles}>
      <Table>
        <TableHead>
          <TableRow>
            {columns.map((column) => (
              <TableCell key={column.key}>{column.header}</TableCell>
            ))}
            <TableCell>Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>{renderRows()}</TableBody>
      </Table>
    </TableContainer>
  );
};

export default ActionTable;
