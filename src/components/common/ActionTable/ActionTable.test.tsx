import { render, screen, fireEvent } from '@testing-library/react';

import ActionTable from './ActionTable';

import '@testing-library/jest-dom';

const rows = [
  {
    id: 1,
    description: 'Test Token 1',
    scope: 'read',
    creation_date: '2024-10-07',
    expires_at: null,
    last_used: null,
  },
  {
    id: 2,
    description: 'Test Token 2',
    scope: 'write',
    creation_date: '2024-09-07',
    expires_at: 1700000000,
    last_used: 1690000000,
  },
];

const columns = [
  {
    key: 'description',
    header: 'Description',
    render: (row: { description: any }) => row.description,
  },
  { key: 'scope', header: 'Scope', render: (row: { scope: any }) => row.scope },
  {
    key: 'creation_date',
    header: 'Creation Date',
    render: (row: { creation_date: any }) => row.creation_date,
  },
  {
    key: 'expires_at',
    header: 'Expiration Date',
    render: (row: { expires_at: any }) =>
      row.expires_at ? row.expires_at : 'Never',
  },
  {
    key: 'last_used',
    header: 'Last Used',
    render: (row: { last_used: any }) =>
      row.last_used ? row.last_used : 'Never',
  },
];

describe('ActionTable Component', () => {
  it('renders table headers correctly', () => {
    render(
      <ActionTable
        rows={[]}
        loading={false}
        onDeleteClick={vi.fn()}
        columns={columns}
      />,
    );

    expect(screen.getByText('Description')).toBeInTheDocument();
    expect(screen.getByText('Scope')).toBeInTheDocument();
    expect(screen.getByText('Creation Date')).toBeInTheDocument();
    expect(screen.getByText('Expiration Date')).toBeInTheDocument();
    expect(screen.getByText('Last Used')).toBeInTheDocument();
  });

  it('displays loading state when loading is true', () => {
    render(
      <ActionTable
        rows={[]}
        loading={true}
        onDeleteClick={vi.fn()}
        columns={columns}
        loadingComponent="Loading..."
      />,
    );

    expect(screen.getByText('Loading...')).toBeInTheDocument();
  });

  it('displays empty state when no rows are available', () => {
    render(
      <ActionTable
        rows={[]}
        loading={false}
        onDeleteClick={vi.fn()}
        columns={columns}
        emptyStateComponent="No data available"
      />,
    );

    expect(screen.getByText('No data available')).toBeInTheDocument();
  });

  it('renders rows properly', () => {
    render(
      <ActionTable
        rows={rows}
        loading={false}
        onDeleteClick={vi.fn()}
        columns={columns}
      />,
    );

    expect(screen.getByText('Test Token 1')).toBeInTheDocument();
    expect(screen.getByText('Test Token 2')).toBeInTheDocument();
    expect(screen.getByText('read')).toBeInTheDocument();
    expect(screen.getByText('write')).toBeInTheDocument();

    const neverElements = screen.getAllByText('Never');
    expect(neverElements.length).toBe(2);

    expect(neverElements[0]).toBeInTheDocument();

    expect(neverElements[1]).toBeInTheDocument();
  });

  it('calls onDeleteClick when delete button is clicked', () => {
    const onDeleteClickMock = vi.fn();

    render(
      <ActionTable
        rows={rows}
        loading={false}
        onDeleteClick={onDeleteClickMock}
        columns={columns}
      />,
    );

    const deleteButtons = screen.getAllByText('Delete');
    fireEvent.click(deleteButtons[0]);

    expect(onDeleteClickMock).toHaveBeenCalledWith(1);
  });
});
