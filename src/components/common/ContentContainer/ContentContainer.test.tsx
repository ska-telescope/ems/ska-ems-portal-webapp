import { ThemeProvider, createTheme } from '@mui/material';
import { render, screen, within } from '@testing-library/react';
import { describe, it, expect } from 'vitest';

import ContentContainer from './ContentContainer';

const theme = createTheme();

const renderWithTheme = (ui: React.ReactElement) => {
  return render(<ThemeProvider theme={theme}>{ui}</ThemeProvider>);
};

describe('ContentContainer', () => {
  it('renders with default props', () => {
    renderWithTheme(<ContentContainer>Test Content</ContentContainer>);

    const container = screen.getByTestId('content-container');
    expect(container).toBeInTheDocument();
    expect(container).toHaveTextContent('Test Content');
    expect(container).toHaveAttribute('role', 'region');
    expect(container).toHaveAttribute('aria-label', 'Content area');
  });

  it('applies custom aria-label when provided', () => {
    renderWithTheme(
      <ContentContainer aria-label="Custom Area">
        Test Content
      </ContentContainer>,
    );

    const container = screen.getByTestId('content-container');
    expect(container).toHaveAttribute('aria-label', 'Custom Area');
  });

  it('applies background color with opacity', () => {
    const testProps = {
      backgroundColor: '#ffffff',
      backgroundOpacity: 0.5,
    };

    renderWithTheme(
      <ContentContainer {...testProps}>Test Content</ContentContainer>,
    );

    const container = screen.getByTestId('content-container');
    expect(container).toHaveClass('MuiBox-root');
  });

  it('handles different background color formats', () => {
    const testCases = [
      { backgroundColor: '#ffffff', backgroundOpacity: 0.5 },
      { backgroundColor: 'rgb(255, 255, 255)', backgroundOpacity: 0.5 },
      { backgroundColor: 'background.paper', backgroundOpacity: 0.5 },
    ];

    testCases.forEach((props) => {
      const { rerender } = renderWithTheme(
        <ContentContainer {...props}>Test Content</ContentContainer>,
      );

      const container = screen.getByTestId('content-container');
      expect(container).toHaveClass('MuiBox-root');

      // Clean up before next iteration
      rerender(<div />);
    });
  });

  it('passes through additional Box props', () => {
    renderWithTheme(
      <ContentContainer className="custom-class" id="test-id">
        Test Content
      </ContentContainer>,
    );

    const container = screen.getByTestId('content-container');
    expect(container).toHaveClass('custom-class');
    expect(container).toHaveAttribute('id', 'test-id');
  });

  it('renders children correctly', () => {
    renderWithTheme(
      <ContentContainer>
        <div data-testid="child-element">Child Content</div>
      </ContentContainer>,
    );

    const container = screen.getByTestId('content-container');
    const childElement = within(container).getByTestId('child-element');
    expect(childElement).toBeInTheDocument();
    expect(childElement).toHaveTextContent('Child Content');
  });

  it('maintains default opacity when backgroundOpacity is not provided', () => {
    renderWithTheme(
      <ContentContainer backgroundColor="#ffffff">
        Test Content
      </ContentContainer>,
    );

    const container = screen.getByTestId('content-container');
    expect(container).toHaveClass('MuiBox-root');
  });
});
