import { Box, BoxProps } from '@mui/material';

// TODO: Make the container oppacity constant instead of passing it to the component

/**
 * Props for the ContentContainer component.
 */
interface ContentContainerProps extends BoxProps {
  /** Maximum width of the container (pixels or percentage) */
  maxWidth?: number | string;
  /** Background color */
  backgroundColor?: string;
  /** Custom aria label for accessibility */
  'aria-label'?: string;
  /** Controls the initial main size of the flex item */
  flexBasis?: string | number;
  /** Background opacity (0-1), applies only to background */
  backgroundOpacity?: number;
}

/**
 * ContentContainer component that creates a content wrapper, with flexible sizing options.
 *
 * This component uses Material-UI's Box component to create a flexible container
 * that can be used in various layouts.
 *
 * @param props ContentContainerProps & React.PropsWithChildren
 * @returns JSX.Element
 */
const ContentContainer = ({
  children,
  maxWidth,
  backgroundColor = '#6d6d6d',
  'aria-label': ariaLabel,
  flexBasis,
  backgroundOpacity = 0.8,
  sx = {},
  ...boxProps
}: ContentContainerProps) => {
  return (
    <Box
      data-testid="content-container"
      sx={{
        flexBasis: flexBasis,
        flexGrow: flexBasis ? 0 : 1,
        flexShrink: 0,
        // bgcolor: backgroundColor,
        bgcolor: backgroundColor,
        borderRadius: 2,
        padding: 2,
        maxWidth: maxWidth,
        width: '100%',
        height: '100%',
        boxShadow: 1,
        display: 'flex',
        flexDirection: 'column',
        overflow: 'auto',
        ...sx,
        opacity: backgroundOpacity,
      }}
      role="region"
      aria-label={ariaLabel || 'Content area'}
      {...boxProps}
    >
      {children}
    </Box>
  );
};

export default ContentContainer;
