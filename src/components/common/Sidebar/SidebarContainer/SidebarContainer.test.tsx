import { ThemeProvider, createTheme } from '@mui/material/styles';
import { render, screen } from '@testing-library/react';
import React from 'react';

import SidebarContainer from './SidebarContainer';

const theme = createTheme({
  palette: {
    primary: {
      main: '#1976d2',
      contrastText: '#ffffff',
    },
  },
});

const renderWithTheme = (ui: React.ReactElement) => {
  return render(<ThemeProvider theme={theme}>{ui}</ThemeProvider>);
};

describe('SidebarContainer', () => {
  it('renders with correct width when expanded', () => {
    renderWithTheme(
      <SidebarContainer isExpanded={true} data-testid="sidebar-container" />,
    );
    const sidebarContainer = screen.getByTestId('sidebar-container');
    expect(sidebarContainer).toHaveStyle('width: 200px');
  });

  it('uses accent color when provided', () => {
    const accentColor = '#FF0000';
    renderWithTheme(
      <SidebarContainer
        isExpanded={false}
        accentColor={accentColor}
        data-testid="sidebar-container"
      />,
    );
    const sidebarContainer = screen.getByTestId('sidebar-container');
    expect(sidebarContainer).toHaveStyle(`background-color: ${accentColor}`);
  });

  it('uses theme primary color when accent color is not provided', () => {
    renderWithTheme(
      <SidebarContainer isExpanded={false} data-testid="sidebar-container" />,
    );
    const sidebarContainer = screen.getByTestId('sidebar-container');
    expect(sidebarContainer).toHaveStyle(
      `background-color: ${theme.palette.primary.main}`,
    );
  });

  it('has correct base styles', () => {
    renderWithTheme(
      <SidebarContainer isExpanded={false} data-testid="sidebar-container" />,
    );
    const sidebarContainer = screen.getByTestId('sidebar-container');
    expect(sidebarContainer).toHaveStyle({
      height: '100vh',
      overflow: 'hidden',
      position: 'fixed',
      left: '0',
      top: '0',
    });
  });

  it('has correct z-index', () => {
    renderWithTheme(
      <SidebarContainer isExpanded={false} data-testid="sidebar-container" />,
    );
    const sidebarContainer = screen.getByTestId('sidebar-container');
    expect(sidebarContainer).toHaveStyle(`z-index: ${theme.zIndex.drawer}`);
  });
});
