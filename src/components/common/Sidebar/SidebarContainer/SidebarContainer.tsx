import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';

interface SidebarContainerProps {
  isExpanded: boolean;
  accentColor?: string;
}

const SidebarContainer = styled(Box, {
  shouldForwardProp: (prop) => prop !== 'isExpanded' && prop !== 'accentColor',
})<SidebarContainerProps>(({ theme, isExpanded, accentColor }) => ({
  width: isExpanded ? 200 : 64,
  backgroundColor: accentColor || theme.palette.primary.main,
  height: '100vh',
  color: theme.palette.text.primary,
  overflow: 'hidden',
  position: 'fixed',
  left: 0,
  top: 0,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  '&:hover': {
    width: 200,
  },
  zIndex: theme.zIndex.drawer,
}));

export default SidebarContainer;
