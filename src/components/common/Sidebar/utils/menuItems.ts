export interface MenuItem {
  text: string;
  iconName: string;
  path: string;
}

const menuItems: MenuItem[] = [
  { text: 'Home', iconName: 'HomeIcon', path: '/' },
  { text: 'Locations', iconName: 'LocationOnIcon', path: '/locations' },
  {
    text: 'Maintenance',
    iconName: 'EngineeringIcon',
    path: '/maintenance',
  },
  { text: 'Settings', iconName: 'SettingsIcon', path: '/settings' },
];

export default menuItems;
