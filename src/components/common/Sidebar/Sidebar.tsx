import { List, Box, Divider, useTheme } from '@mui/material';
import React, { useState } from 'react';

import expandedLogoDark from '@assets/skao-logo-black.svg';
import collapsedLogoDark from '@assets/skao-logo-small-black.svg';
import collapsedLogoLight from '@assets/skao-logo-small-white.svg';
import expandedLogoLight from '@assets/skao-logo-white.svg';
import {
  SidebarContainer,
  MenuItem,
  menuItems,
} from '@components/common/Sidebar';

interface SidebarProps {
  initialExpanded?: boolean;
  onExpandedChange?: (expanded: boolean) => void;
  accentColor?: string;
  logo?: React.ReactNode;
}

/**
 * Sidebar component that displays a navigational sidebar with menu items and a logo.
 * The sidebar can be expanded or collapsed based on user interactions.
 *
 * @param {SidebarProps} props - Props for the Sidebar component.
 * @returns {JSX.Element} - The rendered Sidebar component.
 */
const Sidebar = ({
  initialExpanded = false,
  onExpandedChange,
  accentColor,
}: SidebarProps) => {
  const [isExpanded, setIsExpanded] = useState(initialExpanded);
  const theme = useTheme();

  const isDarkMode = theme.palette.mode === 'dark';

  const expandedLogo = isDarkMode ? expandedLogoLight : expandedLogoDark;
  const collapsedLogo = isDarkMode ? collapsedLogoLight : collapsedLogoDark;

  /**
   * Handles the change in sidebar expansion state.
   * @param {boolean} expanded - The new expansion state of the sidebar.
   */
  const handleExpansionChange = (expanded: boolean) => {
    setIsExpanded(expanded);
    onExpandedChange?.(expanded);
  };

  // Separate settings item from the rest of the menu items
  const mainMenuItems = menuItems.filter((item) => item.text !== 'Settings');
  const settingsItem = menuItems.find((item) => item.text === 'Settings');

  return (
    <SidebarContainer
      isExpanded={isExpanded}
      accentColor={accentColor}
      onMouseEnter={() => handleExpansionChange(true)}
      onMouseLeave={() => handleExpansionChange(false)}
      data-testid="sidebar"
      sx={{ display: 'flex', flexDirection: 'column', height: '100vh' }}
    >
      <Box
        sx={{
          height: 64,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          transition: theme.transitions.create('all'),
        }}
        data-testid="sidebar-logo-container"
      >
        <Box
          data-testid="sidebar-logo-image"
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <img
            data-testid="sidebar-logo"
            src={isExpanded ? expandedLogo : collapsedLogo}
            alt={isExpanded ? 'SKAO Small Logo' : 'SKAO Full Logo'}
            style={{ maxWidth: isExpanded ? '100px' : '30px' }}
          />
        </Box>
      </Box>
      <List sx={{ pt: 0, pb: 0 }} data-testid="sidebar-menu-list">
        {mainMenuItems.map((item) => (
          <MenuItem
            key={item.text}
            item={item}
            isExpanded={isExpanded}
            data-testid={`sidebar-menu-item-${item.text.toLowerCase()}`}
          />
        ))}
      </List>
      <Box sx={{ flexGrow: 1 }} />
      {settingsItem && (
        <Box sx={{ mt: 'auto' }}>
          <Divider sx={{ mt: 1, mb: 1, borderColor: 'text.primary' }} />
          <List sx={{ pt: 0, pb: 0 }} data-testid="sidebar-settings-list">
            <MenuItem
              key={settingsItem.text}
              item={settingsItem}
              isExpanded={isExpanded}
              data-testid={`sidebar-menu-item-${settingsItem.text.toLowerCase()}`}
            />
          </List>
        </Box>
      )}
    </SidebarContainer>
  );
};

export default Sidebar;
