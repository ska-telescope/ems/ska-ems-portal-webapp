import { ThemeProvider, createTheme } from '@mui/material/styles';
import { render, screen, fireEvent } from '@testing-library/react';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import Sidebar from '@components/common/Sidebar/Sidebar';

const theme = createTheme();

const renderWithRouter = (ui: React.ReactElement, { route = '/' } = {}) => {
  window.history.pushState({}, 'Test page', route);
  return render(
    <BrowserRouter>
      <ThemeProvider theme={theme}>{ui}</ThemeProvider>
    </BrowserRouter>,
  );
};

// Mock image paths for testing in vitest
vi.mock('@assets/skao-logo-small-white.svg', () => ({
  default: 'mock-small-logo.svg',
}));
vi.mock('@assets/skao-logo-white.svg', () => ({
  default: 'mock-full-logo.svg',
}));

describe('Sidebar', () => {
  test('renders sidebar with logo and menu items', () => {
    renderWithRouter(<Sidebar />);

    expect(screen.getByTestId('sidebar')).toBeInTheDocument();
    expect(screen.getByTestId('sidebar-logo')).toBeInTheDocument();
    expect(screen.getByTestId('sidebar-menu-list')).toBeInTheDocument();
    expect(screen.getByTestId('menu-item-home')).toBeInTheDocument();
    expect(screen.getByTestId('menu-item-locations')).toBeInTheDocument();
    expect(screen.getByTestId('menu-item-settings')).toBeInTheDocument();
  });

  test('expands sidebar on mouse enter', () => {
    renderWithRouter(<Sidebar />);

    const sidebar = screen.getByTestId('sidebar');
    fireEvent.mouseEnter(sidebar);

    const logoImage = screen.getByTestId('sidebar-logo');
    expect(logoImage).toHaveAttribute('src', '/src/assets/skao-logo-black.svg');
  });

  test('collapses sidebar on mouse leave', () => {
    renderWithRouter(<Sidebar initialExpanded={true} />);

    const sidebar = screen.getByTestId('sidebar');
    fireEvent.mouseLeave(sidebar);

    const logoImage = screen.getByTestId('sidebar-logo');
    expect(logoImage).toHaveAttribute(
      'src',
      '/src/assets/skao-logo-small-black.svg',
    );
  });

  test('calls onExpandedChange when sidebar expands/collapses', () => {
    const onExpandedChange = vi.fn();
    renderWithRouter(<Sidebar onExpandedChange={onExpandedChange} />);

    const sidebar = screen.getByTestId('sidebar');
    fireEvent.mouseEnter(sidebar);
    expect(onExpandedChange).toHaveBeenCalledWith(true);

    fireEvent.mouseLeave(sidebar);
    expect(onExpandedChange).toHaveBeenCalledWith(false);
  });
});
