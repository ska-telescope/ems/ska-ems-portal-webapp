import { ThemeProvider, createTheme } from '@mui/material/styles';
import { render, screen } from '@testing-library/react';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { MenuItem as MenuItemType } from '../utils/menuItems';

import MenuItem from './MenuItem';

const theme = createTheme();

const mockItem: MenuItemType = {
  text: 'Home',
  iconName: 'HomeIcon',
  path: '/',
};

const renderWithRouter = (ui: React.ReactElement, { route = '/' } = {}) => {
  window.history.pushState({}, 'Test page', route);
  return render(
    <BrowserRouter>
      <ThemeProvider theme={theme}>{ui}</ThemeProvider>
    </BrowserRouter>,
  );
};

describe('MenuItem', () => {
  test('renders collapsed menu item correctly', () => {
    renderWithRouter(<MenuItem item={mockItem} isExpanded={false} />);

    const menuItem = screen.getByTestId('menu-item-home');
    expect(menuItem).toBeInTheDocument();
    expect(menuItem).toHaveAttribute('href', '/');

    const icon = screen.getByTestId('menu-item-icon-home');
    expect(icon).toBeInTheDocument();

    const text = screen.getByTestId('menu-item-text-home');
    expect(text).toHaveStyle('opacity: 0');
  });

  test('renders expanded menu item correctly', () => {
    renderWithRouter(<MenuItem item={mockItem} isExpanded={true} />);

    const menuItem = screen.getByTestId('menu-item-home');
    expect(menuItem).toBeInTheDocument();

    const icon = screen.getByTestId('menu-item-icon-home');
    expect(icon).toBeInTheDocument();

    const text = screen.getByTestId('menu-item-text-home');
    expect(text).toHaveStyle('opacity: 1');
    expect(text).toHaveTextContent('Home');
  });

  test('applies active class when route matches', () => {
    renderWithRouter(<MenuItem item={mockItem} isExpanded={true} />, {
      route: '/',
    });

    const menuItem = screen.getByTestId('menu-item-home');
    expect(menuItem).toHaveClass('active');
  });
});
