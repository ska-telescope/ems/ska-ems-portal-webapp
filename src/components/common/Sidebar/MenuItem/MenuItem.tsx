import EngineeringIcon from '@mui/icons-material/Engineering';
import HomeIcon from '@mui/icons-material/Home';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import SettingsIcon from '@mui/icons-material/Settings';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Tooltip,
  ListItemProps,
  useTheme,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import React from 'react';
import { NavLink, NavLinkProps } from 'react-router-dom';

import { MenuItem as MenuItemType } from '../utils/menuItems';

type StyledListItemProps = ListItemProps<typeof NavLink> & NavLinkProps;

const StyledListItem = styled(ListItem)<StyledListItemProps>(({ theme }) => ({
  height: 48,
  transition: theme.transitions.create('background-color', {
    easing: theme.transitions.easing.easeInOut,
    duration: theme.transitions.duration.shorter,
  }),
  '&:hover': {
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
  },
  '&.active': {
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },
  justifyContent: 'flex-start',
  alignItems: 'center',
  padding: 0,
  textDecoration: 'none',
  color: 'inherit',
}));

const iconComponents = {
  HomeIcon,
  LocationOnIcon,
  SettingsIcon,
  EngineeringIcon,
};

interface MenuItemProps {
  item: MenuItemType;
  isExpanded: boolean;
}

const MenuItem: React.FC<MenuItemProps> = ({ item, isExpanded }) => {
  const theme = useTheme();
  const IconComponent =
    iconComponents[item.iconName as keyof typeof iconComponents] || HomeIcon;

  return (
    <StyledListItem
      component={NavLink}
      to={item.path}
      end={item.path === '/'}
      data-testid={`menu-item-${item.text.toLowerCase()}`}
    >
      <Tooltip title={isExpanded ? '' : item.text} placement="right" arrow>
        <ListItemIcon
          sx={{
            minWidth: 64,
            color: 'inherit',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
          }}
          data-testid={`menu-item-icon-${item.text.toLowerCase()}`}
        >
          <IconComponent />
        </ListItemIcon>
      </Tooltip>
      <ListItemText
        primary={item.text}
        sx={{
          opacity: isExpanded ? 1 : 0,
          visibility: isExpanded ? 'visible' : 'hidden',
          transition: theme.transitions.create('opacity'),
          whiteSpace: 'nowrap',
          m: 0,
        }}
        data-testid={`menu-item-text-${item.text.toLowerCase()}`}
      />
    </StyledListItem>
  );
};

export default MenuItem;
