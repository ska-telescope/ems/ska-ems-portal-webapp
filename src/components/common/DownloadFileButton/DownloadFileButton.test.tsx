import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { vi } from 'vitest';

import { DownloadFileButton } from './DownloadFileButton';

const mockRefetch = vi.fn().mockImplementation(() => {
  return Promise.resolve({
    data: { blob: new Blob(['test']), fileName: 'file.csv' },
  }).then(() => {
    throw new Error('Mocked error after then');
  });
});

vi.mock('@tanstack/react-query', () => ({
  useQuery: vi.fn(() => ({ refetch: mockRefetch })),
}));

global.URL.createObjectURL = vi.fn(() => 'mock-url');
global.URL.revokeObjectURL = vi.fn();

describe('DownloadFileButton', () => {
  beforeEach(() => {
    vi.clearAllMocks();
  });

  it('calls refetch when the button is clicked', async () => {
    render(<DownloadFileButton locationId="123" />);

    const button = screen.getByTestId('download-button');

    fireEvent.click(button);

    // Assert refetch is called
    await waitFor(() => {
      expect(mockRefetch).toHaveBeenCalledTimes(1);
    });
  });

  it('handles successful file download', async () => {
    const mockBlob = new Blob(['test data'], { type: 'text/csv' });
    const mockFileName = '123_assets.csv';

    // Mock refetch to resolve with file data
    mockRefetch.mockResolvedValueOnce({
      data: {
        blob: mockBlob,
        fileName: mockFileName,
      },
    });

    render(<DownloadFileButton locationId="123" />);

    const button = screen.getByTestId('download-button');
    fireEvent.click(button);

    await waitFor(() => {
      expect(URL.createObjectURL).toHaveBeenCalledWith(mockBlob);
    });
  });

  it('handles API errors gracefully', async () => {
    const mockConsoleError = vi
      .spyOn(console, 'error')
      .mockImplementation(() => {});
    mockRefetch.mockRejectedValueOnce(new Error('Failed to fetch'));

    render(<DownloadFileButton locationId="123" />);

    const button = screen.getByTestId('download-button');
    fireEvent.click(button);

    await waitFor(() => {
      // Verify error handling
      expect(mockConsoleError).toHaveBeenCalledWith(
        'Failed to download file:',
        expect.any(Error),
      );
    });
    mockConsoleError.mockRestore();
  });
});
