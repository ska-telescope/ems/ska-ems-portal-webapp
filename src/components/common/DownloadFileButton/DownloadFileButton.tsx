import DownloadIcon from '@mui/icons-material/Download';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import { Box } from '@mui/system';
import { useQuery } from '@tanstack/react-query';
import { useCallback } from 'react';

import useAuthApiClient from '@/auth/hooks/useAuthApiClient';
import { locationExportFile } from '@api/locations/locationAssestsApi.ts';

/**
 * Props for the DownloadFileButton component.
 */
interface DownloadFileButtonProps {
  locationId: string;
}

/**
 * The DownloadFileButton component provides a button to download a file containing all the assets for a given location.
 *
 * @param props DownloadFileButtonProps - The props for the component.
 * @returns JSX.Element
 *
 */
export const DownloadFileButton = ({ locationId }: DownloadFileButtonProps) => {
  const authApiClient = useAuthApiClient();

  const { refetch } = useQuery<{ blob: Blob; fileName: string }>({
    queryKey: ['location-export-csv', locationId],
    queryFn: async () => locationExportFile(locationId, authApiClient),
    enabled: false,
  });

  const downloadFile = useCallback(() => {
    refetch()
      .then(({ data }) => {
        if (data) {
          const { blob, fileName } = data;
          const url = URL.createObjectURL(blob);
          const a = document.createElement('a');
          a.href = url;
          a.download = fileName;
          a.click();
          URL.revokeObjectURL(url);
        }
      })
      .catch((error) => {
        console.error('Failed to download file:', error);
        alert(
          'An error occurred while downloading the file. Please try again later.',
        );
      });
  }, [refetch]);

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
      }}
      data-testid="download-file-container"
    >
      <Box sx={{ mb: 1 }} data-testid="tooltip-container">
        <Tooltip title="Extract Assets" placement="left" data-testid="tooltip">
          <IconButton
            aria-label="Extract Assets"
            onClick={downloadFile}
            data-testid="download-button"
          >
            <DownloadIcon data-testid="download-icon" />
          </IconButton>
        </Tooltip>
      </Box>
    </div>
  );
};
