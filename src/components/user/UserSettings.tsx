import {
  Radio,
  RadioGroup,
  FormControlLabel,
  Box,
  Typography,
  Button,
} from '@mui/material';
import FormLabel from '@mui/material/FormLabel';
import { useContext } from 'react';

import { themes } from '@/theme/theme';
import { ThemeContext } from '@/theme/ThemeProvider';

const UserSettings = () => {
  const { themeMode, toggleTheme, setSelectedTheme, selectedTheme } =
    useContext(ThemeContext);

  // Handle light/dark mode changes
  const handleModeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const selectedMode = event.target.value;
    if (selectedMode !== themeMode) {
      toggleTheme();
    }
  };

  return (
    <div>
      <Typography variant="h5" gutterBottom>
        User Settings
      </Typography>

      {/* Light/Dark Mode Selector */}
      <Box mb={4}>
        <FormLabel component="legend">Theme Mode</FormLabel>
        <RadioGroup value={themeMode} onChange={handleModeChange} row>
          <FormControlLabel
            value="light"
            control={<Radio />}
            label="Light Mode"
          />
          <FormControlLabel
            value="dark"
            control={<Radio />}
            label="Dark Mode"
          />
        </RadioGroup>
      </Box>

      {/* Accent Color Selector */}
      <Box mt={4}>
        <Typography variant="h6" gutterBottom>
          Choose Accent Color
        </Typography>
        <Box display="flex" gap={2} flexWrap="wrap">
          {Object.entries(themes).map(([key, theme]) => (
            <Button
              key={`${key}`}
              onClick={() => setSelectedTheme(key)}
              sx={{
                backgroundColor: theme.accentSecondary,
                width: 36,
                height: 36,
                minWidth: 36,
                border: key === selectedTheme ? '2px solid black' : 'none',
                '&:hover': {
                  border: '2px solid black',
                },
              }}
            />
          ))}
        </Box>
        {/* Optional Reset Button */}
        <Box mt={2}>
          <Button
            variant="outlined"
            onClick={() => setSelectedTheme('theme1')} // Default accent color
          >
            Reset to Default
          </Button>
        </Box>
      </Box>
    </div>
  );
};

export default UserSettings;
