import PinDropIcon from '@mui/icons-material/PinDrop';
import PlaceIcon from '@mui/icons-material/Place';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import { styled, alpha } from '@mui/material/styles';
import { TransitionProps } from '@mui/material/transitions';
import Typography from '@mui/material/Typography';
import { TreeViewBaseItem } from '@mui/x-tree-view/models';
import { RichTreeView } from '@mui/x-tree-view/RichTreeView';
import { treeItemClasses } from '@mui/x-tree-view/TreeItem';
import {
  TreeItem2Props,
  TreeItem2Content,
  TreeItem2Label,
  TreeItem2Root,
  TreeItem2Checkbox,
  TreeItem2IconContainer,
} from '@mui/x-tree-view/TreeItem2';
import { TreeItem2Icon } from '@mui/x-tree-view/TreeItem2Icon';
import { TreeItem2Provider } from '@mui/x-tree-view/TreeItem2Provider';
import { unstable_useTreeItem2 as useTreeItem2 } from '@mui/x-tree-view/useTreeItem2';
import clsx from 'clsx';
import React from 'react';

// Interface definitions
/**
 * Defines the props for the LocationTree component
 */
interface LocationTreeProps {
  items: TreeViewBaseItem[];
  onItemClick: (itemId: string) => void;
}

/**
 * Returns the appropriate icon component based on the node type
 * Modify this function to add more icon types as needed
 *
 * @param type - The type of the node (number)
 * @returns React component for the icon
 */
const getIconFromType = (type: number) => {
  switch (type) {
    case 1:
      return PlaceIcon;
    case 2:
      return PinDropIcon;
    default:
      return PlaceIcon;
  }
};

// Styled components
/**
 * Styled component for the root of each tree item
 * Customize the appearance of the tree item root here
 */
const StyledTreeItemRoot = styled(TreeItem2Root)(({ theme }) => ({
  color: theme.palette.text.primary,
  position: 'relative',
  [`& .${treeItemClasses.groupTransition}`]: {
    marginLeft: theme.spacing(3.5),
  },
}));

/**
 * Styled component for the content of each tree item
 * Customize the appearance of the tree item content here
 */
const CustomTreeItemContent = styled(TreeItem2Content)(({ theme }) => ({
  flexDirection: 'row-reverse',
  borderRadius: theme.spacing(0.7),
  marginBottom: theme.spacing(0.5),
  marginTop: theme.spacing(0.5),
  padding: theme.spacing(0.5),
  paddingRight: theme.spacing(1),
  fontWeight: 500,
  [`&.Mui-expanded`]: {
    '&:not(.Mui-focused, .Mui-selected, .Mui-selected.Mui-focused) .labelIcon':
      {
        color: theme.palette.text.primary,
        fontWeight: 'bold',
      },
    '&::before': {
      content: '""',
      display: 'block',
      position: 'absolute',
      left: '16px',
      top: '44px',
      height: 'calc(100% - 48px)',
      width: '1.5px',
      backgroundColor: theme.palette.grey[300],
    },
  },
  '&:hover': {
    backgroundColor: alpha(theme.palette.primary.main, 0.1),
    color: theme.palette.text.primary,
  },
  [`&.Mui-focused, &.Mui-selected, &.Mui-selected.Mui-focused`]: {
    backgroundColor: alpha(theme.palette.primary.main, 0.2),
    color: theme.palette.text.primary,
    fontWeight: 'bold',
  },
}));

/**
 * Styled component for the label of each tree item
 * Customize the appearance of the tree item label here
 */
const StyledTreeItemLabel = styled(Typography)({
  fontWeight: 'inherit',
  flexGrow: 1,
});

/**
 * Custom transition component for smooth expand/collapse animations
 *
 * @param props - TransitionProps from Material-UI
 */
function TransitionComponent(props: TransitionProps) {
  return (
    <Collapse
      {...props}
      timeout="auto"
      easing={{
        enter: 'cubic-bezier(0.0, 0, 0.2, 1)',
        exit: 'cubic-bezier(0.4, 0, 1, 1)',
      }}
      style={{
        transitionProperty: 'opacity, height',
      }}
    >
      {props.children}
    </Collapse>
  );
}

/**
 * Styled wrapper for the TransitionComponent to add fade effect
 */
const StyledTransitionComponent = styled(TransitionComponent)(({ theme }) => ({
  '& .MuiCollapse-wrapperInner': {
    opacity: 0,
    transition: theme.transitions.create('opacity', {
      duration: theme.transitions.duration.standard,
    }),
  },
  '&.MuiCollapse-entered .MuiCollapse-wrapperInner': {
    opacity: 1,
  },
}));

// CustomLabel component
interface CustomLabelProps {
  children: React.ReactNode;
  icon?: React.ElementType;
}

/**
 * Custom label component for each tree item
 *
 * @param props - CustomLabelProps
 */
function CustomLabel({ icon: Icon, children, ...other }: CustomLabelProps) {
  return (
    <TreeItem2Label
      {...other}
      sx={{
        display: 'flex',
        alignItems: 'center',
      }}
    >
      {Icon && (
        <Box
          component={Icon}
          className="labelIcon"
          color="inherit"
          sx={{ mr: 0.5, fontSize: '1.5rem' }}
        />
      )}
      <StyledTreeItemLabel variant="body2">{children}</StyledTreeItemLabel>
    </TreeItem2Label>
  );
}

/**
 * Custom TreeItem component
 * This component is responsible for rendering each item in the tree
 */
const CustomTreeItem = React.forwardRef<HTMLLIElement, TreeItem2Props>(
  (props, ref) => {
    const { id, itemId, label, disabled, children, ...other } = props;

    const {
      getRootProps,
      getContentProps,
      getIconContainerProps,
      getCheckboxProps,
      getLabelProps,
      getGroupTransitionProps,
      status,
      publicAPI,
    } = useTreeItem2({ id, itemId, children, label, disabled, rootRef: ref });

    const item = publicAPI.getItem(itemId);
    const icon = getIconFromType(item.type);

    return (
      <TreeItem2Provider itemId={itemId}>
        <StyledTreeItemRoot {...getRootProps(other as Record<string, unknown>)}>
          <CustomTreeItemContent
            {...getContentProps({
              className: clsx('content', {
                'Mui-expanded': status.expanded,
                'Mui-selected': status.selected,
                'Mui-focused': status.focused,
                'Mui-disabled': status.disabled,
              }),
            })}
          >
            <TreeItem2IconContainer {...getIconContainerProps()}>
              <TreeItem2Icon status={status} />
            </TreeItem2IconContainer>
            <TreeItem2Checkbox {...getCheckboxProps()} />
            <CustomLabel
              {...getLabelProps({
                icon,
              })}
            />
          </CustomTreeItemContent>
          {children && (
            <StyledTransitionComponent {...getGroupTransitionProps()} />
          )}
        </StyledTreeItemRoot>
      </TreeItem2Provider>
    );
  },
);

CustomTreeItem.displayName = 'CustomTreeItem';

/**
 * Main LocationTree component
 * This component renders the entire tree structure
 *
 * @param props - LocationTreeProps
 */
const LocationTree: React.FC<LocationTreeProps> = ({ items, onItemClick }) => {
  const handleNodeClick = (_event: React.SyntheticEvent, itemId: string) => {
    onItemClick(itemId);
  };

  return (
    <RichTreeView
      aria-label="location navigator"
      slots={{ item: CustomTreeItem }}
      items={items}
      onItemClick={handleNodeClick}
      sx={{
        height: 'fit-content',
        flexGrow: 1,
        maxWidth: 400,
        overflowY: 'auto',
      }}
    />
  );
};

export default LocationTree;
