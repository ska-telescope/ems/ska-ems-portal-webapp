import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { Button, Paper, Tooltip } from '@mui/material';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid2';
import Typography from '@mui/material/Typography';
import React from 'react';

import { TokenData } from '@/api';
import { formatEpochDate } from '@components/apiTokens/utils/dateUtils.ts';

/**
 * Defines the props for the TokenDisplay component
 */
interface TokenDisplayProps {
  tokenData: TokenData;
  onClick: () => void;
  onCancel: () => void;
}

/**
 * TokenDisplay component
 * This component displays token information after token creation and allows the user to return to the token list.
 */
const TokenDisplay: React.FC<TokenDisplayProps> = ({
  tokenData,
  onClick,
  onCancel,
}) => {
  const handleCopy = async () => {
    try {
      await navigator.clipboard.writeText(tokenData.token);
    } catch (err) {
      console.error('Failed to copy text: ', err);
    }
  };

  return (
    <Grid container spacing={3} sx={{ width: '100%', maxWidth: 700 }}>
      <Grid size={{ xs: 12 }}>
        <Typography variant="h5">Token Details</Typography>
      </Grid>
      <Grid size={{ xs: 12 }}>
        <Typography>Scope: {tokenData.scope}</Typography>
      </Grid>
      <Grid size={{ xs: 12 }}>
        <Typography>Description: {tokenData.description}</Typography>
      </Grid>
      <Grid size={{ xs: 12 }}>
        <Typography>
          Expires At:{' '}
          {tokenData.expires_at
            ? formatEpochDate(tokenData.expires_at)
            : 'Never'}
        </Typography>
      </Grid>
      <Grid size={{ xs: 12 }}>
        <Paper
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            border: '1px solid',
            borderColor: 'primary.main',
            width: '100%',
            maxWidth: '100%',
          }}
        >
          <Box
            sx={{
              p: '20px',
              wordWrap: 'break-word',
              overflowWrap: 'break-word',
              flexGrow: 1,
              minWidth: 0,
            }}
          >
            {tokenData.token}
          </Box>
          <Tooltip title="Copy to clipboard">
            <Button onClick={handleCopy} sx={{ minWidth: '30px' }}>
              <ContentCopyIcon sx={{ height: '20px', color: 'primary' }} />
            </Button>
          </Tooltip>
        </Paper>
      </Grid>
      <Grid size={{ xs: 12 }} justifyContent="flex-end">
        <Button
          variant="text"
          onClick={onCancel}
          size="medium"
          sx={{
            textTransform: 'none',
          }}
        >
          Back
        </Button>
        <Button
          size="medium"
          variant="contained"
          color="primary"
          onClick={onClick}
          style={{ marginLeft: '10px' }}
          sx={{ textTransform: 'none' }}
        >
          Ok
        </Button>
      </Grid>
    </Grid>
  );
};
export default TokenDisplay;
