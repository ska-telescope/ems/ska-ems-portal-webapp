import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { describe, it, expect, vi } from 'vitest';

import { TokenData } from '@/api';

import TokenDisplay from './TokenDisplay';

const writeText = vi.fn();

Object.assign(navigator, {
  clipboard: {
    writeText,
  },
});

describe('TokenDisplay Component', () => {
  const mockOnClick = vi.fn();
  const mockOnCancel = vi.fn();

  const tokenData: TokenData = {
    id: 1,
    client_id: '',
    token: '12345-abcde-67890',
    scope: 'read',
    description: 'Test API Token',
    expires_at: '',
    last_used: '',
    creation_date: '1234567890',
  };

  it('renders token information', () => {
    render(
      <TokenDisplay
        tokenData={tokenData}
        onClick={mockOnClick}
        onCancel={mockOnCancel}
      />,
    );

    expect(screen.getByText('Token Details')).toBeInTheDocument();
    expect(screen.getByText('Scope: read')).toBeInTheDocument();
    expect(screen.getByText('Description: Test API Token')).toBeInTheDocument();
    expect(screen.getByText('Expires At: Never')).toBeInTheDocument();
    expect(screen.getByText(tokenData.token)).toBeInTheDocument();
  });

  it('calls onClick when the Ok button is clicked', () => {
    render(
      <TokenDisplay
        tokenData={tokenData}
        onClick={mockOnClick}
        onCancel={mockOnCancel}
      />,
    );

    const okButton = screen.getByText('Ok');
    fireEvent.click(okButton);

    expect(mockOnClick).toHaveBeenCalledTimes(1);
  });

  it('calls onCancel when the Back button is clicked', () => {
    render(
      <TokenDisplay
        tokenData={tokenData}
        onClick={mockOnClick}
        onCancel={mockOnCancel}
      />,
    );

    const backButton = screen.getByText('Back');
    fireEvent.click(backButton);

    expect(mockOnCancel).toHaveBeenCalledTimes(1);
  });

  it('copies the token to the clipboard when the Copy button is clicked', async () => {
    render(
      <TokenDisplay
        tokenData={tokenData}
        onClick={mockOnClick}
        onCancel={mockOnCancel}
      />,
    );

    const copyButton = screen.getByRole('button', {
      name: /copy to clipboard/i,
    });

    fireEvent.click(copyButton);

    await waitFor(() => {
      expect(navigator.clipboard.writeText).toHaveBeenCalledWith(
        tokenData.token,
      );
    });
  });
});
