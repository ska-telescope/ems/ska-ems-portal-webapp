import { useMsal } from '@azure/msal-react';
import { useEffect, useState } from 'react';

import {
  deleteApiToken,
  fetchTokens,
  generateApiToken,
  TokenData,
} from '@/api';
import useAuthApiClient from '@/auth/hooks/useAuthApiClient.ts';
import { useTokenStore } from '@/stores/useGatewayTokenStore.ts';
import TokenDisplay from '@components/apiTokens/TokenDisplay/TokenDisplay.tsx';
import TokenForm from '@components/apiTokens/TokenForm/TokenForm.tsx';
import TokenList from '@components/apiTokens/TokenList/TokenList.tsx';
import ActionModal from '@components/common/ActionModal/ActionModal.tsx';

const ApiTokens = () => {
  const [rows, setRows] = useState<TokenData[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [selectedTokenId, setSelectedTokenId] = useState<number | null>(null);
  const { accounts } = useMsal();
  const authApiClient = useAuthApiClient();

  const {
    showForm,
    showResult,
    tokenData,
    setShowForm,
    setShowResult,
    setTokenData,
    reset,
  } = useTokenStore();

  const loadTokens = async () => {
    try {
      setLoading(true);
      const tokens = await fetchTokens(accounts[0].homeAccountId);
      setRows(tokens);
    } catch (error) {
      console.error('Error fetching tokens:', error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    loadTokens(); // Fetch tokens on component mount
    return () => {
      reset(); // Reset the token store when leaving the component
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleShowForm = () => {
    setShowForm(true);
  };

  const handleFormSubmit = async (
    expiration: string,
    scope: string,
    description: string,
  ) => {
    try {
      const result = await generateApiToken(
        accounts[0].homeAccountId,
        expiration,
        scope,
        description,
        authApiClient,
      );
      setTokenData(result);
      setShowResult(true);

      await loadTokens();
    } catch (error) {
      console.error('Error generating token:', error);
    }
  };

  const handleCancelForm = () => {
    reset(); // Reset the state and show the table
    loadTokens();
  };

  const handleDeleteClick = (id: number) => {
    setSelectedTokenId(id);
    setOpenDeleteDialog(true);
  };

  const confirmDelete = async () => {
    if (selectedTokenId !== null) {
      try {
        await deleteApiToken(selectedTokenId);
        setOpenDeleteDialog(false);
        setRows((prevRows) =>
          prevRows.filter((row) => row.id !== selectedTokenId),
        );
      } catch (error) {
        console.error('Error deleting token:', error);
      }
    }
  };

  const handleCloseDialog = () => {
    setOpenDeleteDialog(false);
  };

  const handleBackToTable = () => {
    reset();
  };

  let content;

  if (showForm) {
    content = (
      <TokenForm onSubmit={handleFormSubmit} onCancel={handleCancelForm} />
    );
  } else if (showResult && tokenData) {
    content = (
      <TokenDisplay
        tokenData={tokenData}
        onClick={handleBackToTable}
        onCancel={handleShowForm}
      />
    );
  } else {
    content = (
      <TokenList
        rows={rows}
        loading={loading}
        onDeleteClick={handleDeleteClick}
        onClick={handleShowForm}
      />
    );
  }

  return (
    <>
      {content}
      {/* Confirmation Modal for Deleting a Token */}
      <ActionModal
        open={openDeleteDialog}
        onClose={handleCloseDialog}
        onClick={confirmDelete}
        dialogContentText={
          'Are you sure you want to delete this token? This action cannot be undone.'
        }
        dialogTitle={'Confirm Deletion'}
      />
    </>
  );
};

export default ApiTokens;
