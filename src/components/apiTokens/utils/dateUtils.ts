import { format, fromUnixTime } from 'date-fns';

/**
 * Converts an epoch timestamp (in seconds) to a formatted date string.
 */
export const formatEpochDate = (epochString: string): string => {
  const epochTime = parseInt(epochString, 10); // Convert the string to a number
  return format(fromUnixTime(epochTime), 'MMM do yyyy, HH:mm:ss');
};
