import { render, screen, fireEvent, within } from '@testing-library/react';

import TokenForm from './TokenForm.tsx';

describe('TokenForm Component', () => {
  const mockOnSubmit = vi.fn();
  const mockOnCancel = vi.fn();

  it('renders the form with default values', () => {
    render(<TokenForm onSubmit={mockOnSubmit} onCancel={mockOnCancel} />);

    const descriptionInput = screen.getByLabelText(
      /API Token Description/i,
    ) as HTMLInputElement;
    expect(descriptionInput.value).toMatch(/^API Token from/);

    const expirationSelect = screen.getByLabelText(/API Token Expiration/i);
    expect(expirationSelect).toHaveTextContent('Never');
    expect(screen.getByLabelText(/Read Only/i)).toBeChecked();
  });

  it('submits the form with correct values', () => {
    render(<TokenForm onSubmit={mockOnSubmit} onCancel={mockOnCancel} />);

    const descriptionInput = screen.getByLabelText(/API Token Description/i);
    fireEvent.change(descriptionInput, {
      target: { value: 'Custom Token Description' },
    });

    const expirationSelect = screen.getByLabelText(/API Token Expiration/i);
    fireEvent.mouseDown(expirationSelect);
    const option = screen.getByText('1 Month');
    fireEvent.click(option);

    const scopeRadio = screen.getByLabelText(/Read & Write/i);
    fireEvent.click(scopeRadio);

    expect(screen.getByLabelText(/Read & Write/i)).toBeChecked();

    const submitButton = screen.getByText(/Submit/i);
    fireEvent.click(submitButton);

    expect(mockOnSubmit).toHaveBeenCalledTimes(1);
    expect(mockOnSubmit).toHaveBeenCalledWith(
      '1 Month',
      'Read & Write',
      'Custom Token Description',
    );
  });

  it('cancels the form submission', () => {
    render(<TokenForm onSubmit={mockOnSubmit} onCancel={mockOnCancel} />);

    const cancelButton = screen.getByText(/Cancel/i);
    fireEvent.click(cancelButton);

    expect(mockOnCancel).toHaveBeenCalledTimes(1);
  });

  it('renders token expiration options', () => {
    render(<TokenForm onSubmit={mockOnSubmit} onCancel={mockOnCancel} />);

    const expirationSelect = screen.getByRole('combobox', {
      name: /API Token Expiration/i,
    });
    fireEvent.mouseDown(expirationSelect);

    const listbox = screen.getByRole('listbox');

    const options = within(listbox).getAllByRole('option');

    expect(options).toHaveLength(6);
    expect(within(listbox).getByText('Never')).toBeInTheDocument();
    expect(within(listbox).getByText('1 Month')).toBeInTheDocument();
    expect(within(listbox).getByText('2 Months')).toBeInTheDocument();
    expect(within(listbox).getByText('6 Months')).toBeInTheDocument();
    expect(within(listbox).getByText('1 Year')).toBeInTheDocument();
    expect(within(listbox).getByText('2 Years')).toBeInTheDocument();
  });

  it('displays token scope helper text', () => {
    render(<TokenForm onSubmit={mockOnSubmit} onCancel={mockOnCancel} />);

    expect(
      screen.getByText(/Read operations are methods of type GET/i),
    ).toBeInTheDocument();
  });
});
