import {
  Button,
  FormControl,
  Select,
  MenuItem,
  Radio,
  RadioGroup,
  FormHelperText,
} from '@mui/material';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import Grid from '@mui/material/Grid2';
import OutlinedInput from '@mui/material/OutlinedInput';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/system';
import React, { useEffect, useState } from 'react';

/**
 * Defines the props for the ApiTokenFormProps component
 */
interface ApiTokenFormProps {
  onSubmit: (expiration: string, scope: string, description: string) => void;
  onCancel: () => void;
}

/**
 * Defines the token expiration options
 */
const ExpirationOptions = {
  NEVER: 'Never',
  ONE_MONTH: '1 Month',
  TWO_MONTHS: '2 Months',
  SIX_MONTHS: '6 Months',
  ONE_YEAR: '1 Year',
  TWO_YEARS: '2 Years',
};

/**
 * Styled Grid component for the form display
 */
const FormGrid = styled(Grid)(() => ({
  display: 'flex',
  flexDirection: 'column',
}));

/**
 * TokenForm component
 * Form component for API Gateway tokens with cancel and submit actions.
 */
const TokenForm: React.FC<ApiTokenFormProps> = ({ onSubmit, onCancel }) => {
  const [description, setDescription] = useState('');
  const [scope, setScope] = useState('Read Only');
  const [expiration, setExpiration] = useState(ExpirationOptions.NEVER);

  useEffect(() => {
    const today = new Date();
    const formattedDate = today.toISOString().split('T')[0]; // Get the date in YYYY-MM-DD format
    setDescription(`API Token from ${formattedDate}`);
  }, []);

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    onSubmit(expiration, scope, description);
  };

  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={3} sx={{ width: '100%', maxWidth: 500 }}>
        <Typography variant="h6" component="h6">
          New API Token
        </Typography>
        <FormGrid size={{ xs: 12 }}>
          <FormLabel htmlFor="apiDescription" required>
            API Token Description
          </FormLabel>
          <OutlinedInput
            id="apiDescription"
            name="apiDescription"
            type="apiDescription"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
            size="small"
          />
          <Typography variant="caption" color="textSecondary">
            Choose a descriptive name for your API token or use the default.
          </Typography>
        </FormGrid>
        <FormGrid size={{ xs: 12 }}>
          <FormControl variant="outlined" fullWidth size="small" required>
            <FormLabel id="expiration-label">API Token Expiration</FormLabel>
            <Select
              labelId="expiration-label"
              id="apiExpiration"
              value={expiration}
              onChange={(e) => setExpiration(e.target.value)}
              fullWidth
              variant="outlined"
            >
              {Object.entries(ExpirationOptions).map(([key, value]) => (
                <MenuItem key={key} value={value}>
                  {value}
                </MenuItem>
              ))}
            </Select>
            <Typography variant="caption" color="textSecondary">
              Choose how long it takes until the token expires.
            </Typography>
          </FormControl>
        </FormGrid>
        <FormGrid size={{ xs: 12 }}>
          <FormControl variant="outlined" fullWidth size="small" required>
            <FormLabel id="token-scope">Token Scope</FormLabel>
            <RadioGroup
              aria-labelledby="token-scope"
              onChange={(e) => setScope(e.target.value)}
              defaultValue="Read Only"
              name="token-scope"
            >
              <FormControlLabel
                value="Read Only"
                control={<Radio color="default" size="small" />}
                label="Read Only"
              />
              <FormControlLabel
                value="Read & Write"
                control={<Radio color="default" size="small" />}
                label="Read & Write"
              />
            </RadioGroup>
            <FormHelperText>
              Read operations are methods of type GET, HEAD and OPTIONS.
              <br />
              Using any other HTTP method counts as write operation.
            </FormHelperText>
          </FormControl>
        </FormGrid>
        <Grid size={{ xs: 12 }} justifyContent="flex-end">
          <Button
            variant="text"
            onClick={onCancel}
            size="medium"
            sx={{
              textTransform: 'none',
            }}
          >
            Cancel
          </Button>

          <Button
            type="submit"
            variant="contained"
            color="primary"
            size="medium"
            style={{ marginLeft: '10px' }}
            sx={{
              textTransform: 'none',
            }}
          >
            Submit
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default TokenForm;
