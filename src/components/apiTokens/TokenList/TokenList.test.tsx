import { render, screen, fireEvent } from '@testing-library/react';
import { describe, it, expect, vi } from 'vitest';

import { TokenData } from '@/api';

import TokenList from './TokenList.tsx';

const mockTokens: TokenData[] = [
  {
    id: 1,
    description: 'Test Token 1',
    scope: 'read',
    creation_date: '1728384994',
    client_id: '',
    token: '',
    expires_at: '',
    last_used: '',
  },
  {
    id: 2,
    description: 'Test Token 2',
    scope: 'write',
    creation_date: '1728384994',
    expires_at: '1728384994',
    last_used: '1728384994',
    client_id: '',
    token: '',
  },
];

const mockOnDeleteClick = vi.fn();
const mockOnClick = vi.fn();

describe('TokenList Component', () => {
  it('renders the component with the correct headings', () => {
    render(
      <TokenList
        rows={mockTokens}
        loading={false}
        onDeleteClick={mockOnDeleteClick}
        onClick={mockOnClick}
      />,
    );

    expect(screen.getByText(/EMS API Gateway tokens/i)).toBeInTheDocument();
    expect(
      screen.getByText(/API tokens are the authentication method/i),
    ).toBeInTheDocument();
    expect(
      screen.getByText(/The table below will show all the tokens/i),
    ).toBeInTheDocument();
  });

  it('renders the ActionTable with the correct data', () => {
    render(
      <TokenList
        rows={mockTokens}
        loading={false}
        onDeleteClick={mockOnDeleteClick}
        onClick={mockOnClick}
      />,
    );

    expect(screen.getByText('Test Token 1')).toBeInTheDocument();
    expect(screen.getByText('Test Token 2')).toBeInTheDocument();
    expect(screen.getByText('read')).toBeInTheDocument();
    expect(screen.getByText('write')).toBeInTheDocument();
    const dateOccurrences = screen.getAllByText(
      /Oct 8th 2024, \d{2}:\d{2}:\d{2}/,
    );
    expect(dateOccurrences).toHaveLength(4);
    const neverElements = screen.getAllByText('Never');
    expect(neverElements).toHaveLength(2);
  });

  it('renders "loading" when loading is true', () => {
    render(
      <TokenList
        rows={[]}
        loading={true}
        onDeleteClick={mockOnDeleteClick}
        onClick={mockOnClick}
      />,
    );

    expect(screen.getByText('Loading...')).toBeInTheDocument();
  });

  it('calls the onClick function when "New API Token" button is clicked', () => {
    render(
      <TokenList
        rows={mockTokens}
        loading={false}
        onDeleteClick={mockOnDeleteClick}
        onClick={mockOnClick}
      />,
    );

    const button = screen.getByText('New API Token');
    fireEvent.click(button);

    expect(mockOnClick).toHaveBeenCalledTimes(1);
  });

  it('calls onDeleteClick when the delete button is clicked in the ActionTable', () => {
    render(
      <TokenList
        rows={mockTokens}
        loading={false}
        onDeleteClick={mockOnDeleteClick}
        onClick={mockOnClick}
      />,
    );

    const deleteButton1 = screen.getByTestId('delete-button-1');
    fireEvent.click(deleteButton1);
    expect(mockOnDeleteClick).toHaveBeenCalledTimes(1);
    expect(mockOnDeleteClick).toHaveBeenCalledWith(1);

    const deleteButton2 = screen.getByTestId('delete-button-2');
    fireEvent.click(deleteButton2);
    expect(mockOnDeleteClick).toHaveBeenCalledTimes(2);
    expect(mockOnDeleteClick).toHaveBeenCalledWith(2);
  });
});
