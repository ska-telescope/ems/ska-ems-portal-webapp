import { Typography, Button, Box } from '@mui/material';
import React from 'react';

import { TokenData } from '@/api';
import { formatEpochDate } from '@components/apiTokens/utils/dateUtils.ts';
import ActionTable from '@components/common/ActionTable/ActionTable.tsx';

/**
 * Defines the props for the TokenList component
 */
interface GatewayTokensProps {
  rows: TokenData[];
  loading: boolean;
  onDeleteClick: (id: number) => void;
  onClick: () => void;
}

/**
 * TokenList component
 * This component includes the token management interface components, such as
 *  - The user tokens table
 *  - Create a new token button
 */
const TokenList: React.FC<GatewayTokensProps> = ({
  rows,
  loading,
  onDeleteClick,
  onClick,
}) => {
  return (
    <>
      <Typography variant="h4" component="h4" gutterBottom>
        EMS API Gateway tokens
      </Typography>
      <Typography variant="subtitle1" gutterBottom>
        API tokens are the authentication method used in the EMS API Gateway.
        <br />
        Create new tokens for your API connections with the button in this
        screen.
      </Typography>
      <Typography variant="subtitle1" gutterBottom>
        The table below will show all the tokens associated to your user.
      </Typography>
      <ActionTable
        rows={rows}
        loading={loading}
        onDeleteClick={onDeleteClick}
        columns={[
          {
            key: 'description',
            header: 'Description',
            render: (row: TokenData) => row.description,
          },
          {
            key: 'scope',
            header: 'Scope',
            render: (row: TokenData) => row.scope,
          },
          {
            key: 'creation_date',
            header: 'Creation Date',
            render: (row: TokenData) => formatEpochDate(row.creation_date),
          },
          {
            key: 'expires_at',
            header: 'Expiration Date',
            render: (row: TokenData) =>
              row.expires_at ? formatEpochDate(row.expires_at) : 'Never',
          },
          {
            key: 'last_used',
            header: 'Last Used',
            render: (row: TokenData) =>
              row.last_used ? formatEpochDate(row.last_used) : 'Never',
          },
        ]}
      />
      <Box mt={2} textAlign="right">
        <Button
          size="medium"
          variant="contained"
          color="secondary"
          onClick={onClick}
          sx={{ textTransform: 'none' }}
        >
          New API Token
        </Button>
      </Box>
    </>
  );
};

export default TokenList;
