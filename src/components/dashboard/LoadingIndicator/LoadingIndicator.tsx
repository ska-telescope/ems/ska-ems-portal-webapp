/****
 * Loading Indicator Component
 *
 * This component displays a loading indicator to be used when a loading condition is fulfilled.
 */
import { CircularProgress, Typography, Box } from '@mui/material';

import { useCustomTheme } from '@hooks/useCustomTheme.ts';

function LoadingIndicator() {
  const { colors } = useCustomTheme();

  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      {' '}
      <Box sx={{ position: 'relative', display: 'inline-flex' }}>
        <CircularProgress
          sx={{ color: colors.customAccentPrimary }}
          disableShrink
          size={150}
          thickness={2}
        />
        <Box
          sx={{
            position: 'absolute',
            top: 10,
            left: 0,
            bottom: 0,
            right: 0,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Typography
            variant="body2"
            sx={{ color: colors.textSecondary, textAlign: 'center' }}
          >
            Loading data
          </Typography>
        </Box>
      </Box>
    </Box>
  );
}

export default LoadingIndicator;
