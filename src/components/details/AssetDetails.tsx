import {
  CircularProgress,
  Alert,
  Box,
  Typography,
  Divider,
} from '@mui/material';
import { useTheme } from '@mui/material/styles';

import { useAssetDetails } from '@/api/assetDetails/assetDetails';
import useAuthApiClient from '@/auth/hooks/useAuthApiClient.ts';
import ContentContainer from '@/components/common/ContentContainer/ContentContainer';

import GenericSmall from '../charts/GenericSmall/GenericSmall';

// Test Data Example
const testData = [
  { category: 'A', value: 30 },
  { category: 'B', value: 70 },
];

interface FieldRowProps {
  label: string;
  value: string | number | boolean;
  testId: string;
}

const FieldRow = ({ label, value, testId }: FieldRowProps) => {
  return (
    <Box
      display="flex"
      justifyContent="space-between"
      mb={1}
      data-testid={`field-row-${testId}`}
    >
      <Typography
        component="span"
        sx={{ width: '40%', fontWeight: 'medium' }}
        data-testid={`field-label-${testId}`}
      >
        {label}
      </Typography>
      <Typography
        component="span"
        sx={{ fontWeight: 'bold' }}
        data-testid={`field-value-${testId}`}
      >
        {typeof value === 'boolean' ? (value ? 'Yes' : 'No') : value}
      </Typography>
    </Box>
  );
};

const AssetDetailsComponent = ({ assetId }: { assetId: string }) => {
  const authApiClient = useAuthApiClient();
  const { data, isLoading, error } = useAssetDetails(assetId, authApiClient);
  const themeMode = localStorage.getItem('themeMode');
  const theme = useTheme();

  if (isLoading) {
    return (
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        minHeight="200px"
        data-testid="asset-details-loading"
      >
        <CircularProgress />
      </Box>
    );
  }

  if (error) {
    return (
      <Box p={2} data-testid="asset-details-error">
        <Alert severity="error">Error loading asset details</Alert>
      </Box>
    );
  }

  if (!data) return null;

  return (
    <>
      <ContentContainer
        backgroundColor={
          themeMode === 'light' ? theme.palette.background.default : 'black'
        }
        backgroundOpacity={0.7}
        style={{ flex: '1 1 0', minWidth: 0 }}
        data-testid="asset-details-content"
      >
        <Typography
          variant="h6"
          gutterBottom
          sx={{ fontWeight: 'bold' }}
          data-testid="asset-details-title"
        >
          Asset Details - {data.itemNumber}
        </Typography>
        <Divider sx={{ mb: 2 }} />
        <Box display="flex" flexWrap="wrap" justifyContent="space-between">
          <Box width={{ xs: '100%', md: '48%' }}>
            <FieldRow
              label="Item Number"
              value={data.itemNumber}
              testId="item-number"
            />
            <FieldRow label="Version" value={data.version} testId="version" />
            <FieldRow
              label="Description"
              value={data.description}
              testId="description"
            />
            <FieldRow label="Level" value={data.level} testId="level" />
            <FieldRow
              label="Config Item"
              value={data.configItem}
              testId="config-item"
            />
            <FieldRow
              label="Sequence Number"
              value={data.seqNumber}
              testId="seq-number"
            />
          </Box>
          <Box width={{ xs: '100%', md: '48%' }}>
            <FieldRow label="UOM" value={data.uom} testId="uom" />
            <FieldRow label="Status" value={data.status} testId="status" />
            <FieldRow
              label="Quantity Per"
              value={data.quantityPer}
              testId="quantity-per"
            />
            <FieldRow
              label="Item Class"
              value={data.itemClass}
              testId="item-class"
            />
            <FieldRow
              label="Software"
              value={data.software}
              testId="software"
            />
            <FieldRow
              label="Serialised"
              value={data.serialised}
              testId="serialised"
            />
            <FieldRow
              label="Under Change"
              value={data.underChange}
              testId="under-change"
            />
            <FieldRow
              label="Main Equipment"
              value={data.mainEquipment}
              testId="main-equipment"
            />
          </Box>
        </Box>
      </ContentContainer>
      <Box display="flex" justifyContent="space-between" mt={4}>
        <ContentContainer
          backgroundColor={
            themeMode === 'light' ? theme.palette.background.default : 'black'
          }
          backgroundOpacity={0.7}
          p={3}
          mb={2}
          style={{ flex: '1 1 30%', marginRight: '10px' }}
          data-testid="asset-details-chart-1"
        >
          <GenericSmall
            type="donut"
            data={testData}
            title="Line Chart Example"
          />
        </ContentContainer>
        <ContentContainer
          backgroundColor={
            themeMode === 'light' ? theme.palette.background.default : 'black'
          }
          backgroundOpacity={0.7}
          p={3}
          mb={2}
          style={{ flex: '1 1 30%', marginRight: '10px' }}
          data-testid="asset-details-chart-2"
        >
          <GenericSmall
            type="line"
            data={testData}
            title="Donut Chart Example"
          />
        </ContentContainer>
        <ContentContainer
          backgroundColor={
            themeMode === 'light' ? theme.palette.background.default : 'black'
          }
          backgroundOpacity={0.7}
          p={3}
          style={{ flex: '1 1 30%' }}
          data-testid="asset-details-chart-3"
        >
          <GenericSmall type="bar" data={testData} title="Bar Chart Example" />
        </ContentContainer>
      </Box>
    </>
  );
};

export default AssetDetailsComponent;
