/****
 * Asset Details Component
 *
 * This component displays asset details using the assetsLocationId from the parent Location component.
 * Uses the existing useAssetDetails hook for data fetching.
 */

import {
  CircularProgress,
  Alert,
  Box,
  Paper,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Typography,
  Divider,
} from '@mui/material';

import { useAssetDetails } from '@/api/assetDetails/assetDetails';
import useAuthApiClient from '@/auth/hooks/useAuthApiClient.ts';

interface FieldRowProps {
  label: string;
  value: string | number | boolean;
  testId: string;
}

const FieldRow = ({ label, value, testId }: FieldRowProps) => {
  return (
    <TableRow data-testid={`field-row-${testId}`}>
      <TableCell
        component="th"
        scope="row"
        sx={{ width: '40%', fontWeight: 'medium' }}
      >
        {label}
      </TableCell>
      <TableCell data-testid={`field-value-${testId}`}>
        {typeof value === 'boolean' ? (value ? 'Yes' : 'No') : value}
      </TableCell>
    </TableRow>
  );
};

const AssetDetailsComponent = ({ assetId }: { assetId: string }) => {
  const authApiClient = useAuthApiClient();

  const { data, isLoading, error } = useAssetDetails(assetId, authApiClient);

  if (isLoading) {
    return (
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        minHeight="200px"
        data-testid="asset-details-loading"
      >
        <CircularProgress />
      </Box>
    );
  }

  if (error) {
    return (
      <Box p={2} data-testid="asset-details-error">
        <Alert severity="error">Error loading asset details</Alert>
      </Box>
    );
  }

  if (!data) return null;

  return (
    <Paper elevation={2} sx={{ mt: 2 }} data-testid="asset-details-card">
      <Box p={3}>
        <Typography variant="h6" gutterBottom data-testid="asset-details-title">
          Asset Details - {data.itemNumber}
        </Typography>
        <Divider sx={{ mb: 2 }} />
        <Table size="small" data-testid="asset-details-content">
          <TableBody>
            <FieldRow
              label="Item Number"
              value={data.itemNumber}
              testId="item-number"
            />
            <FieldRow label="Version" value={data.version} testId="version" />
            <FieldRow
              label="Description"
              value={data.description}
              testId="description"
            />
            <FieldRow label="Level" value={data.level} testId="level" />
            <FieldRow
              label="Config Item"
              value={data.configItem}
              testId="config-item"
            />
            <FieldRow
              label="Sequence Number"
              value={data.seqNumber}
              testId="seq-number"
            />
            <FieldRow label="UOM" value={data.uom} testId="uom" />
            <FieldRow label="Status" value={data.status} testId="status" />
            <FieldRow
              label="Quantity Per"
              value={data.quantityPer}
              testId="quantity-per"
            />
            <FieldRow
              label="Item Class"
              value={data.itemClass}
              testId="item-class"
            />
            <FieldRow
              label="Software"
              value={data.software}
              testId="software"
            />
            <FieldRow
              label="Serialised"
              value={data.serialised}
              testId="serialised"
            />
            <FieldRow
              label="Under Change"
              value={data.underChange}
              testId="under-change"
            />
            <FieldRow
              label="Main Equipment"
              value={data.mainEquipment}
              testId="main-equipment"
            />
          </TableBody>
        </Table>
      </Box>
    </Paper>
  );
};

export default AssetDetailsComponent;
