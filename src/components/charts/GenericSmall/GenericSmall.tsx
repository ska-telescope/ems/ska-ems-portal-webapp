/* eslint-disable import/named */
import { AgChartOptions } from 'ag-charts-community';
import { AgCharts } from 'ag-charts-react';
import React from 'react';

import { useAgChartTheme } from '@hooks/useAgChartTheme.ts';

// TODO: This component either serves as an example or it needs refactoring

interface ChartData {
  category: string;
  value: number;
}

interface ChartProps {
  type: 'bar' | 'donut' | 'line';
  data: ChartData[];
  title: string;
}

const AgChartComponent = ({ type, data, title }: ChartProps) => {
  const agChartTheme = useAgChartTheme();

  const options: AgChartOptions = React.useMemo(() => {
    switch (type) {
      case 'bar':
        return {
          data,
          title: {
            text: title,
          },
          background: {
            visible: false,
          },
          series: [
            {
              type: 'bar',
              xKey: 'category',
              yKey: 'value',
            },
          ],
          axes: [
            {
              type: 'category',
              position: 'bottom',
              title: { text: 'Category' },
            },
            { type: 'number', position: 'left', title: { text: 'Value' } },
          ],
          theme: agChartTheme,
        };
      case 'donut':
        return {
          data,
          title: {
            text: title,
          },
          background: {
            visible: false,
          },
          series: [
            {
              type: 'pie', // Use 'pie' for donut charts
              angleKey: 'value',
              labelKey: 'category',
              innerRadiusRatio: 0.7, // Makes it a hollow donut
            },
          ],
          theme: agChartTheme,
        };
      case 'line':
        return {
          data,
          title: {
            text: title,
          },
          background: {
            visible: false,
          },
          series: [
            {
              type: 'line',
              xKey: 'category',
              yKey: 'value',
            },
          ],
          axes: [
            {
              type: 'category',
              position: 'bottom',
              title: { text: 'Category' },
            },
            { type: 'number', position: 'left', title: { text: 'Value' } },
          ],
          theme: agChartTheme,
        };
      default:
        return {} as AgChartOptions; // Fallback to an empty object
    }
  }, [type, data, title, agChartTheme]);

  return <AgCharts options={options} />;
};

export default AgChartComponent;
