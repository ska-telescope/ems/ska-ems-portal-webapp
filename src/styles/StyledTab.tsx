import { styled } from '@mui/material/styles';
import Tab from '@mui/material/Tab';

// Styled Tab component for consistent size and animations, adapting to theme
export const StyledTab = styled(Tab)(({ theme }) => ({
  border: '0.5px solid',
  borderColor: theme.palette.divider,
  backgroundColor: theme.palette.background.paper,
  marginRight: '-0.5px',
  height: '48px', // Fixed height for consistency
  padding: '6px 16px',
  textTransform: 'none',
  color: theme.palette.text.primary,
  width: '150px', // Ensure all tabs have the same width
  '&.Mui-selected': {
    backgroundColor: theme.palette.action.selected,
    color: theme.palette.text.primary,
    zIndex: 1,
    borderBottom: 'none',
  },
  borderRadius: 0,
  '&:first-of-type': {
    borderTopLeftRadius: '4px',
  },
  '&:last-of-type': {
    borderTopRightRadius: '4px',
    marginRight: 0,
    position: 'relative',
    '&::after': {
      content: '""',
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      width: '0.1px',
      backgroundColor: theme.palette.divider,
    },
  },
}));
