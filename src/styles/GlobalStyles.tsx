import { GlobalStyles as MUIGlobalStyles } from '@mui/material';

const GlobalStyles = () => (
  <MUIGlobalStyles
    styles={(theme) => ({
      body: {
        margin: 0,
        padding: 0,
        backgroundColor: theme.palette.background.default,
        color: theme.palette.text.primary,
        fontFamily: theme.typography.fontFamily,
      },
      a: {
        color: theme.palette.primary.main,
        textDecoration: 'none',
        '&:hover': {
          textDecoration: 'underline',
        },
      },
      '.MuiButton-root': {
        borderRadius: theme.shape.borderRadius,
      },
      // Add other global styles as needed
    })}
  />
);

export default GlobalStyles;
