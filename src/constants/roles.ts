// Core Roles
export const SUPER_ADMIN = 'SuperAdmin';
export const ADMIN = 'Admin';
export const EDIT = 'Edit';
export const READ = 'Read';

// Group-Based Role Variations
export const MID_ADMIN = 'MIDAdmin';
export const MID_EDIT = 'MIDEdit';
export const MID_READ = 'MIDRead';
export const LOW_ADMIN = 'LOWAdmin';
export const LOW_EDIT = 'LOWEdit';
export const LOW_READ = 'LOWRead';

// Define All Base Roles (Including MID & LOW Variants)
export const BASE_ROLES = [
  SUPER_ADMIN,
  ADMIN,
  EDIT,
  READ,
  MID_ADMIN,
  MID_EDIT,
  MID_READ,
  LOW_ADMIN,
  LOW_EDIT,
  LOW_READ,
];

// Extended roles (for potential future use)
export const EXTENDED_ROLES: string[] = [];

// All Roles Combined (Used in Role Validation)
export const ALL_ROLES = [...BASE_ROLES, ...EXTENDED_ROLES];
