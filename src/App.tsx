import { PublicClientApplication } from '@azure/msal-browser';
import { MsalProvider, useIsAuthenticated, useMsal } from '@azure/msal-react';
import { CssBaseline, Box, useTheme } from '@mui/material';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { AgCharts as AgChartsEnterprise } from 'ag-charts-enterprise';
import { LicenseManager } from 'ag-grid-enterprise';
import { useEffect } from 'react';
import {
  Navigate,
  Route,
  BrowserRouter as Router,
  Routes,
  useLocation,
} from 'react-router-dom';

import { useUserRoles } from '@/auth/hooks/useUserRoles';
import Login from '@/auth/Login/Login';
import { msalConfig } from '@/authConfig';
import RoutesRenderer from '@/routes/RoutesRenderer';
import { CustomThemeProvider } from '@/theme/ThemeProvider';
import Sidebar from '@components/common/Sidebar/Sidebar';
import Topbar from '@components/common/Topbar/Topbar';
import BackgroundWrapper from '@components/dataGrids/common/BackgroundWrapper/BackgroundWrapper';
import { usePageInfo } from '@hooks/usePageInfo';

import usePreloadData from './hooks/usePreloadHooks';

// Initialize MSAL instance
const msalInstance = new PublicClientApplication(msalConfig);
const queryClient = new QueryClient();

// Set the AG Grid and Charts Enterprise license key
LicenseManager.setLicenseKey(window.env.AG_GRID_LICENSE_KEY);
AgChartsEnterprise.setLicenseKey(window.env.AG_GRID_LICENSE_KEY);

/**
 * AppContent Component
 *
 * Handles authentication, role-based access, and renders the main application layout.
 * Redirects users to the login page if they are not authenticated.
 */
const AppContent = () => {
  const isAuthenticated = useIsAuthenticated();
  const { inProgress, accounts, instance } = useMsal();
  const { baseRoles } = useUserRoles();
  const hasValidRoles = baseRoles.length > 0;
  const { title, subtitle } = usePageInfo();
  const location = useLocation();

  // TODO: Fetch the locations in the Locations page
  // Preload essential data for the application
  usePreloadData();

  // Ensure the user account is set after login
  useEffect(() => {
    if (accounts.length > 0) {
      instance.setActiveAccount(accounts[0]);
    }
  }, [accounts, instance]);

  // Wait for MSAL to fully initialize before making redirect decisions
  if (inProgress !== 'none') {
    return <div>Loading...</div>; // Prevents premature redirection
  }

  // If the user is not authenticated, show the login page
  if (inProgress === 'none' && !isAuthenticated) {
    return (
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<Navigate to="/login" replace />} />
      </Routes>
    );
  }

  // Compute subtitle dynamically based on user role access
  const computedSubtitle = hasValidRoles
    ? subtitle // Use specific subtitle if roles exist
    : 'Some sections may be restricted based on your role'; // Default message for users with no roles

  return (
    <>
      {/* Sidebar navigation (visible only for authenticated users) */}
      <Sidebar />
      <Box
        sx={{
          flexGrow: 1,
          marginLeft: `var(--sidebar-width)`,
          marginTop: `var(--topbar-height)`,
        }}
      >
        {/* Top navigation bar */}
        <Topbar
          title={title}
          subtitle={computedSubtitle}
          userName={accounts[0]?.name ?? ''}
        />

        {/* Main content area */}
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            p: 3,
            overflow: 'auto',
            display: 'flex',
            flexDirection: 'column',
            height: `calc(100vh - var(--topbar-height))`,
          }}
        >
          <BackgroundWrapper />
          <RoutesRenderer key={location.pathname} />
        </Box>
      </Box>
    </>
  );
};

/**
 * App Component (Root)
 *
 * Sets up the overall application structure, including:
 * - MSAL authentication provider
 * - Query client provider for API calls
 * - Theme provider for consistent UI styling
 * - React Router for client-side navigation
 */
const App = () => {
  return (
    <MsalProvider instance={msalInstance}>
      <QueryClientProvider client={queryClient}>
        <CustomThemeProvider>
          <CssBaseline />
          <Router basename={window.env.BASE_URL || '/'}>
            <Box
              sx={{
                display: 'flex',
                minHeight: '100vh', // Ensure full viewport height
                overflow: 'hidden', // Prevent scrolling on the body
                '--topbar-height': '100px',
                '--sidebar-width': `${useTheme().spacing(8)}`,
              }}
            >
              <AppContent />
            </Box>
          </Router>
        </CustomThemeProvider>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </MsalProvider>
  );
};

export default App;
