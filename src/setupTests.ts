import '@testing-library/jest-dom';
import { vi } from 'vitest';

// Mock the `window.env` object with the required properties
global.window.env = {
  EMS_BACKEND_URL: 'https://mock-backend-url.com',
};

// Mock useCustomTheme globally
vi.mock('@/hooks/useCustomTheme', () => ({
  useCustomTheme: () => ({
    colors: {
      customAccentPrimary: '#ff0000',
      customAccentQuinary: '#00ff00',
      customAccentSecondary: '#0000ff',
      customAccentTertiary: '#ff00ff',
      customAccentQuaternary: '#ffff00',
      customContainerBackground: '#f0f0f0',
    },
    spacing: (value: number) => `${value * 8}px`,
  }),
}));
