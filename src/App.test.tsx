import { createTheme, ThemeProvider } from '@mui/material/styles';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { render, screen } from '@testing-library/react';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { beforeEach, describe, expect, test, vi } from 'vitest';

import App from '@/App';

// TODO: Address skipped tests

// Mock AG Grid License Manager
vi.mock('ag-grid-enterprise', () => ({
  LicenseManager: {
    setLicenseKey: vi.fn(),
  },
}));

// Mock react-query-devtools to avoid implementation errors
vi.mock('@tanstack/react-query-devtools', () => ({
  ReactQueryDevtools: () => null,
}));

// Create a mock function for usePreloadData
const mockUsePreloadData = vi.fn(() => ({ isLoading: false, isError: false }));

// Mock usePreloadData hook
vi.mock('@/hooks/usePreloadHooks', () => ({
  default: () => mockUsePreloadData(),
}));

// Mock react-router-dom
vi.mock('react-router-dom', async () => {
  const actual = await vi.importActual('react-router-dom');
  return {
    ...actual,
    BrowserRouter: ({ children }: { children: React.ReactNode }) => (
      <>{children}</>
    ),
  };
});

// Mock authentication hooks
vi.mock('@azure/msal-react', async (importOriginal) => {
  const actual = (await importOriginal()) as typeof import('@azure/msal-react'); // Explicitly type actual

  return {
    ...actual,
    useIsAuthenticated: () => true, // or false depending on the test case
    useMsal: () => ({
      inProgress: 'none', // Set 'none' to bypass loading state
      accounts: [{ name: 'Test User' }],
      instance: { setActiveAccount: vi.fn() },
    }),
  };
});

// Mock the components
vi.mock('@components/common/Sidebar/Sidebar', () => ({
  default: () => (
    <div data-testid="mock-sidebar" role="navigation">
      Sidebar
    </div>
  ),
}));

vi.mock('@components/common/Topbar/Topbar', () => ({
  default: ({ title, subtitle }: { title: string; subtitle: string }) => (
    <header data-testid="mock-topbar">
      <span data-testid="topbar-title">{title}</span>
      <span data-testid="topbar-subtitle">{subtitle}</span>
    </header>
  ),
}));

vi.mock('@pages/Home/Home', () => ({
  default: () => <div data-testid="mock-home">Home Page</div>,
}));

vi.mock('@pages/Locations/Locations', () => ({
  default: () => <div data-testid="mock-locations">Locations Page</div>,
}));

vi.mock('@pages/Settings/Settings', () => ({
  default: () => <div data-testid="mock-settings">Settings Page</div>,
}));

vi.mock('@/hooks/usePageInfo', () => ({
  usePageInfo: () => ({ title: 'Test Title', subtitle: 'Test Subtitle' }),
}));

// Test QueryClient setup
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
    },
  },
});

const theme = createTheme();

// Helper function to render with all required providers
const renderWithProviders = (ui: React.ReactElement, { route = '/' } = {}) => {
  window.history.pushState({}, 'Test page', route);

  return render(
    <QueryClientProvider client={queryClient}>
      <ThemeProvider theme={theme}>
        <MemoryRouter initialEntries={[route]}>{ui}</MemoryRouter>
      </ThemeProvider>
    </QueryClientProvider>,
  );
};

describe('App', () => {
  beforeEach(() => {
    // Clear any previous render effects
    queryClient.clear();
    mockUsePreloadData.mockClear();
  });

  // eslint-disable-next-line vitest/no-disabled-tests
  test.skip('renders loading state when preload data is loading', async () => {
    mockUsePreloadData.mockReturnValueOnce({
      isLoading: true,
      isError: false,
    });

    renderWithProviders(<App />);
    expect(screen.getByText('Loading...')).toBeInTheDocument();
  });

  // eslint-disable-next-line vitest/no-disabled-tests
  test.skip('renders error state when preload data fails', () => {
    mockUsePreloadData.mockReturnValueOnce({
      isLoading: false,
      isError: true,
    });

    renderWithProviders(<App />);
    expect(screen.getByText('Error loading initial data')).toBeInTheDocument();
  });

  test('renders main application structure when data is loaded', () => {
    renderWithProviders(<App />);

    expect(screen.getByTestId('mock-sidebar')).toBeInTheDocument();
    expect(screen.getByTestId('mock-topbar')).toBeInTheDocument();
    expect(screen.getByRole('main')).toBeInTheDocument();
  });

  // eslint-disable-next-line vitest/no-disabled-tests
  test.skip('renders Home page by default', () => {
    renderWithProviders(<App />);
    expect(screen.getByTestId('mock-home')).toBeInTheDocument();
  });

  // eslint-disable-next-line vitest/no-disabled-tests
  test.skip('renders Locations page when navigated to /locations', () => {
    renderWithProviders(<App />, { route: '/locations' });
    expect(screen.getByTestId('mock-locations')).toBeInTheDocument();
  });

  test('renders Settings page when navigated to /settings', () => {
    renderWithProviders(<App />, { route: '/settings' });
    expect(screen.getByTestId('mock-settings')).toBeInTheDocument();
  });

  // eslint-disable-next-line vitest/no-disabled-tests
  test.skip('renders Topbar with correct title and subtitle', () => {
    renderWithProviders(<App />);

    expect(screen.getByTestId('topbar-title')).toHaveTextContent('Test Title');
    expect(screen.getByTestId('topbar-subtitle')).toHaveTextContent(
      'Test Subtitle',
    );
  });

  test('layout structure is correctly ordered', () => {
    renderWithProviders(<App />);

    const sidebar = screen.getByRole('navigation');
    const topbar = screen.getByRole('banner');
    const main = screen.getByRole('main');

    expect(sidebar).toBeInTheDocument();
    expect(topbar).toBeInTheDocument();
    expect(main).toBeInTheDocument();
  });

  test('main content has correct base styling', () => {
    renderWithProviders(<App />);

    const main = screen.getByRole('main');

    expect(main).toHaveStyle({
      display: 'flex',
      flexDirection: 'column',
    });
  });
});
