import CalendarContainer from '@components/calendars/Calendar/Calendar.tsx';
import ContentContainer from '@components/common/ContentContainer/ContentContainer.tsx';
import { useCustomTheme } from '@hooks/useCustomTheme.ts';

const Maintenance = () => {
  const { colors } = useCustomTheme();
  const themeMode = localStorage.getItem('themeMode');

  return (
    <div
      style={{
        display: 'grid',
        height: '100%',
        width: '100%',
        padding: '5px',
        overflow: 'hidden',
      }}
      data-testid="maintenance-container"
    >
      <ContentContainer
        {...(themeMode === 'light' && {
          backgroundColor: colors.customContainerBackground,
          backgroundOpacity: 0.8,
        })}
      >
        <CalendarContainer />
      </ContentContainer>
    </div>
  );
};

export default Maintenance;
