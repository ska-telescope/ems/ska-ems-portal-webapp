import { Tabs, Box } from '@mui/material';
import { useState } from 'react';

import { useUserRoles } from '@/auth/hooks/useUserRoles';
import { StyledTab } from '@/styles/StyledTab';
import ApiTokens from '@components/apiTokens/ApiTokens';
import ContentContainer from '@components/common/ContentContainer/ContentContainer.tsx';
import UserSettings from '@components/user/UserSettings';
import { useCustomTheme } from '@hooks/useCustomTheme.ts';

// TODO: Needs refactoring of Styled tab and the whole component
/**
 * Settings component with tab navigation
 * - Displays different tabs based on user roles
 */
const Settings = () => {
  const [tabValue, setTabValue] = useState(0);
  const { colors } = useCustomTheme();
  const themeMode = localStorage.getItem('themeMode');
  const { isSuperAdmin } = useUserRoles(); // Extracts SuperAdmin flag from user roles

  /**
   * Handles tab change event
   * @param _event - Event triggering the change (not used)
   * @param newValue - The index of the selected tab
   */
  const handleTabChange = (_event: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  /**
   * Dynamically defines available tabs based on user roles
   */
  const tabs = [
    { label: 'User Settings', component: <UserSettings /> },
    ...(isSuperAdmin
      ? [{ label: 'API Tokens', component: <ApiTokens /> }]
      : []),
  ];

  return (
    <ContentContainer
      {...(themeMode === 'light' && {
        backgroundColor: colors.customContainerBackground,
        backgroundOpacity: 0.8,
      })}
      style={{ flex: '1 1 0', minWidth: 0 }}
      data-testid="settings-content"
    >
      {/* Tabs Navigation */}
      <Tabs
        value={tabValue}
        onChange={handleTabChange}
        aria-label="settings tabs"
      >
        {tabs.map((tab) => (
          <StyledTab key={tab.label} label={tab.label} />
        ))}
      </Tabs>

      {/* Render the appropriate component for the selected tab */}
      <Box mt={2}>{tabs[tabValue]?.component}</Box>
    </ContentContainer>
  );
};

export default Settings;
