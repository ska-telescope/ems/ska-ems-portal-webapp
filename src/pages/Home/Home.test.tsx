import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { render, screen } from '@testing-library/react';
import { describe, it, expect } from 'vitest';

import Home from '@pages/Home/Home';

describe('Home', () => {
  it('renders the Home component', () => {
    const queryClient = new QueryClient();
    render(
      <QueryClientProvider client={queryClient}>
        <Home />
      </QueryClientProvider>,
    );
    expect(screen.getByText('Loading data')).toBeInTheDocument();
  });
});
