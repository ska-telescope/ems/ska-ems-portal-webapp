import { Alert } from '@mui/material';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid2';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import { useQuery } from '@tanstack/react-query';

import { fetchDashboard } from '@/api';
import useAuthApiClient from '@/auth/hooks/useAuthApiClient';
import { useCustomTheme } from '@/hooks/useCustomTheme';
import ContentContainer from '@components/common/ContentContainer/ContentContainer.tsx';
import LoadingIndicator from '@components/dashboard/LoadingIndicator/LoadingIndicator.tsx';
import MaintenanceActionByType from '@components/widgets/MaintenanceActionByType/MaintenanceActionByType.tsx';
import MaintenanceStats from '@components/widgets/MaintenanceStats/MaintenanceStats.tsx';
import ProblemReportsByWeek from '@components/widgets/ProblemReportsByWeek/ProblemReportsByWeek.tsx';
import ProblemReportsInfo from '@components/widgets/ProblemReportsInfo/ProblemReportsInfo.tsx';
import StatusOfProblemReports from '@components/widgets/StatusOfProblemReports/StatusOfProblemReports.tsx';
import {
  handleProblemReportClick,
  ProblemReportData,
} from '@components/widgets/utils/widgetUtils.ts';

export type WidgetDataItem = Record<string, unknown>;

interface DashboardWidget {
  id: number;
  widget_type: string;
  title: string;
  widget_key: string;
  data: WidgetDataItem[];
}

interface DashboardResponse {
  id: number;
  title: string;
  is_global: boolean;
  owner_user_id?: string | null;
  widgets: DashboardWidget[];
}
/**
 * Dashboard component that displays a grid of widgets containing system information.
 */
function DashboardCharts() {
  const theme = useTheme();
  const { colors } = useCustomTheme();
  const themeMode = localStorage.getItem('themeMode');
  const authApiClient = useAuthApiClient();

  const gridContainerStyle = {
    display: 'grid',
    gridTemplateColumns: '1fr',
    gridTemplateRows: 'auto auto auto',
    gridTemplateAreas: `
      "reports"
      "stats"
      "info"
    `,
    gap: 2,
    // At widths above 1600 px, switch to two columns
    [theme.breakpoints.up(1600)]: {
      gridTemplateColumns: '3fr 2fr',
      gridTemplateRows: 'auto auto',
      gridTemplateAreas: `
        "reports stats"
        "reports info"
      `,
    },
  } as const;

  const gridReportsStyle = {
    gridArea: 'reports',
    alignSelf: 'start',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100px',
    maxHeight: '1000px',
    height: '100%',
  } as const;

  const gridStatsStyle = {
    gridArea: 'stats',
    alignSelf: 'start',
  } as const;

  const gridInfoStyle = {
    gridArea: 'info',
  } as const;

  const infoInnerBoxStyle = {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    gap: 2,
  } as const;

  const {
    data: dashboard,
    error,
    isLoading,
  } = useQuery<DashboardResponse>({
    queryKey: ['mainDashboard'],
    queryFn: () => fetchDashboard(authApiClient),
  });

  if (isLoading) {
    return <LoadingIndicator />;
  }

  if (error) {
    const errorMessage =
      error instanceof Error ? error.message : 'An unknown error occurred';
    const errorDetails = error instanceof Error ? error.stack : null;

    return (
      <Box p={2} data-testid="home-widgets-error">
        <Alert severity="error">
          <Typography variant="h6">Error loading dashboard widgets</Typography>
          <Typography variant="body2">{errorMessage}</Typography>
          {errorDetails && (
            <Typography variant="body2" sx={{ whiteSpace: 'pre-wrap', mt: 1 }}>
              {errorDetails}
            </Typography>
          )}
        </Alert>
      </Box>
    );
  }

  if (!dashboard) {
    return <div>No dashboard data available.</div>;
  }

  const widgetMap = new Map<string, DashboardWidget>(
    dashboard.widgets.map((widget) => [widget.widget_key, widget]),
  );

  const problemReportsWidget = widgetMap.get('problem_reports_week');
  const variationTasksMonthWidget = widgetMap.get('variation_tasks_per_month');
  const maintenanceActionsToBeResolvedWidget = widgetMap.get(
    'maintenance_actions_to_be_resolved',
  );
  const maintenanceActionsResolvedWidget = widgetMap.get(
    'maintenance_actions_resolved',
  );
  const openReportsPieWidget = widgetMap.get('open_problem_reports');
  const latestIssuesWidget = widgetMap.get('latest_issues');
  const maintenanceActionByTypeWidget = widgetMap.get('maintenance_by_type');

  const hasAllMaintenanceData =
    maintenanceActionsResolvedWidget?.data?.length &&
    maintenanceActionsToBeResolvedWidget?.data?.length &&
    variationTasksMonthWidget?.data?.length;

  return (
    <div>
      {/* Outer Box */}
      <Box sx={{ maxWidth: '100%', boxSizing: 'border-box' }}>
        {/* Main Grid */}
        <Grid container sx={gridContainerStyle}>
          {/* Reports Grid Area */}
          <Grid sx={gridReportsStyle}>
            <ContentContainer
              {...(themeMode === 'light' && {
                maxWidth: '100%',
                backgroundColor: colors.customContainerBackground,
                backgroundOpacity: 0.8,
              })}
              sx={{
                minHeight: 500,
              }}
            >
              {problemReportsWidget?.data ? (
                <ProblemReportsByWeek
                  chartDataset={problemReportsWidget.data.map((item) => ({
                    ...item,
                    x: new Date(item.x as string),
                  }))}
                />
              ) : (
                <p>No problem reports data</p>
              )}
            </ContentContainer>
          </Grid>

          {/* Stats Grid Area */}
          <Grid sx={gridStatsStyle}>
            <ContentContainer
              maxWidth="100%"
              // backgroundColor={colors.customContainerBackground}
              {...(themeMode === 'light' && {
                backgroundColor: colors.customContainerBackground,
                backgroundOpacity: 0.8,
              })}
              sx={{ flexDirection: 'row', alignItems: 'center', padding: 2 }}
            >
              {hasAllMaintenanceData && (
                <MaintenanceStats
                  totalCompletedLastMonth={
                    maintenanceActionsResolvedWidget.data[0]
                      .total_completed_last_month as number
                  }
                  totalNotCompletedLastMonth={
                    maintenanceActionsToBeResolvedWidget.data[0]
                      .total_not_completed as number
                  }
                  numbers={
                    variationTasksMonthWidget.data.map(
                      (entry) => entry.total_completed,
                    ) as number[]
                  }
                />
              )}
            </ContentContainer>
          </Grid>

          {/* Info Grid Area */}
          <Grid sx={gridInfoStyle}>
            <Box sx={infoInnerBoxStyle}>
              <ContentContainer
                {...(themeMode === 'light' && {
                  maxWidth: '100%',
                  backgroundColor: colors.customContainerBackground,
                  backgroundOpacity: 0.8,
                })}
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  padding: 2,
                  maxWidth: '100%',
                  height: '100%',
                }}
              >
                {openReportsPieWidget?.data && (
                  <StatusOfProblemReports
                    data={
                      openReportsPieWidget.data as {
                        id: number;
                        label: string;
                        value: number;
                      }[]
                    }
                    onItemClick={(_event, d) =>
                      handleProblemReportClick(
                        openReportsPieWidget.data as unknown as ProblemReportData[],
                        d.dataIndex,
                      )
                    }
                  />
                )}
              </ContentContainer>

              {/* Latest Issues */}
              <ContentContainer
                {...(themeMode === 'light' && {
                  maxWidth: '100%',
                  backgroundColor: colors.customContainerBackground,
                  backgroundOpacity: 0.8,
                })}
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'flex-start',
                  padding: 2,
                  height: '100%',
                  maxWidth: '100%',
                }}
              >
                {latestIssuesWidget?.data && (
                  <ProblemReportsInfo
                    dataLatestIssues={
                      latestIssuesWidget.data as {
                        jira_key: string;
                        summary: string;
                        description: string;
                        url: string;
                      }[]
                    }
                  />
                )}
              </ContentContainer>
            </Box>
          </Grid>
        </Grid>
      </Box>

      {/* Maintenance Action By Type */}
      <Box sx={{ width: '100%', height: '500px', my: 2 }}>
        <ContentContainer
          {...(themeMode === 'light' && {
            maxWidth: '100%',
            backgroundColor: colors.customContainerBackground,
            backgroundOpacity: 0.8,
          })}
          sx={{
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          {maintenanceActionByTypeWidget?.data && (
            <MaintenanceActionByType
              maintenanceChartData={
                maintenanceActionByTypeWidget.data as {
                  month: string;
                  preventative: number;
                  unplanned: number;
                  planned: number;
                  workRequests: number;
                  materials: number;
                }[]
              }
            />
          )}
        </ContentContainer>
      </Box>
      <Box sx={{ height: 2 }} />
    </div>
  );
}

const Home = () => {
  return <DashboardCharts />;
};

export default Home;
