import { render, screen } from '@testing-library/react';
import { describe, it, expect, vi, beforeEach } from 'vitest';

import LocationTree from '@/components/trees/LocationTree/LocationTree';

import Location from './Locations';

// Define types for location data structure
interface LocationNode {
  id: string;
  label: string;
  type: number;
  children?: LocationNode[];
}

interface LocationStore {
  locations: LocationNode[] | null;
  setSelectedNode: (node: LocationNode) => void;
  getAncestors: (id: string) => LocationNode[];
  selectedNode: LocationNode | null;
  clearSelectedNode: () => void;
}

// Mock the ContentContainer component
vi.mock('@/components/common/ContentContainer/ContentContainer', () => ({
  default: ({ children, 'data-testid': testId }: any) => (
    <div data-testid={testId}>{children}</div>
  ),
}));

// Mock the LocationTree component with a spy
vi.mock('@/components/trees/LocationTree/LocationTree', () => ({
  default: vi.fn(
    ({
      items,
      onItemClick,
    }: {
      items: LocationNode[];
      onItemClick: (id: string) => void;
    }) => (
      <div data-testid="location-tree">
        <span data-testid="tree-items">{JSON.stringify(items)}</span>
        <button onClick={() => onItemClick('leaf-node-1')}>
          Select Leaf Node
        </button>
        <button onClick={() => onItemClick('parent-node-1')}>
          Select Parent Node
        </button>
      </div>
    ),
  ),
}));

// Mock the AssetsGrid component
vi.mock('@/components/dataGrids/AssetGrid/AssetGrid', () => ({
  default: ({ locationId }: { locationId: string }) => (
    <div data-testid="asset-grid">Asset Grid for {locationId}</div>
  ),
}));

// Mock the location store with sample data structure
const mockLocations: LocationNode[] = [
  {
    id: '1538',
    label: 'United Kingdom',
    type: 1,
    children: [
      {
        id: '2588',
        label: 'GHQ',
        type: 2,
      },
      {
        id: '3200',
        label: 'GHQ-Cluster S08',
        type: 2,
      },
    ],
  },
];

// Create a mock store with default values
const defaultMockStore: LocationStore = {
  locations: mockLocations,
  setSelectedNode: vi.fn(),
  getAncestors: (id: string) => [
    { id: '1538', label: 'United Kingdom', type: 1 },
    { id: id, label: id === '2588' ? 'GHQ' : 'GHQ-Cluster S08', type: 2 },
  ],
  selectedNode: { id: '2588', label: 'GHQ', type: 2 },
  clearSelectedNode: vi.fn(),
};

// Mock the useLocationStore hook
const mockUseLocationStore = vi.fn<[], LocationStore>(() => defaultMockStore);
vi.mock('@/stores/useLocationStore', () => ({
  useLocationStore: () => mockUseLocationStore(),
}));

describe('Location', () => {
  beforeEach(() => {
    vi.clearAllMocks();
    mockUseLocationStore.mockReturnValue(defaultMockStore);
  });

  it('renders without crashing', () => {
    render(<Location />);
    expect(screen.getByTestId('location-tree')).toBeInTheDocument();
  });

  it('passes correct data structure to LocationTree', () => {
    render(<Location />);
    expect(LocationTree).toHaveBeenCalledWith(
      expect.objectContaining({
        items: expect.arrayContaining([
          expect.objectContaining({
            id: '1538',
            label: 'United Kingdom',
            type: 1,
            children: expect.arrayContaining([
              expect.objectContaining({
                id: '2588',
                label: 'GHQ',
                type: 2,
              }),
              expect.objectContaining({
                id: '3200',
                label: 'GHQ-Cluster S08',
                type: 2,
              }),
            ]),
          }),
        ]),
      }),
      expect.any(Object),
    );
  });

  it('transforms location data correctly', () => {
    render(<Location />);
    const treeItems = screen.getByTestId('tree-items');
    const parsedItems = JSON.parse(treeItems.textContent || '[]');

    expect(parsedItems).toEqual(mockLocations);
  });

  it('shows loading state when locations are not available', () => {
    mockUseLocationStore.mockReturnValue({
      ...defaultMockStore,
      locations: null,
    });

    render(<Location />);
    expect(screen.getByTestId('location-loading')).toBeInTheDocument();
  });

  it('displays breadcrumbs with correct hierarchy', () => {
    render(<Location />);
    const breadcrumbs = screen.getByTestId('location-breadcrumbs');

    expect(breadcrumbs).toBeInTheDocument();
    expect(screen.getByText('United Kingdom')).toBeInTheDocument();
    expect(screen.getByText('GHQ')).toBeInTheDocument();
  });

  it('maintains data structure integrity', () => {
    render(<Location />);
    const mockCall = vi.mocked(LocationTree).mock.calls[0][0];

    expect(mockCall.items[0]).toEqual(
      expect.objectContaining({
        id: '1538',
        label: 'United Kingdom',
        type: 1,
        children: expect.arrayContaining([
          expect.objectContaining({
            id: '2588',
            label: 'GHQ',
            type: 2,
          }),
          expect.objectContaining({
            id: '3200',
            label: 'GHQ-Cluster S08',
            type: 2,
          }),
        ]),
      }),
    );
  });
});
