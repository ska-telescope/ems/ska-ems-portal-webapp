import { Tabs } from '@mui/material';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Typography from '@mui/material/Typography';
import React, { useEffect, useState } from 'react';

import ContentContainer from '@/components/common/ContentContainer/ContentContainer';
import TabPanel from '@/components/common/TabPanel/TabPanel';
import AssetsGrid from '@/components/dataGrids/AssetGrid/AssetGrid';
import DocumentsGrid from '@/components/dataGrids/DocumetsGrid/DocumentsGrid';
import ProblemReports from '@/components/dataGrids/ProblemReportsGrid/ProblemReportsGrid';
import WorkOrdersGrid from '@/components/dataGrids/WorkOrdersGrid/WorkOrdersGrid';
import AssetDetailsComponent from '@/components/details/AssetDetails';
import LocationTree from '@/components/trees/LocationTree/LocationTree';
import { useLocationStore } from '@/stores/useLocationStore';
import { StyledTab } from '@/styles/StyledTab';
import { DownloadFileButton } from '@components/common/DownloadFileButton/DownloadFileButton.tsx';
import { useCustomTheme } from '@hooks/useCustomTheme.ts';

interface DynamicTab {
  label: string;
  content: React.ReactNode;
}

/**
 * Location component that displays a hierarchical view of locations with details.
 * Provides navigation through a tree structure and displays location details
 * in a tabbed interface for leaf nodes.
 *
 * @returns {JSX.Element} A component displaying location hierarchy and details
 *
 * @example
 * ```tsx
 * <Location />
 * ```
 */
const Location = () => {
  const {
    locations,
    setSelectedNode,
    getAncestors,
    selectedNode,
    clearSelectedNode,
  } = useLocationStore();
  const { colors } = useCustomTheme();
  const themeMode = localStorage.getItem('themeMode');

  const [tabValue, setTabValue] = useState(0);
  const [assetsLocationId, setAssetsLocationId] = useState<string | null>(null);
  const [dynamicTabs, setDynamicTabs] = useState<DynamicTab[]>([]);
  const [isRemovingTabs, setIsRemovingTabs] = useState(false);
  const [isAddingTabs, setIsAddingTabs] = useState(false);

  useEffect(() => {
    return () => {
      clearSelectedNode();
    };
  }, [clearSelectedNode]);

  /**
   * Handles the selection of a location item in the tree
   * @param {string} itemId - The ID of the selected location
   */
  const handleItemClick = (itemId: string) => {
    const node = getAncestors(itemId).pop(); // Get the clicked node
    if (node) {
      setSelectedNode(node);
      setTabValue(0); // Reset to the first tab when a new node is selected

      // Check if node is a leaf node
      const isLeafNode = !node.children || node.children.length === 0;
      if (isLeafNode) {
        // Trigger the fade-out animation for old tabs before clearing them
        setIsRemovingTabs(true);
        setTimeout(() => {
          setAssetsLocationId(node.id);
          setDynamicTabs([]); // Clear dynamic tabs after fade-out animation
          setIsRemovingTabs(false);

          // Once old tabs are removed, set flag to animate new tabs
          setIsAddingTabs(true);
        }, 500); // Match the fade-out duration
      } else {
        setAssetsLocationId(null);
        setDynamicTabs([]);
      }
    }
  };

  /**
   * Handles tab change events
   * @param {React.SyntheticEvent} _event - The event object
   * @param {number} newValue - The index of the newly selected tab
   */
  const handleTabChange = (_event: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  /**
   * Handles adding new tabs when an action button is clicked in AssetGrid
   * @param {string} assetId - The ID of the selected asset
   */
  const handleAssetActionClick = (assetId: string) => {
    // Create new tabs with your custom components as content
    const newTabs: DynamicTab[] = [
      {
        label: `Overview`,
        content: <AssetDetailsComponent assetId={assetId} />,
      },
      {
        label: `Work Orders`,
        content: <WorkOrdersGrid assetId={assetId} />,
      },
      {
        label: `Documents`,
        content: <DocumentsGrid assetId={assetId} />,
      },
      {
        label: `Problem Reports`,
        content: <ProblemReports assetId={assetId} />,
      },
    ];
    setDynamicTabs(newTabs);
    setTabValue(1); // Set focus to the first newly added tab
    setIsAddingTabs(false); // Reset adding flag after tabs are added
  };

  if (!locations) {
    return <div data-testid="location-loading">Loading...</div>;
  }

  const breadcrumbs = selectedNode
    ? getAncestors(selectedNode.id).map((node, index, array) => (
        <Typography
          key={node.id}
          sx={{
            fontSize: '14px',
            ...(index === array.length - 1
              ? {
                  fontWeight: 600,
                  color: 'text.primary',
                }
              : {
                  color: 'text.secondary',
                  textDecoration: 'none',
                  '&:hover': {
                    textDecoration: 'underline',
                    cursor: 'pointer',
                  },
                }),
          }}
        >
          {node.label}
        </Typography>
      ))
    : [];

  // Show tabs only if a leaf node is selected
  const showTabs = !!assetsLocationId;

  return (
    <div
      style={{
        display: 'flex',
        height: '100%',
        width: '100%',
        padding: '5px',
        overflow: 'hidden',
      }}
      data-testid="location-container"
    >
      <ContentContainer
        maxWidth={360}
        {...(themeMode === 'light' && {
          backgroundColor: colors.customContainerBackground,
          backgroundOpacity: 0.8,
        })}
        flexBasis="360px"
        style={{ marginRight: '10px' }}
        data-testid="location-navigation"
      >
        <LocationTree items={locations} onItemClick={handleItemClick} />
      </ContentContainer>

      <ContentContainer
        {...(themeMode === 'light' && {
          backgroundColor: colors.customContainerBackground,
          backgroundOpacity: 0.8,
        })}
        style={{ flex: '1 1 0', minWidth: 0 }}
        data-testid="location-content"
      >
        <Breadcrumbs
          aria-label="breadcrumb"
          data-testid="location-breadcrumbs"
          sx={{
            marginBottom: '15px',
            '& .MuiBreadcrumbs-separator': {
              fontSize: '14px',
            },
          }}
        >
          {breadcrumbs}
        </Breadcrumbs>

        {showTabs && (
          <>
            <div style={{ position: 'relative', width: '100%' }}>
              <Tabs
                value={tabValue}
                onChange={handleTabChange}
                aria-label="location tabs"
                data-testid="location-tabs"
                sx={{
                  position: 'relative',
                  '& .MuiTabs-indicator': {
                    display: 'none',
                  },
                  width: 'fit-content',
                  minHeight: '48px', // Ensure minHeight is consistent
                }}
              >
                <StyledTab
                  label="Part List"
                  id="location-tab-1"
                  aria-controls="location-tabpanel-1"
                  data-testid="location-tab-parts"
                />
                {dynamicTabs.map((tab, index) => (
                  <StyledTab
                    key={index}
                    label={tab.label}
                    id={`location-tab-${index + 2}`}
                    aria-controls={`location-tabpanel-${index + 2}`}
                    className={
                      isRemovingTabs
                        ? 'fade-out-animation'
                        : isAddingTabs
                          ? 'new-tab-animation'
                          : ''
                    }
                  />
                ))}
              </Tabs>
              {/* Bottom border line */}
              <div
                style={{
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  right: 0,
                  height: '0.5px',
                  backgroundColor: colors.textPrimary,
                }}
              />
            </div>
            <TabPanel value={tabValue} index={0}>
              <DownloadFileButton locationId={assetsLocationId} />
              {assetsLocationId && (
                <AssetsGrid
                  key={assetsLocationId}
                  locationId={assetsLocationId}
                  onActionClick={handleAssetActionClick}
                />
              )}
            </TabPanel>
            {dynamicTabs.map((tab, index) => (
              <TabPanel key={index} value={tabValue} index={index + 1}>
                {tab.content}
              </TabPanel>
            ))}
          </>
        )}
      </ContentContainer>
    </div>
  );
};

export default Location;
