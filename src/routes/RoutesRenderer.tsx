import { ReactElement, isValidElement } from 'react';
import { useRoutes, Navigate } from 'react-router-dom';

import { useUserRoles } from '@/auth/hooks/useUserRoles';
import ProtectedRoute from '@/auth/ProtectedRoute/ProtectedRoute';
import { appRoutes } from '@/routes/routesConfig';

/**
 * RoutesRenderer Component
 *
 * Dynamically renders application routes while enforcing role-based access control.
 * Ensures that:
 * - Users without required roles are redirected to `/unauthorized`
 * - Invalid routes are filtered out
 * - Unrecognized routes are redirected to `/unauthorized`
 */
const RoutesRenderer = () => {
  const { baseRoles } = useUserRoles();

  /**
   * Generates application routes:
   * - Wraps protected routes inside `<ProtectedRoute />`
   * - Redirects users without access to `/unauthorized`
   * - Ensures only valid React elements are included
   */
  const filteredRoutes = appRoutes
    .map((route) => {
      if (!route.element || !isValidElement(route.element)) {
        return null; // Skip invalid elements
      }

      let wrappedElement: ReactElement = route.element;

      // Apply role-based protection if requiredRoles are specified
      if (route.requiredRoles) {
        const hasAccess = baseRoles.some((role) =>
          route.requiredRoles!.includes(role),
        );

        wrappedElement = hasAccess ? (
          <ProtectedRoute>{route.element}</ProtectedRoute>
        ) : (
          <Navigate to="/unauthorized" replace />
        );
      }

      return { path: route.path, element: wrappedElement };
    })
    .filter(
      (route): route is { path: string; element: ReactElement } =>
        route !== null,
    ); // Remove null values and enforce correct type

  // Ensure non-matching routes are redirected to `/unauthorized`
  filteredRoutes.push({
    path: '*',
    element: <Navigate to="/unauthorized" replace />,
  });

  return useRoutes(filteredRoutes);
};

export default RoutesRenderer;
