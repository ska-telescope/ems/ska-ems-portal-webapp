import { RouteObject } from 'react-router-dom';

import Login from '@/auth/Login/Login';
import UnauthorizedPage from '@/auth/UnauthorizedPage/UnauthorizedPage';
import { ALL_ROLES } from '@/constants/roles';
import Home from '@/pages/Home/Home';
import Locations from '@/pages/Locations/Locations';
import Maintenance from '@/pages/Maintenance/Maintenance';
import Settings from '@/pages/Settings/Settings';

/**
 * AppRoute Interface
 *
 * Extends RouteObject while enforcing:
 * - Explicit `path` definition (string)
 * - Optional `requiredRoles` for role-based access control
 */
export interface AppRoute extends Omit<RouteObject, 'children'> {
  path: string; // Explicitly define the path property for better type safety
  requiredRoles?: string[]; // Optional: Role-based access control
}

/**
 * Application Routes Configuration
 *
 * Defines all application routes in a centralized location.
 * - Public routes (login, unauthorized)
 * - Protected routes (require specific roles)
 * - Wildcard (*) fallback route
 */
export const appRoutes: AppRoute[] = [
  // Public Routes (No authentication required)
  { path: '/login', element: <Login /> },
  { path: '/unauthorized', element: <UnauthorizedPage /> },

  // Protected Routes (Require at least 'Read' role)
  { path: '/', element: <Home />, requiredRoles: ALL_ROLES },
  {
    path: '/locations',
    element: <Locations />,
    requiredRoles: ALL_ROLES,
  },
  {
    path: '/maintenance',
    element: <Maintenance />,
    requiredRoles: ALL_ROLES,
  },

  // Settings Page (Partially restricted: Accessible to all, but certain tabs may be role-based)
  { path: '/settings', element: <Settings /> },

  // Fallback Route (Redirects unknown routes to Home, but still requires authentication)
  { path: '*', element: <Home />, requiredRoles: ALL_ROLES },
];
