/* eslint-disable @typescript-eslint/no-explicit-any */

/* DON'T EDIT THIS FILE DIRECTLY

Run: "make dev-local-env" to update it
*/

declare global {
  interface Window {
    env: any;
  }
}

type EnvType = {
  BASE_URL: string;
  EMS_BACKEND_URL: string;
  AG_GRID_LICENSE_KEY: string;
  MSENTRA_CLIENT_ID: string;
  MSENTRA_CLIENT_SECRET: string;
  MSENTRA_TENANT_ID: string;
};
export const env: EnvType = {
  ...process.env,
  ...window.env,
};
