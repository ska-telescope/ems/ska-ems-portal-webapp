/**
 * Gateway Token API Client
 *
 * This module provides functions to interact with the API Gateway token endpoints of our API.
 * It handles the fetching and management functionalities of API Gateway tokens.
 */
export interface TokenData {
  id: number;
  client_id: string;
  token: string;
  expires_at: string;
  scope: string;
  description: string;
  creation_date: string;
  last_used: string;
}

export const generateApiToken = async (
  client_id: string | undefined,
  expiration: string,
  scope: string,
  description: string,
  authApiClient: (url: string, options?: RequestInit) => Promise<Response>,
): Promise<TokenData> => {
  const requestBody = {
    client_id,
    expiration,
    scope,
    description,
  };

  try {
    const response = await authApiClient(
      `${window.env.EMS_BACKEND_URL}/generate-api-gateway-token`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody),
      },
    );

    if (!response.ok) {
      throw new Error('Failed to generate token');
    }
    const tokenData: TokenData = await response.json();
    return tokenData;
  } catch (error) {
    console.error('Error:', error);
    throw error;
  }
};

export const fetchTokens = async (
  client_id: string | undefined,
): Promise<TokenData[]> => {
  try {
    const response = await fetch(
      `${window.env.EMS_BACKEND_URL}/tokens/${client_id}`,
    );

    if (!response.ok) {
      throw new Error('Failed to fetch tokens');
    }

    return await response.json();
  } catch (error) {
    console.error('Error fetching tokens:', error);
    throw error;
  }
};

export const deleteApiToken = async (token_id: number): Promise<void> => {
  try {
    const response = await fetch(
      `${window.env.EMS_BACKEND_URL}/delete-token/${token_id}`,
      {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );

    if (!response.ok) {
      throw new Error(`Failed to delete token with ID ${token_id}`);
    }

    console.log(`Token with ID ${token_id} deleted successfully.`);
  } catch (error) {
    console.error('Error deleting token:', error);
    throw error; // Re-throw the error so it can be handled by the caller
  }
};
