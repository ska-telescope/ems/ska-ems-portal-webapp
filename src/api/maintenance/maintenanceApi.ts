/**
 * Engineering Management API Client
 *
 * This module provides functions to interact with the engineering management endpoints of our API.
 * It handles the fetching and filtering of engineering management tasks for the maintenance calendar.
 */
interface ApiTask {
  id: number;
  name: string;
  description: string;
  created_date: string;
  last_edited: string;
  location_name: string;
  region_name: string;
  asset_name: string;
  asset_part_number: string;
  status: number | null;
  priority: number | null;
  due_date: string | null;
  assigned_to: string | null;
}

// TODO: Refactor this function to improve code quality, (See MR !15, Comment #456789)
export const fetchEvents = async (
  start: Date,
  end: Date,
  authApiClient: (url: string, options?: RequestInit) => Promise<Response>,
  status?: boolean,
  priority?: number[],
): Promise<ApiTask[]> => {
  const startDateISO = start.toISOString();
  const endDateISO = end.toISOString();

  let url = `${window.env.EMS_BACKEND_URL}/tasks?start=${startDateISO}&end=${endDateISO}`;

  if (status) {
    url += `&status=0`;
  }

  if (priority && priority.length > 0) {
    const priorityParams = priority.map((p) => `priority=${p}`).join('&');
    url += `&${priorityParams}`;
  }

  const response = await authApiClient(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  });

  if (!response.ok) {
    throw new Error('Failed to fetch events');
  }

  return await response.json();
};

export default ApiTask;
