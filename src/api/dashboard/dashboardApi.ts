/**
 * Dashboard API Client
 *
 * This module provides functions to interact with the dashboard related endpoints of our API.
 * It handles the fetching of detailed data for widgets in the main app dashboard.
 */
let QUERY_DATA_URL = `${window.env.EMS_BACKEND_URL}/dashboards`;

/**
 * Fetches the entire main dashboard data from the backend.
 *
 * @throws Will throw an error if the response is not OK or parsing fails.
 */
export const fetchDashboard = async (
  authApiClient: (url: string, options?: RequestInit) => Promise<Response>,
) => {
  const response = await authApiClient(QUERY_DATA_URL, { method: 'GET' });

  if (!response.ok) {
    throw new Error(
      `Fetching main dashboard failed with status ${response.status}`,
    );
  }

  try {
    return await response.json();
  } catch (err) {
    throw new Error(`Failed to parse JSON for main dashboard: ${String(err)}`);
  }
};
