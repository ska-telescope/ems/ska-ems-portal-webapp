/****
 * Asset API Client
 *
 * This module provides functions to interact with the asset-related endpoints of our API.
 * It handles fetching of detailed asset information based on asset ID.
 */

import { useQuery } from '@tanstack/react-query';

export interface RawAssetDetails {
  item_number: string;
  version: string;
  description: string;
  level: number;
  config_item: string;
  seq_number: string;
  uom: string;
  status: string;
  quantity_per: string;
  item_class: string;
  software: string;
  serialised: boolean;
  under_change: boolean;
  main_equipment: string;
}

export interface AssetDetails {
  itemNumber: string;
  version: string;
  description: string;
  level: number;
  configItem: string;
  seqNumber: string;
  uom: string;
  status: string;
  quantityPer: string;
  itemClass: string;
  software: string;
  serialised: boolean;
  underChange: boolean;
  mainEquipment: string;
}

const ASSETS_ENDPOINT = `${window.env.EMS_BACKEND_URL}/assets`;

/****
 * Converts raw asset details from the API into a more frontend-friendly format
 * by transforming snake_case properties to camelCase.
 *
 * @param raw - The raw asset details from the API
 * @returns Converted asset details with camelCase properties
 */
const convertAssetDetails = (raw: RawAssetDetails): AssetDetails => {
  return {
    itemNumber: raw.item_number,
    version: raw.version,
    description: raw.description,
    level: raw.level,
    configItem: raw.config_item,
    seqNumber: raw.seq_number,
    uom: raw.uom,
    status: raw.status,
    quantityPer: raw.quantity_per,
    itemClass: raw.item_class,
    software: raw.software,
    serialised: raw.serialised,
    underChange: raw.under_change,
    mainEquipment: raw.main_equipment,
  };
};

/****
 * Fetches detailed information about a specific asset.
 *
 * @param assetId - The ID of the asset to fetch
 * @param authApiClient - The authentication api
 * @returns A promise that resolves to an AssetDetails object
 */
export const fetchAssetDetails = async (
  assetId: string,
  authApiClient: (url: string, options?: RequestInit) => Promise<Response>,
): Promise<AssetDetails> => {
  const response = await authApiClient(`${ASSETS_ENDPOINT}/${assetId}`);
  if (!response.ok) {
    throw new Error('Failed to fetch asset details');
  }
  const data: RawAssetDetails = await response.json();
  return convertAssetDetails(data);
};

/****
 * React Query hook for fetching asset details.
 * Uses stale time of 5 minutes to avoid unnecessary refetches.
 *
 * @param assetId - The ID of the asset to fetch
 * @param authApiClient - The authentication api
 * @returns Query result containing asset details and status
 */
export const useAssetDetails = (
  assetId: string,
  authApiClient: (url: string, options?: RequestInit) => Promise<Response>,
) => {
  return useQuery({
    queryKey: ['asset', assetId],
    queryFn: async () => fetchAssetDetails(assetId, authApiClient),
    enabled: !!assetId,
    staleTime: 5 * 60 * 1000, // 5 minutes
  });
};
