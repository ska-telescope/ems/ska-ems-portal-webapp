/**
 * Location Assets API Client
 *
 * This module provides functions to interact with the assets-related endpoints of our API.
 * It handles the fetching of asset data associated with specific locations.
 */
import { format } from 'date-fns';

export interface Asset {
  id: string;
  name: string;
  part_number: string;
  version: string;
  serial_number: string;
  topological_name: string;
  type: string;
  status: string;
}

const LOCATIONS_ENDPOINT = `${window.env.EMS_BACKEND_URL}/locations`;

/**
 * Fetches assets for a given location ID.
 *
 * @param locationId - The ID of the location to fetch assets for.
 * @param authApiClient - The authentication api
 * @returns A promise that resolves to an array of Asset objects.
 */
export const fetchLocationAssets = async (
  locationId: string,
  authApiClient: (url: string, options?: RequestInit) => Promise<Response>,
): Promise<Asset[]> => {
  const response = await authApiClient(
    `${LOCATIONS_ENDPOINT}/${locationId}/assets`,
  );
  if (!response.ok) {
    throw new Error('Network response was not ok');
  }

  // Since the API returns an array of assets, we can type it as Asset[]
  const data: Asset[] = await response.json();

  // Map over the data to ensure proper typing and default values
  return data.map((asset) => ({
    id: String(asset.id),
    name: asset.name || '',
    part_number: asset.part_number || '',
    version: asset.version || '',
    serial_number: asset.serial_number || '',
    topological_name: asset.topological_name || '',
    type: asset.type || '',
    status: asset.status || '',
  }));
};

/**
 * Fetches a CSV file with the assets for a given location ID.
 *
 * @param locationId - The ID of the location to fetch assets for.
 * @param authApiClient - The authentication api
 * @returns A promise that resolves to a blob of a CSV file.
 */
export const locationExportFile = async (
  locationId: string,
  authApiClient: (url: string, options?: RequestInit) => Promise<Response>,
) => {
  const response = await authApiClient(
    `${window.env.EMS_BACKEND_URL}/locations/${locationId}/export_assets`,
    {
      method: 'GET',
      headers: {
        Accept: 'text/csv',
      },
    },
  );

  if (!response.ok) {
    throw new Error(
      `Failed to fetch file: ${response.status} ${response.statusText}`,
    );
  }

  const blob = await response.blob();
  const currentDate = format(new Date(), 'dd-MM-yyyy');
  const fileName = `${locationId}_assets_${currentDate}.csv`;

  return { blob, fileName };
};
