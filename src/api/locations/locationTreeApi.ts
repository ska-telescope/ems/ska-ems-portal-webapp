export interface LocationNode {
  id: string;
  label: string;
  type: number;
  children?: LocationNode[];
}

export interface RawLocationNode {
  id: number;
  label: string;
  type: number;
  children?: RawLocationNode[];
  locations?: RawLocationNode[];
}

const LOCATIONS_ENDPOINT = `${window.env.EMS_BACKEND_URL}/locations`;

/**
 * Recursively converts all node IDs and changes any `locations` properties to `children`.
 *
 * @param node - The node to be converted.
 * @returns The node with all IDs converted to strings and `locations` properties renamed to `children`.
 */
const convertNodeStructure = (node: RawLocationNode): LocationNode => {
  const children = [...(node.children || []), ...(node.locations || [])].map(
    (child) => convertNodeStructure(child),
  );

  return {
    id: String(node.id),
    label: node.label,
    type: node.type,
    children,
  };
};

/**
 * Fetches the complete locations tree from the API.
 *
 * @returns A promise that resolves to an array of LocationNode objects representing the entire tree.
 */
export const fetchCompleteLocationsTree = async (
  authApiClient: (url: string, options?: RequestInit) => Promise<Response>,
): Promise<LocationNode[]> => {
  const response = await authApiClient(LOCATIONS_ENDPOINT);
  if (!response.ok) {
    throw new Error('Network response was not ok');
  }

  const data: RawLocationNode[] = await response.json();

  return data.map((location) => convertNodeStructure(location));
};
