/**
 * API Module Index
 *
 * This file serves as the entry point for all API-related functions.
 * It re-exports all functions from individual API modules, allowing for clean imports throughout the application.
 *
 * Usage:
 * import { fetchLocations, ... } from '@api/index';
 */

export * from '@api/locations/locationTreeApi';
export * from '@/api/apiTokens/gatewayTokensApi';
export * from '@api/maintenance/maintenanceApi';
export * from '@api/dashboard/dashboardApi';
