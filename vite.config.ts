import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react-swc';
import tsconfigPaths from 'vite-tsconfig-paths'

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  build: {rollupOptions:{external:["/env.js"]}},
  plugins: [react(), tsconfigPaths()],
});
