# Include core makefile targets for release management
-include .make/base.mk
-include .make/k8s.mk
-include .make/helm.mk
-include .make/oci.mk
-include .make/js.mk
-include PrivateRules.mak

dev-local-env:
	-rm public/env.js src/env.ts
	ENV_TYPE_FILE=env_scripts/env_config \
	ENV_JS_OUTPUT_LOCATION=public/env.js \
		bash env_scripts/env_config.sh js
	ENV_TYPE_FILE=env_scripts/env_config \
	ENV_JS_OUTPUT_LOCATION=src/env.ts \
		bash env_scripts/env_config.sh ts

# Override the default image repository and tag to always use the previously built image in pipelines
ifneq ($(CI_JOB_ID),)
K8S_CHART_PARAMS += --set image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set image.repository=$(CI_REGISTRY)/ska-telescope/ems/ska-ems-portal-webapp/ska-ems-portal-webapp
endif

K8S_CHART_PARAMS += \
	--set env.baseUrl=$(BASE_URL)
