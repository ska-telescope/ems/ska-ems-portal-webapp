# pull the base image
FROM node:21-alpine as base

# set the working directory
WORKDIR /app

# copy project files
COPY . .

# Install corepack, prepare Yarn, and build project
RUN apk add bash && corepack enable \
    && corepack prepare yarn@4 --activate \
    && yarn install && yarn cache clean \
    && ENV_TYPE_FILE=env_scripts/env_config \
    ENV_JS_OUTPUT_LOCATION=src/env.ts \
    bash env_scripts/env_config.sh ts \
    && yarn build

FROM nginx:1.27.0 as final

# Copy built files
COPY --from=base /app/dist/ /usr/share/nginx/html/

COPY nginx.conf /etc/nginx/

EXPOSE 80

COPY env_scripts/env_config /env_config
COPY env_scripts/env_config.sh /docker-entrypoint.d/
RUN chmod 777 /docker-entrypoint.d/*.sh